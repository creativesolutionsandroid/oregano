package com.cs.oregano;

import android.app.Application;

import com.cs.oregano.widgets.FontsOverride;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by CS on 26-05-2016.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FontsOverride.setDefaultFont(this, "SERIF", "GOTHIC_0.ttf");
    }
}
