package com.cs.oregano.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.oregano.Constants;
import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.R;
import com.cs.oregano.activities.CategoryItems;
import com.cs.oregano.activities.CheckoutActivity;
import com.cs.oregano.widgets.Utils;

import java.text.DecimalFormat;

/**
 * Created by CS on 09-06-2016.
 */
public class OrderFragment extends Fragment implements View.OnClickListener{

    Integer[] images = {R.drawable.salad, R.drawable.soupe,
            R.drawable.sharable, R.drawable.rustica, R.drawable.colzones,
            R.drawable.pasta, R.drawable.pizza, R.drawable.pizza,
            R.drawable.desserts, R.drawable.beverage, R.drawable.beverage};
    String[] tites;

    String[] catIds = {"1","2","3","4","5","6","7","8","9","10","11"};

    public static TextView orderPrice, orderQuantity;
    ImageView salads, soups, sharables, rustica, colzones, pastas, pizzas, desserts, beverages;
    RelativeLayout checkOut;
    ImageView ramadanPromotion;

//    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    Toolbar toolbar;
    private DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    String language;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        Display deviceDisplay = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay();
        int layoutWidth = deviceDisplay.getWidth();// 480
        int layoutHeight = deviceDisplay.getHeight();// 800
        if(language.equalsIgnoreCase("En")) {
//            if(layoutHeight < 1280){
//                rootView = inflater.inflate(R.layout.menu_activity_scroll, container,
//                        false);
//            }else{
                rootView = inflater.inflate(R.layout.menu_activity_new, container,
                        false);
//            }
            tites = new String[]{"Salad", "Soup","Sharable", "Rustica", "Calzone",
                    "Pasta", "Pizza", "Pizza",
                    "Dessert", "Beverage", "Beverage"};
        }else if(language.equalsIgnoreCase("Ar")){
//            if(layoutHeight < 1280){
//                rootView = inflater.inflate(R.layout.menu_activity_arabic_scroll, container,
//                        false);
//            }else{
                rootView = inflater.inflate(R.layout.menu_activity_new_arabic, container,
                        false);
//            }
            tites = new String[]{"السلطات", "الحساء","أطباق للمشاركة", "روستيكا", "الكالزون",
                    "الباستا", "بيتزا", "بيتزا",
                    "الحلويات", "مشروبات", "مشروبات"};
        }
        myDbHelper = new DataBaseHelper(getActivity());
        orderPrice = (TextView) rootView.findViewById(R.id.item_price);
        orderQuantity = (TextView) rootView.findViewById(R.id.item_qty);
        checkOut = (RelativeLayout) rootView.findViewById(R.id.checkout_layout);
        salads = (ImageView) rootView.findViewById(R.id.salad);
        soups = (ImageView) rootView.findViewById(R.id.soup);
        sharables = (ImageView) rootView.findViewById(R.id.shareables);
        rustica = (ImageView) rootView.findViewById(R.id.rustica);
        colzones = (ImageView) rootView.findViewById(R.id.colzones);
        pastas = (ImageView) rootView.findViewById(R.id.pasta);
        pizzas = (ImageView) rootView.findViewById(R.id.pizza);
        desserts = (ImageView) rootView.findViewById(R.id.desserts);
        beverages = (ImageView) rootView.findViewById(R.id.beverage);
        ramadanPromotion = (ImageView) rootView.findViewById(R.id.ramadanPromotion);
//        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        // The number of Columns
//        mLayoutManager = new GridLayoutManager(getActivity(), 2);
//        mRecyclerView.setLayoutManager(mLayoutManager);
//
//        mAdapter = new MenuAdapter();
//        mRecyclerView.setAdapter(mAdapter);

        salads.setOnClickListener(this);
        soups.setOnClickListener(this);
        sharables.setOnClickListener(this);
        rustica.setOnClickListener(this);
        colzones.setOnClickListener(this);
        pastas.setOnClickListener(this);
        pizzas.setOnClickListener(this);
        desserts.setOnClickListener(this);
        beverages.setOnClickListener(this);
        ramadanPromotion.setOnClickListener(this);

        if(!MainFragmentNew.RamadanCategoryId.equals("")){
            Glide.with(getActivity())
                    .load(Constants.IMAGE_URL+MainFragmentNew.RamadanSeasonImage)
                    .into(ramadanPromotion);
            ramadanPromotion.setVisibility(View.VISIBLE);
        }

        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myDbHelper.getTotalOrderQty() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Material_Light_Dialog));

                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Oregano");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("اوريجانو");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {
                    Intent checkoutIntent = new Intent(getActivity(), CheckoutActivity.class);
                    startActivity(checkoutIntent);
                }
            }
        });
        DecimalFormat decimalFormat =new DecimalFormat("0.00");

//        float dec = Float.parseFloat(decimalFormat.format(myDbHelper.getTotalOrderPrice()));
        if (language.equalsIgnoreCase("Ar")) {

//            NumberFormat format = NumberFormat.getCurrencyInstance();
                orderPrice.setText("" + decimalFormat.format(myDbHelper.getTotalOrderPrice()));

//            Log.e("TAG","price "+format.format(myDbHelper.getTotalOrderPrice()));
        }else {
            orderPrice.setText("" + decimalFormat.format(myDbHelper.getTotalOrderPrice()));
        }
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.salad:
                Intent intent = new Intent(getActivity(), CategoryItems.class);
                intent.putExtra("catId", catIds[0]);
                intent.putExtra("catName", tites[0]);
                intent.putExtra("cat_type",1);
                startActivity(intent);
                break;

            case R.id.soup:
                Intent intent1 = new Intent(getActivity(), CategoryItems.class);
                intent1.putExtra("catId", catIds[1]);
                intent1.putExtra("catName", tites[1]);
                intent1.putExtra("cat_type",1);
                startActivity(intent1);
                break;

            case R.id.shareables:
                Intent intent2 = new Intent(getActivity(), CategoryItems.class);
                intent2.putExtra("catId", catIds[2]);
                intent2.putExtra("catName", tites[2]);
                intent2.putExtra("cat_type",1);
                startActivity(intent2);
                break;

            case R.id.rustica:
                Intent intent3 = new Intent(getActivity(), CategoryItems.class);
                intent3.putExtra("catId", catIds[3]);
                intent3.putExtra("catName", tites[3]);
                intent3.putExtra("cat_type",1);
                startActivity(intent3);
                break;

            case R.id.colzones:
                Intent intent4 = new Intent(getActivity(), CategoryItems.class);
                intent4.putExtra("catId", catIds[4]);
                intent4.putExtra("catName", tites[4]);
                intent4.putExtra("cat_type",1);
                startActivity(intent4);
                break;

            case R.id.pasta:
                Intent intent5 = new Intent(getActivity(), CategoryItems.class);
                if(language.equalsIgnoreCase("En")) {
                    intent5.putExtra("catName", tites[5]);
                }else if(language.equalsIgnoreCase("Ar")){
                    intent5.putExtra("catName", "الباستا");
                }
                intent5.putExtra("catId", catIds[5]);
                intent5.putExtra("cat_type",1);
                startActivity(intent5);
                break;

            case R.id.pizza:
                Intent intent6 = new Intent(getActivity(), CategoryItems.class);
                if(language.equalsIgnoreCase("En")) {
                    intent6.putExtra("catName", tites[7]);
                }else if(language.equalsIgnoreCase("Ar")){
                    intent6.putExtra("catName", "البيتزا");
                }
                intent6.putExtra("catId", catIds[7]);
                intent6.putExtra("cat_type",2);
                startActivity(intent6);
                break;

            case R.id.desserts:
                Intent intent7 = new Intent(getActivity(), CategoryItems.class);
                intent7.putExtra("catId", catIds[8]);
                intent7.putExtra("catName", tites[8]);
                intent7.putExtra("cat_type",1);
                startActivity(intent7);
                break;

            case R.id.beverage:
                Intent intent8 = new Intent(getActivity(), CategoryItems.class);
                intent8.putExtra("catId", catIds[10]);
                intent8.putExtra("catName", tites[10]);
                intent8.putExtra("cat_type",2);
                startActivity(intent8);
                break;

            case R.id.ramadanPromotion:
                Intent intent9 = new Intent(getActivity(), CategoryItems.class);
                intent9.putExtra("catId", MainFragmentNew.RamadanCategoryId);
                if(language.equals("En")) {
                    intent9.putExtra("catName", MainFragmentNew.RamadanCatergoryName);
                }
                else{
                    intent9.putExtra("catName", MainFragmentNew.RamadanCatergoryNameAr);
                }
                intent9.putExtra("cat_type",1);
                startActivity(intent9);
                break;
        }
    }


    public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

        public class MenuViewHolder extends RecyclerView.ViewHolder {

            TextView mExplorerCategory;
            TextView mExploreCount;
            ImageView mExploreImage;
            CardView mExploreCard;

            MenuViewHolder(View itemView) {
                super(itemView);
                mExplorerCategory = (TextView) itemView.findViewById(R.id.item_text);
                mExploreImage = (ImageView) itemView.findViewById(R.id.item_image);
                mExploreCard = (CardView) itemView.findViewById(R.id.explorecard);


                ViewGroup.LayoutParams layoutParams =  mExploreImage.getLayoutParams();
                layoutParams.height = Utils.getScreenWidth(getActivity(),2);
                layoutParams.width = Utils.getScreenWidth(getActivity(),2);
                mExploreImage.setLayoutParams(layoutParams);
            }
        }


        @Override
        public int getItemCount() {
            return images.length;
        }


        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        @Override
        public MenuViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_row, viewGroup, false);
            MenuViewHolder mvh = new MenuViewHolder(v);
            return mvh;
        }

        @Override
        public void onBindViewHolder(MenuViewHolder exploreViewHolder, final int position)

        {
            exploreViewHolder.mExplorerCategory.setText(tites[position]);


            Glide.with(getActivity())
                    .load(images[position])
                    .override(Utils.getScreenWidth(getActivity(),2), Utils.getScreenWidth(getActivity(),2)).into(exploreViewHolder.mExploreImage);


            exploreViewHolder.mExploreImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(getActivity(), CategoryItems.class);
                    intent.putExtra("catId", catIds[position]);
                    intent.putExtra("catName", tites[position]);
                    startActivity(intent);

                }
            });
        }


    }
}
