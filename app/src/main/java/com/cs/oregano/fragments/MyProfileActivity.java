package com.cs.oregano.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cs.oregano.R;
import com.cs.oregano.activities.AddressActivity;
import com.cs.oregano.activities.ChangePassword;
import com.cs.oregano.activities.EditProfile;
import com.cs.oregano.activities.MessagesActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 26-05-2016.
 */
public class MyProfileActivity extends AppCompatActivity {

    TextView manageAddress, name, email, mobile, messages, changePassword, feedback, editProfile;
    Button logout;
    SharedPreferences userPrefs;
    SharedPreferences.Editor  userPrefEditor;
    String response;
    String mLoginStatus;
    View line;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;

    @Nullable
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.account_fragment);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.account_fragment_arabic);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        mLoginStatus = userPrefs.getString("login_status", "");
//        userId = userPrefs.getString("userId", null);
        response = userPrefs.getString("user_profile", null);
        manageAddress = (TextView) findViewById(R.id.manage_address);
        name = (TextView) findViewById(R.id.name);
        email = (TextView) findViewById(R.id.email);
        mobile = (TextView) findViewById(R.id.phone);
        messages = (TextView) findViewById(R.id.messages);
        changePassword = (TextView) findViewById(R.id.change_password);
        logout = (Button) findViewById(R.id.logout);
        editProfile = (TextView) findViewById(R.id.edit_profile);


            if(response != null) {
                try {
                    JSONObject property = new JSONObject(response);
                    JSONObject userObjuect = property.getJSONObject("profile");

                    name.setText(userObjuect.getString("fullName") +" "+ userObjuect.getString("family_name")+"\n"+userObjuect.getString("nick_name"));
                    mobile.setText("+"+userObjuect.getString("mobile"));
                    email.setText(userObjuect.getString("email"));
                } catch (JSONException e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }
            }


        manageAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MyProfileActivity.this, AddressActivity.class);
                startActivity(i);

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    userPrefEditor.clear();
                    userPrefEditor.commit();
                    logout.setText("Login");
                    onBackPressed();

            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileActivity.this, ChangePassword.class);
                startActivity(intent);
            }
        });

        messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileActivity.this, MessagesActivity.class);
                startActivity(intent);
            }
        });

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyProfileActivity.this, EditProfile.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        response = userPrefs.getString("user_profile", null);
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                name.setText(userObjuect.getString("fullName") +" "+ userObjuect.getString("family_name")+"\n"+userObjuect.getString("nick_name"));
                mobile.setText("+"+userObjuect.getString("mobile"));
                email.setText(userObjuect.getString("email"));
            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }
}
