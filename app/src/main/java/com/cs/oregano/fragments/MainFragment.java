package com.cs.oregano.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.JSONParser;
import com.cs.oregano.MainActivity;
import com.cs.oregano.R;
import com.cs.oregano.activities.FavoriteOrdersActivity;
import com.cs.oregano.activities.LoginActivity;
import com.cs.oregano.activities.MessagesActivity;
import com.cs.oregano.activities.OffersActivity;
import com.cs.oregano.activities.OrderHistoryActivity;
import com.cs.oregano.activities.TrackOrderSteps;
import com.cs.oregano.adapters.BannersAdapter;
import com.cs.oregano.model.Banner;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by CS on 25-05-2016.
 */
public class MainFragment extends Fragment {

    private static final int ORDER_HISTORY_REQUEST = 1;
    private static final int FAVORITE_ORDER_REQUEST = 2;
    private static final int TRACK_ORDER_REQUEST = 3;
    private static final int MESSAGES_REQUEST = 4;
    RelativeLayout orderNowBtn, orderHistoryBtn, favoriteOrderBtn, trackOrderBtn, messagesBtn;
    LinearLayout banner1, banner2, banner3;
    SharedPreferences userPrefs;
    String mLoginStatus;

    public static ArrayList<Banner> bannerList = new ArrayList<>();
    BannersAdapter defaultPagerAdapter;
    CircleIndicator defaultIndicator;
    ViewPager defaultViewpager;
    SharedPreferences languagePrefs;
    String language;
    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.main_fragment, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.main_fragment_arabic, container,
                    false);
        }
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");
        orderNowBtn = (RelativeLayout) rootView.findViewById(R.id.order_now_btn);
        orderHistoryBtn = (RelativeLayout) rootView.findViewById(R.id.order_history_btn);
        favoriteOrderBtn = (RelativeLayout) rootView.findViewById(R.id.fav_order_btn);
        trackOrderBtn = (RelativeLayout) rootView.findViewById(R.id.track_order_btn);
        messagesBtn = (RelativeLayout) rootView.findViewById(R.id.messages_btn);
        banner1 = (LinearLayout) rootView.findViewById(R.id.banner1);
        banner2 = (LinearLayout) rootView.findViewById(R.id.banner2);
        banner3 = (LinearLayout) rootView.findViewById(R.id.banner3);
        defaultViewpager = (ViewPager) rootView.findViewById(R.id.viewPager);
        defaultIndicator = (CircleIndicator) rootView.findViewById(R.id.indicator_default);
//        defaultPagerAdapter = new BannersAdapter(getActivity(), bannerList);
//        defaultViewpager.setAdapter(defaultPagerAdapter);
//        defaultIndicator.setViewPager(defaultViewpager);
        if(bannerList.size()>0){
            defaultPagerAdapter = new BannersAdapter(getActivity(), bannerList);
            defaultViewpager.setAdapter(defaultPagerAdapter);
            defaultIndicator.setViewPager(defaultViewpager);
        }else {
            new GetBanners().execute(Constants.GET_BANNERS_URL);
        }
        orderNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).selectItem(2);
            }
        });

        orderHistoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(getActivity(), OrderHistoryActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, ORDER_HISTORY_REQUEST);
                }

            }
        });

        favoriteOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(getActivity(), FavoriteOrdersActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, FAVORITE_ORDER_REQUEST);
                }
            }
        });

        trackOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(getActivity(), TrackOrderSteps.class);
                    intent.putExtra("orderId","-1");
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, TRACK_ORDER_REQUEST);
                }
            }
        });

        messagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(getActivity(), MessagesActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, MESSAGES_REQUEST);
                }
            }
        });

        banner1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), OffersActivity.class);
                i.putExtra("offerId" , "1");
                startActivity(i);
            }
        });

        banner2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), OffersActivity.class);
                i.putExtra("offerId" , "2");
                startActivity(i);
            }
        });

        banner3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), OffersActivity.class);
                i.putExtra("offerId" , "3");
                startActivity(i);
            }
        });


        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLoginStatus = userPrefs.getString("login_status", "");
        if (requestCode == TRACK_ORDER_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), TrackOrderSteps.class);
            intent.putExtra("orderId","-1");
            startActivity(intent);
        }else if(requestCode == ORDER_HISTORY_REQUEST
                && resultCode == Activity.RESULT_OK){
            Intent intent = new Intent(getActivity(), OrderHistoryActivity.class);
            startActivity(intent);
        }else if(requestCode == FAVORITE_ORDER_REQUEST
                && resultCode == Activity.RESULT_OK){
            Intent intent = new Intent(getActivity(), FavoriteOrdersActivity.class);
            startActivity(intent);
        }else if(requestCode == MESSAGES_REQUEST
                && resultCode == Activity.RESULT_OK){
            Intent intent = new Intent(getActivity(), MessagesActivity.class);
            startActivity(intent);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }



    public class GetBanners extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            dialog = ProgressDialog.show(OffersActivity.this, "",
//                    "Loading address...");
            bannerList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            bannerList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if(getActivity() != null) {
                        Toast.makeText(getActivity(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        if(getActivity() != null) {
                            Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
                        }
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i<ja.length(); i++) {

                                    Banner offer = new Banner();
                                    JSONObject jo1 = ja.getJSONObject(i);


                                    String id = jo1.getString("id");
                                    boolean isClick = jo1.getBoolean("isclick");
                                    String image = jo1.getString("bannername");

                                    offer.setId(id);
                                    offer.setClickable(isClick);
                                    offer.setImage(image);

                                    bannerList.add(offer);

                                }
                            }catch (JSONException je){
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Oregano");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            if(getActivity() != null) {
                defaultPagerAdapter = new BannersAdapter(getActivity(), bannerList);
                defaultViewpager.setAdapter(defaultPagerAdapter);
                defaultIndicator.setViewPager(defaultViewpager);
            }
            super.onPostExecute(result);

        }

    }
}
