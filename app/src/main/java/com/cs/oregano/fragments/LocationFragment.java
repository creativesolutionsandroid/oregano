package com.cs.oregano.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.GPSTracker;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.activities.StoreDetailsActivity;
import com.cs.oregano.adapters.SelectStoresAdapter;
import com.cs.oregano.model.StoreInfo;
import com.cs.oregano.widgets.NetworkUtil;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 15-06-2016.
 */
public class LocationFragment extends Fragment {
    ListView listView;

    private String timeResponse = null;

    String response;
    private ArrayList<StoreInfo> storesList = new ArrayList<>();
    SelectStoresAdapter mAdapter;

    RelativeLayout titleBar;

    Double lat, longi;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;

    private SwipeRefreshLayout swipeLayout;
    boolean loading = false;

    String language;
    SharedPreferences languagePrefs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.select_stores_layout, container,
                false);

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        titleBar = (RelativeLayout) rootView.findViewById(R.id.title_bar);
        titleBar.setVisibility(View.GONE);
        listView = (ListView) rootView.findViewById(R.id.stores_list);
        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);


        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        }else {
            getGPSCoordinates();
        }

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                if(!loading) {
                    loading = true;
                    getGPSCoordinates();
                }else{
                    swipeLayout.setRefreshing(false);
                }

            }
        });
        swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StoreInfo si = storesList.get(position);
                Intent intent = new Intent(getActivity(), StoreDetailsActivity.class);
                intent.putExtra("storeName", si.getStoreName());
                intent.putExtra("storeImage", si.getImageURL());
                intent.putExtra("storeAddress", si.getStoreAddress());
                intent.putExtra("storeId", si.getStoreId());
                intent.putExtra("latitude", si.getLatitude());
                intent.putExtra("longitude", si.getLongitude());
                intent.putExtra("lat", lat);
                intent.putExtra("longi", longi);
                intent.putExtra("start_time", si.getStartTime());
                intent.putExtra("end_time", si.getEndTime());
                intent.putExtra("full_hours", si.getIs24x7());
                intent.putExtra("order_type", Constants.ORDER_TYPE);
                startActivity(intent);
            }
        });

        return rootView;
    }


    public void getGPSCoordinates(){
        gps = new GPSTracker(getActivity());
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                mAdapter = new SelectStoresAdapter(getActivity(), storesList, lat, longi);
//                listView.setAdapter(mAdapter);
                new GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String dayOfWeek;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading items...");
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
            storesList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]+dayOfWeek);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            storesList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getActivity(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONArray ja = new JSONArray(result);
                            for (int i = 0; i < ja.length(); i++) {
                                StoreInfo si = new StoreInfo();
                                JSONObject jo = ja.getJSONObject(i);
//                                lat = 24.70321657;
//                                longi = 46.68097073;
                                si.setStoreId(jo.getString("storeId"));
                                si.setStartTime(jo.getString("ST"));
                                si.setEndTime(jo.getString("ET"));
                                si.setStoreName(jo.getString("StoreName"));
                                si.setStoreAddress(jo.getString("StoreAddress"));
                                si.setLatitude(jo.getDouble("Latitude"));
                                si.setLongitude(jo.getDouble("Longitude"));
                                si.setCountryName(jo.getString("CountryName"));
                                si.setCityName(jo.getString("CityName"));
                                si.setImageURL(jo.getString("imageURL"));
                                si.setCarryoutdistance(jo.getInt("DineInDistance"));
                                si.setFamilySection(jo.getString("FamilySection"));
                                si.setWifi(jo.getString("Wifi"));
                                si.setPatioSitting(jo.getString("PatioSitting"));
                                si.setDriveThru(jo.getString("DriveThru"));
                                si.setMeetingSpace(jo.getString("MeetingSpace"));
                                si.setHospital(jo.getString("Hospital"));
                                si.setUniversity(jo.getString("University"));
                                si.setOffice(jo.getString("Office"));
                                si.setShoppingMall(jo.getString("ShoppingMall"));
                                try{
                                    si.setAirPort(jo.getString("Airport"));
                                }catch (Exception e){
                                    si.setAirPort("false");
                                }
                                try{
                                    si.setDineIn(jo.getString("DineIn"));
                                }catch (Exception e){
                                    si.setDineIn("false");
                                }
                                try{
                                    si.setLadies(jo.getString("Ladies"));
                                }catch (Exception e){
                                    si.setLadies("false");
                                }

                                si.setNeighborhood(jo.getString("Neighborhood"));
                                si.setIs24x7(jo.getString("is24x7"));
                                si.setStatus(jo.getString("status"));
                                si.setOgCountry(jo.getString("OGCountry"));
                                si.setOgCity(jo.getString("OGCity"));
                                si.setStoreName_ar(jo.getString("StoreName_ar"));
                                si.setStoreAddress_ar(jo.getString("StoreAddress_ar"));
                                si.setStoreNumber(jo.getString("phone"));
                                try{
                                    si.setMessage(jo.getString("Message"));
                                }catch (Exception e){
                                    si.setMessage("");
                                }

                                try{
                                    si.setMessage_ar(jo.getString("Message_ar"));
                                }catch (Exception e){
                                    si.setMessage_ar("");
                                }

                                Location me   = new Location("");
                                Location dest = new Location("");

                                me.setLatitude(lat);
                                me.setLongitude(longi);

                                dest.setLatitude(jo.getDouble("Latitude"));
                                dest.setLongitude(jo.getDouble("Longitude"));

                                float dist = (me.distanceTo(dest))/1000;

                                Log.i("TAG","carryout distance "+ si.getCarryoutdistance());
                                si.setDistance(dist);
                                if(dist <= si.getCarryoutdistance()) {
                                    storesList.add(si);
                                }
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            Collections.sort(storesList, StoreInfo.storeDistance);
            mAdapter.notifyDataSetChanged();
            loading = false;
            swipeLayout.setRefreshing(false);

            if(storesList.size() == 0){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Material_Light_Dialog));

                if (language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("Oregano");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("We're sorry, but no stores were found in your area.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                } else if (language.equalsIgnoreCase("Ar")) {
                    // set title
                    alertDialogBuilder.setTitle("اوريجانو");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("نعتذر لك ، لا يوجد فروع متوفرة في منطتك")
                            .setCancelable(false)
                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                }

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
            super.onPostExecute(result);

        }

    }


    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            }else{
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(serverTime == null){
                dialog.dismiss();
            } else if(serverTime.equals("no internet")){
                dialog.dismiss();
                Toast.makeText(getActivity(), "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
//                    timeResponse = "28/3/2018 05:00 PM";
                }catch (JSONException je){
                    je.printStackTrace();
                }
                new GetStoresInfo().execute(Constants.STORES_URL);
//                if(language.equalsIgnoreCase("En")) {

                mAdapter = new SelectStoresAdapter(getActivity(), storesList, lat, longi, timeResponse);
                listView.setAdapter(mAdapter);
//                }else if(language.equalsIgnoreCase("Ar")){
//                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
//                    mStoresListView.setAdapter(mStoreListAdapterArabic);
//                }

            }


            super.onPostExecute(result1);
        }
    }
}
