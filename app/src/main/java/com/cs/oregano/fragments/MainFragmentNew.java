package com.cs.oregano.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.JSONParser;
import com.cs.oregano.MainActivity;
import com.cs.oregano.R;
import com.cs.oregano.activities.FavoriteOrdersActivity;
import com.cs.oregano.activities.LoginActivity;
import com.cs.oregano.activities.MessagesActivity;
import com.cs.oregano.activities.OffersActivity;
import com.cs.oregano.activities.OrderHistoryActivity;
import com.cs.oregano.activities.RatingActivity;
import com.cs.oregano.activities.TrackOrderSteps;
import com.cs.oregano.model.Rating;
import com.cs.oregano.model.RatingDetails;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by CS on 20-01-2017.
 */
public class MainFragmentNew  extends Fragment {

    private static final int ORDER_HISTORY_REQUEST = 1;
    private static final int FAVORITE_ORDER_REQUEST = 2;
    private static final int TRACK_ORDER_REQUEST = 3;
    private static final int MESSAGES_REQUEST = 4;
    RelativeLayout orderNowBtn, orderHistoryBtn, favoriteOrderBtn, trackOrderBtn, messagesBtn, locationBtn;
    TextView banner1, banner2, banner3;
    RelativeLayout offerLayout;
    ImageView menuBar;
    LinearLayout offerLayout1;
    ImageView designImage;
    SharedPreferences userPrefs;
    String mLoginStatus;
    TextView langAr, langEng;
    int ratingFromService;

    public static String RamadanCategoryId = "";
    public static String RamadanSeasonImage = "";
    public static String RamadanCatergoryName = "";
    public static String RamadanCatergoryNameAr = "";

    int layoutWidth ,layoutHeight, logo_width, logo_height;

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    View rootView;

    RelativeLayout rate_layout;
    MaterialRatingBar ratingBar;
    TextView cancel;
    String orderId;
    String userId;
    String OrderType = "", OrderStatus;
    final ArrayList<RatingDetails> rateArray = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.main_fragment_new, container,
                    false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.main_fragment_new_arabic, container,
                    false);
        }
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");
        orderNowBtn = (RelativeLayout) rootView.findViewById(R.id.order_now_btn);
        locationBtn = (RelativeLayout) rootView.findViewById(R.id.location_btn);
        orderHistoryBtn = (RelativeLayout) rootView.findViewById(R.id.order_history_btn);
        favoriteOrderBtn = (RelativeLayout) rootView.findViewById(R.id.fav_order_btn);
        trackOrderBtn = (RelativeLayout) rootView.findViewById(R.id.track_order_btn);
        messagesBtn = (RelativeLayout) rootView.findViewById(R.id.messages_btn);
        designImage = (ImageView) rootView.findViewById(R.id.design_image);
        offerLayout = (RelativeLayout) rootView.findViewById(R.id.offer_layout);
        offerLayout1 = (LinearLayout) rootView.findViewById(R.id.offer_layout1);
        banner1 = (TextView) rootView.findViewById(R.id.banner1);
        banner2 = (TextView) rootView.findViewById(R.id.banner2);
        banner3 = (TextView) rootView.findViewById(R.id.banner3);
        menuBar = (ImageView) rootView.findViewById(R.id.menu_bar);
        langAr = (TextView) rootView.findViewById(R.id.language_arabic);
        langEng = (TextView) rootView.findViewById(R.id.language_eng);
        ratingBar = (MaterialRatingBar) rootView.findViewById(R.id.ratingBar1);
        cancel = (TextView) rootView.findViewById(R.id.skip_rating);
        rate_layout = (RelativeLayout) rootView.findViewById(R.id.rate_layout);

        Display deviceDisplay = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay();
        layoutWidth = deviceDisplay.getWidth();// 480
        layoutHeight = deviceDisplay.getHeight();// 800

        /* :::::::::::::Parameters for UI controls:::::::::::::::::: */


        // setting parameters for Header Logo
//        logo_width = (int) (layoutWidth * 10 / 17.1428);// 280
        logo_height = (int) (layoutHeight * 10 / 22.8571);// 300

//        ViewTreeObserver vto = designImage.getViewTreeObserver();
//        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            public boolean onPreDraw() {
//                designImage.getViewTreeObserver().removeOnPreDrawListener(this);
//                int finalHeight = designImage.getMeasuredHeight();
//                Log.i("Height TAG", "" + finalHeight);
//                LinearLayout.LayoutParams newsCountParam = new
//                        LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
//                RelativeLayout.LayoutParams params = new
//                        RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight);
//                offerLayout.setLayoutParams(params);
//                offerLayout1.setLayoutParams(params);
//                return true;
//            }
//        });

        RelativeLayout.LayoutParams backParam = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, logo_height);
        LinearLayout.LayoutParams backParam1 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, logo_height);
        offerLayout.setLayoutParams(backParam1);
        designImage.setLayoutParams(backParam);

        if(RamadanCategoryId.equals("")){
            new GetRamadanPromotion().execute(Constants.RAMADAN_PROMOTION_URL+userId+"&OrderId=0");
        }

        langAr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languagePrefsEditor.putString("language","Ar");
                languagePrefsEditor.commit();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                rate_layout.setVisibility(View.GONE);
                startActivity(intent);
                getActivity().finish();
            }
        });

        langEng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languagePrefsEditor.putString("language","En");
                languagePrefsEditor.commit();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                rate_layout.setVisibility(View.GONE);
                startActivity(intent);
                getActivity().finish();
            }
        });

        orderNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).selectItem(2);
            }
        });

        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).selectItem(1);
            }
        });


        menuBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        orderHistoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(getActivity(), OrderHistoryActivity.class);
                    rate_layout.setVisibility(View.GONE);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    rate_layout.setVisibility(View.GONE);
                    startActivityForResult(intent, ORDER_HISTORY_REQUEST);
                }

            }
        });

        favoriteOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(getActivity(), FavoriteOrdersActivity.class);
                    rate_layout.setVisibility(View.GONE);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    rate_layout.setVisibility(View.GONE);
                    startActivityForResult(intent, FAVORITE_ORDER_REQUEST);
                }
            }
        });

        trackOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(getActivity(), TrackOrderSteps.class);
                    rate_layout.setVisibility(View.GONE);
                    intent.putExtra("orderId","-1");
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    rate_layout.setVisibility(View.GONE);
                    startActivityForResult(intent, TRACK_ORDER_REQUEST);
                }
            }
        });

        messagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(getActivity(), MessagesActivity.class);
                    rate_layout.setVisibility(View.GONE);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    rate_layout.setVisibility(View.GONE);
                    startActivityForResult(intent, MESSAGES_REQUEST);
                }
            }
        });

        banner1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), OffersActivity.class);
                rate_layout.setVisibility(View.GONE);
                i.putExtra("offerId" , "1");
                startActivity(i);
            }
        });

        banner2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), OffersActivity.class);
                rate_layout.setVisibility(View.GONE);
                i.putExtra("offerId" , "2");
                startActivity(i);
            }
        });

        banner3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), OffersActivity.class);
                rate_layout.setVisibility(View.GONE);
                i.putExtra("offerId" , "3");
                startActivity(i);
            }
        });


        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLoginStatus = userPrefs.getString("login_status", "");
        if (requestCode == TRACK_ORDER_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), TrackOrderSteps.class);
            rate_layout.setVisibility(View.GONE);
            intent.putExtra("orderId","-1");
            startActivity(intent);
        }else if(requestCode == ORDER_HISTORY_REQUEST
                && resultCode == Activity.RESULT_OK){
            Intent intent = new Intent(getActivity(), OrderHistoryActivity.class);
            rate_layout.setVisibility(View.GONE);
            startActivity(intent);
        }else if(requestCode == FAVORITE_ORDER_REQUEST
                && resultCode == Activity.RESULT_OK){
            Intent intent = new Intent(getActivity(), FavoriteOrdersActivity.class);
            rate_layout.setVisibility(View.GONE);
            startActivity(intent);
        }else if(requestCode == MESSAGES_REQUEST
                && resultCode == Activity.RESULT_OK){
            Intent intent = new Intent(getActivity(), MessagesActivity.class);
            rate_layout.setVisibility(View.GONE);
            startActivity(intent);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    public class GetOrderTrackDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        //        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {

            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Reloading...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
//            arrayList.clear();
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getActivity(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
//                        Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String failure = jo.getString("Failure");
                            } catch (JSONException jse) {

                                try {
                                    JSONArray ja = jo.getJSONArray("Received");
//                                for (int i = 0; i<ja.length(); i++) {

                                    JSONObject jo1 = ja.getJSONObject(0);
                                    orderId = jo1.getString("OrderId");
                                    OrderType = jo1.getString("OrderType");
                                    ratingFromService = jo1.getInt("Rating");
                                    OrderStatus = "Order!";

                                    RatingDetails rd = new RatingDetails();
                                    JSONArray jA = jo1.getJSONArray("Ratings");
                                    JSONObject obj = jA.getJSONObject(0);
//                                    rd.setGivenrating("5");
                                    JSONArray ratingArray = obj.getJSONArray("5");
                                    ArrayList<Rating> ratingsArrayList = new ArrayList<>();
                                    for (int i = 0; i < ratingArray.length(); i++) {
                                        JSONObject object1 = ratingArray.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);

                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray1 = obj.getJSONArray("4");
                                    rd.setGivenrating("4");
                                    for (int i = 0; i < ratingArray1.length(); i++) {
                                        JSONObject object1 = ratingArray1.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray2 = obj.getJSONArray("3");
                                    rd.setGivenrating("3");
                                    for (int i = 0; i < ratingArray2.length(); i++) {
                                        JSONObject object1 = ratingArray2.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);

                                    JSONArray ratingArray3 = obj.getJSONArray("2");
                                    rd.setGivenrating("2");
                                    for (int i = 0; i < ratingArray3.length(); i++) {
                                        JSONObject object1 = ratingArray3.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray4 = obj.getJSONArray("1");
                                    rd.setGivenrating("1");
                                    for (int i = 0; i < ratingArray4.length(); i++) {
                                        JSONObject object1 = ratingArray4.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);

                                    rateArray.add(rd);
//                                }
                                } catch (JSONException je) {
                                    je.printStackTrace();

                                }


//                                if (OrderType.equalsIgnoreCase("Delivery")) {

                                try {
                                    JSONArray ja = jo.getJSONArray("Delivered");
                                    JSONObject jo1 = ja.getJSONObject(0);
                                    if (ratingFromService == 0) {


                                        rate_layout.setVisibility(View.VISIBLE);
                                        cancel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                rate_layout.setVisibility(View.GONE);
                                            }
                                        });

                                        ratingBar.setRating(0);
                                        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                            @Override
                                            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                if (fromUser) {
                                                    Intent intent = new Intent(getActivity(), RatingActivity.class);
                                                    intent.putExtra("rating", rating);
                                                    intent.putExtra("array", rateArray);
                                                    intent.putExtra("userid", userId);
                                                    intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                                    startActivity(intent);
                                                    rate_layout.setVisibility(View.GONE);
                                                }
                                            }
                                        });
                                    }
                                } catch (JSONException je) {
                                    je.printStackTrace();
//                                    }
                                }

////                                }
//                                    } catch (JSONException je) {
//                                    }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
//                                    Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public class GetRamadanPromotion extends AsyncTask<String, Integer, String> {

        String  networkStatus;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.v("TAG", "ramadan response:" + result);
            if (result != null) {
                try {
                    JSONObject jo = new JSONObject(result);
                    JSONArray promoArray = jo.getJSONArray("SeasonOffers");
                    JSONObject promoObj = promoArray.getJSONObject(0);
                    RamadanCategoryId = promoObj.getString("CategoryId");
                    RamadanSeasonImage = promoObj.getString("SeasonImage");
                    RamadanCatergoryName = promoObj.getString("CategoryName");
                    RamadanCatergoryNameAr = promoObj.getString("CategoryName_Ar");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (MainFragmentNew.this.isVisible()) {
                    Log.d("TAG", "fragment visible");
                    new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL+"-1&userId="+userId);
                }
                else{
                    Log.d("TAG", "fragment not visible");
                }
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();

//        new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL+"-1&userId="+userId);
    }
}


