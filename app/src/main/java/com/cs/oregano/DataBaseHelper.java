package com.cs.oregano;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cs.oregano.model.Order;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 12-06-2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String LOGCAT = null;

    public DataBaseHelper(Context applicationcontext) {
        super(applicationcontext, "oregano.db", null, 5);
        Log.d(LOGCAT,"Created");
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String query,query1;
        query = "CREATE TABLE \"OrderTable\" (\"orderId\" INTEGER PRIMARY KEY  AUTOINCREMENT ,\"mainCategoryId\" INTEGER,\"subCategoryId\" INTEGER, \"itemId\" INTEGER,\"itemName\" VARCHAR,\"itemImage\"VARCHAR,\"description\" VARCHAR,\"itemTypeId\" INTEGER,\"itemPrice\" TEXT DEFAULT 0,\"itemPriceAd\" TEXT DEFAULT 0,\"additionalId\" TEXT DEFAULT 0,\"additionalName\" TEXT , \"additionalPrice\" TEXT DEFAULT 0, \"qty\" INTEGER DEFAULT 0,\"totalAmount\" TEXT DEFAULT 0,\"size\" TEXT DEFAULT 0,\"comment\" TEXT,\"itemNameAr\" VARCHAR,\"descriptionAr\" VARCHAR,\"additionalNameAr\" TEXT,\"nonDelivery\" TEXT)";

        database.execSQL(query);
    }
    @Override
    public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
        String query;
        query = "DROP TABLE IF EXISTS OrderTable";
        database.execSQL(query);        onCreate(database);
    }


    public void insertOrder(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("mainCategoryId", queryValues.get("mainCategoryId"));
        values.put("subCategoryId", queryValues.get("subCategoryId"));
        values.put("itemId", queryValues.get("itemId"));
        values.put("itemName", queryValues.get("itemName"));
        values.put("itemImage", queryValues.get("itemImage"));
        values.put("description", queryValues.get("description"));
        values.put("itemTypeId", queryValues.get("itemTypeId"));
        values.put("itemPrice", queryValues.get("itemPrice"));
        values.put("itemPriceAd", queryValues.get("itemPriceAd"));
        values.put("additionalId", queryValues.get("additionalId"));
        values.put("additionalName", queryValues.get("additionalName"));
        values.put("additionalPrice", queryValues.get("additionalPrice"));
        values.put("qty", queryValues.get("qty"));
        values.put("totalAmount", queryValues.get("totalAmount"));
        values.put("size", queryValues.get("size"));
        values.put("comment", queryValues.get("comment"));
        values.put("itemNameAr", queryValues.get("itemNameAr"));
        values.put("descriptionAr", queryValues.get("descriptionAr"));
        values.put("additionalNameAr", queryValues.get("additionalNameAr"));
        values.put("nonDelivery",queryValues.get("nonDelivery"));

        database.insert("OrderTable", null, values);
        database.close();
    }

    public void updateOrder(String qty, String price, String orderId){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("qty", qty);
        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET qty ="+qty+ ",totalAmount ="+ price +" where orderId ="+orderId;
        database.execSQL(updateQuery);
    }


    public ArrayList<Order> getOrderInfo(){
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setMainCategoryId(cursor.getString(1));
                sc.setSubCategoryId(cursor.getString(2));
                sc.setItemId(cursor.getString(3));
                sc.setItemName(cursor.getString(4));
                sc.setItemImage(cursor.getString(5));
                sc.setDescription(cursor.getString(6));
                sc.setItemTypeId(cursor.getString(7));
                sc.setItemPrice(cursor.getString(8));
                sc.setItemPriceAd(cursor.getString(9));
                sc.setAdditionalId(cursor.getString(10));
                sc.setAdditionalName(cursor.getString(11));
                sc.setAdditionalPrice(cursor.getString(12));
                sc.setQty(cursor.getString(13));
                sc.setTotalAmount(cursor.getString(14));
                sc.setSize(cursor.getString(15));
                sc.setComment(cursor.getString(16));
                sc.setItemNameAr(cursor.getString(17));
                sc.setDescriptionAr(cursor.getString(18));
                sc.setAdditionalNameAr(cursor.getString(19));
                sc.setNonDelivery(cursor.getString(20));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public ArrayList<Order> getItemOrderInfo(String itemId){
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable where itemId = "+itemId;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setMainCategoryId(cursor.getString(1));
                sc.setSubCategoryId(cursor.getString(2));
                sc.setItemId(cursor.getString(3));
                sc.setItemName(cursor.getString(4));
                sc.setItemImage(cursor.getString(5));
                sc.setDescription(cursor.getString(6));
                sc.setItemTypeId(cursor.getString(7));
                sc.setItemPrice(cursor.getString(8));
                sc.setItemPriceAd(cursor.getString(9));
                sc.setAdditionalId(cursor.getString(10));
                sc.setAdditionalName(cursor.getString(11));
                sc.setAdditionalPrice(cursor.getString(12));
                sc.setQty(cursor.getString(13));
                sc.setTotalAmount(cursor.getString(14));
                sc.setSize(cursor.getString(15));
                sc.setComment(cursor.getString(16));
                sc.setItemNameAr(cursor.getString(17));
                sc.setDescriptionAr(cursor.getString(18));
                sc.setAdditionalNameAr(cursor.getString(19));
                sc.setNonDelivery(cursor.getString(20));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public ArrayList<Order> getNonDeliveryItems() {
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable where nonDelivery=?;";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, new String[] {"false"});
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setMainCategoryId(cursor.getString(1));
                sc.setSubCategoryId(cursor.getString(2));
                sc.setItemId(cursor.getString(3));
                sc.setItemName(cursor.getString(4));
                sc.setItemImage(cursor.getString(5));
                sc.setDescription(cursor.getString(6));
                sc.setItemTypeId(cursor.getString(7));
                sc.setItemPrice(cursor.getString(8));
                sc.setItemPriceAd(cursor.getString(9));
                sc.setAdditionalId(cursor.getString(10));
                sc.setAdditionalName(cursor.getString(11));
                sc.setAdditionalPrice(cursor.getString(12));
                sc.setQty(cursor.getString(13));
                sc.setTotalAmount(cursor.getString(14));
                sc.setSize(cursor.getString(15));
                sc.setComment(cursor.getString(16));
                sc.setItemNameAr(cursor.getString(17));
                sc.setDescriptionAr(cursor.getString(18));
                sc.setAdditionalNameAr(cursor.getString(19));
                sc.setNonDelivery(cursor.getString(20));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public int getItemOrderCount(String itemId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where itemId="+itemId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getCategoryOrderCount(String categoryID){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where mainCategoryId="+categoryID;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public float getItemOrderPrice(String itemId){
        float cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(price) FROM  OrderTable where orderId="+itemId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getFloat(0);
        }
        return cnt;
    }

    public int getSubcatOrderCount(String subCategoryId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where subCategoryId="+subCategoryId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getTotalOrderQty(){
        int qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cur = database.rawQuery("SELECT SUM(qty) FROM OrderTable", null);

            if (cur.moveToFirst()) {
                qty = cur.getInt(0);
            }
        }catch (SQLiteException sqle){
            sqle.printStackTrace();
        }
        return qty;
    }

    public float getTotalOrderPrice(){
        float qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT SUM(totalAmount) FROM OrderTable", null);
        if(cur.moveToFirst())
        {
            qty  = cur.getFloat(0);
        }

        return qty;
    }


    public void deleteItemFromOrder(String orderId){
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where orderId = "+orderId;
        database.execSQL(updateQuery);

    }

    public void deleteItemFromOrderID(String orderId, String size) {
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where itemId = " + orderId + " AND itemTypeId =" + size;
        database.execSQL(updateQuery);

    }

    public void deleteOrderTable(){
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable";
        database.execSQL(updateQuery);
    }

    public void updateComment(String orderId, String comment){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET comment = '"+comment+"' where orderId ="+orderId;
        database.execSQL(updateQuery);
    }

}
