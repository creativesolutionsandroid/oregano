package com.cs.oregano;

import java.text.DecimalFormat;

/**
 * Created by CS on 09-06-2016.
 */
public class Constants {

//    static String live_url = "http://www.ircfood.com";
    static String live_url = "http://csadms.com/OreganoServices";
//    public static String IMAGE_URL = "http://csadms.com/OreganoServices/images/";

    public static String RAMADAN_PROMOTION_URL = live_url+"/api/RatingsApi?UserId=";
    public static String IMAGE_URL = "http://www.ircfood.com/images/";
    public static String CATEGORY_ITEMS_URL = live_url+"/api/SubCategoryApi/";
    public static String ADDITIONALS_URL = live_url+"/Api/ModifierInfoList/";
    public static String STORES_URL = live_url+"/api/StoreInformationApi/";
    public static String LOGIN_URL = live_url+"/api/VerifyUserCredentialsApi/";
    public static String REGISTRATION_URL = live_url+"/api/RegistrationApi";
    public static String INSERT_ORDER_URL = live_url+"/api/OrderDetailsApi";
    public static String ORDER_HISTORY_URL = live_url+"/api/OrderTrackApi/";
    public static String SAVE_ADDRESS_URL = live_url+"/api/RegistrationApi/";
    public static String TRACK_ORDER_STEPS_URL = live_url+"/api/OrderTrackApi?orderId=";
//    public static String TRACK_ORDER_STEPS_URL = live_url+"/api/OrderTrackApi/GetTrackOrder?orderId=-1&userId=1122";
    public static String SAVED_ADDRESS_URL = live_url+"/api/RegistrationApi/";
    public static String GET_FAVORITE_ORDERS_URL = live_url+"/api/FaviorateOrderApi/";
    public static String DELETE_FAVORITE_ORDERS_URL = live_url+"/api/FaviorateOrderApi/";
    public static String INSERT_FAVORITE_ORDER_URL = live_url+"/api/FaviorateOrderApi?OrderId=";
    public static String CHANGE_PASSWORD_URL = live_url+"/api/VerifyUserCredentialsApi/";
    public static String ORDERD_DETAILS_URL = live_url+"/api/OrderDetailsApi?OrderId=";
    public static String VERIFY_RANDOM_NUMBER_URL = live_url+"/api/RegistrationApi?MobileNo=";
    public static String GET_SAVED_CARDS_URL = live_url+"/api/CreditCardDetails?userId=";
    public static String SAVE_CARD_DETAILS_URL = live_url+"/api/CreditCardDetails?userId=";
    public static String FORGOT_PASSWORD_URL = live_url+"/api/RegistrationApi?MobileNo=";
    public static String RESET_PASSWORD_URL = live_url + "/api/VerifyUserCredentialsApi?UsrName=";
    public static String GET_CURRENT_TIME_URL = live_url+"/api/GetCurrentTime";
    public static String EDIT_ADDRESS_URL = live_url+"/api/RegistrationApi";
    public static String GET_MESSAGES_URL = live_url+"/api/PushMsg?userid=";
    public static String GET_OFFERS_URL = live_url+"/api/OfferDetails?offerId=";
    public static String UPDATE_MESSAGE_URL = live_url+"/api/PushMsg";
    public static String UPDATE_PROFILE_URL = live_url+"/api/VerifyUserCredentialsApi";
    public static String GET_BANNERS_URL = live_url+"/api/OfferDetails";
    public static String GET_PROMOS_URL = live_url+"/api/OreganoPromotions?userId=";
    public static String LIVE_TRACKING_URL = live_url+"/api/OrderTrackApi?OrderId=";
    public static String RATING_URL = live_url+"/api/RatingsApi";
    public static String DELETE_ORDER_FROM_HISTORY = live_url + "/api/OrderDetailsApi?Delete_OrderID=";

//    static String local_url = "http://192.168.1.126";

//    public static String CATEGORY_ITEMS_URL = local_url+"/OreganoLive/api/SubCategoryApi/";
//    public static String ADDITIONALS_URL = local_url+"/OreganoLive/Api/ModifierInfoList/";
//    public static String STORES_URL = local_url+"/OreganoLive/api/StoreInformationApi/saturday";
//    public static String LOGIN_URL = local_url+"/OreganoLive/api/VerifyUserCredentialsApi/";
//    public static String REGISTRATION_URL = local_url+"/OreganoLive/api/RegistrationApi";
//    public static String INSERT_ORDER_URL = local_url+"/OreganoLive/api/OrderDetailsApi";
//    public static String ORDER_HISTORY_URL = local_url+"/OreganoLive/api/OrderDetailsApi/";
//    public static String SAVE_ADDRESS_URL = local_url+"/OreganoLive/api/RegistrationApi/";
//    public static String TRACK_ORDER_STEPS_URL = local_url+"/OreganoLive/api/OrderTrackApi?orderId=";
//    public static String SAVED_ADDRESS_URL = local_url+"/OreganoLive/api/RegistrationApi/";
//    public static String GET_FAVORITE_ORDERS_URL = local_url+"/OreganoLive/Api/FaviorateOrderApi/";
//    public static String DELETE_FAVORITE_ORDERS_URL = local_url+"/OreganoLive/Api/FaviorateOrderApi/";
//    public static String INSERT_FAVORITE_ORDER_URL = local_url+"/OreganoLive/Api/FaviorateOrderApi?OrderId=";
//    public static String CHANGE_PASSWORD_URL = local_url+"/OreganoLive/api/VerifyUserCredentialsApi/";
//    public static String ORDERD_DETAILS_URL = local_url+"/OreganoLive/api/OrderDetailsApi?OrderId=";
//    public static String VERIFY_RANDOM_NUMBER_URL = local_url+"/OreganoLive/api/RegistrationApi?MobileNo=";
//    public static String GET_SAVED_CARDS_URL = local_url+"/OreganoLive/api/CreditCardDetails?userId=";
//    public static String SAVE_CARD_DETAILS_URL = local_url+"/OreganoLive/api/CreditCardDetails?userId=";
//    public static String FORGOT_PASSWORD_URL = local_url+"/OreganoLive/api/VerifyUserCredentialsApi?UsrName=";
//    public static String GET_CURRENT_TIME_URL = local_url+"/OreganoLive/api/GetCurrentTime";
//    public static String EDIT_ADDRESS_URL = local_url+"/OreganoLive/api/RegistrationApi";
//    public static String GET_MESSAGES_URL = local_url+"/OreganoLive/api/PushMsg?userid=";
//    public static String GET_OFFERS_URL = local_url+"/OreganoLive/api/OfferDetails?offerId=";
//    public static String UPDATE_MESSAGE_URL = local_url+"/OreganoLive/OreganoServices/api/PushMsg";
//    public static String UPDATE_PROFILE_URL = local_url+"/OreganoLive/api/VerifyUserCredentialsApi";
//    public static String GET_BANNERS_URL = local_url+"/OreganoLive/api/OfferDetails";

    public static String ORDER_TYPE = "";
    public static String COMMENTS = "";

    public static DecimalFormat decimalFormat =new DecimalFormat("0.00");

    public static String convertToArabic(String value)
    {
        String newValue = (value
                .replaceAll("١","1" ).replaceAll("٢","2" )
                .replaceAll("٣","3" ).replaceAll("٤","4" )
                .replaceAll("٥","5" ).replaceAll("٦","6" )
                .replaceAll("٧","7" ).replaceAll("٨","8")
                .replaceAll("٩","9" ).replaceAll("٠","0" )
                .replaceAll("٫",".").replaceAll(",","."));
        return newValue;
    }

//    public static String convertToArabic(String value)
//    {
//        String newValue = (value
//                .replaceAll("١","1" ).replaceAll("٢","2" )
//                .replaceAll("٣","3" ).replaceAll("٤","4" )
//                .replaceAll("٥","5" ).replaceAll("٦","6" )
//                .replaceAll("٧","7" ).replaceAll("٨","8")
//                .replaceAll("٩","9" ).replaceAll("٠","0" )
//                .replaceAll("٫","."));
//        return newValue;
//    }
}
