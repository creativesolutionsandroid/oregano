package com.cs.oregano;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class DrawerListAdapter extends ArrayAdapter<String> {

	Context context;
	int layoutResourceId;
	private String[] data;
	private int[] icons;
	int checkedPosition;
	String loggedinName, loggedinImage, language;

	/*private int[] unselected_icons = { R.drawable.ic_launcher, R.drawable.ic_home_black_24dp, 
			   R.drawable.ic_visibility_black_24dp, R.drawable.ic_favorite_black_24dp,
			   R.drawable.ic_info_black_24dp, R.drawable.ic_share_black_24dp,
			   R.drawable.ic_grade_black_24dp, R.drawable.logout };

			 private int[] unselected_strips = { R.drawable.transparent_strip,R.drawable.transparent_strip,
			   R.drawable.transparent_strip, R.drawable.transparent_strip,
			   R.drawable.transparent_strip, R.drawable.transparent_strip,
			   R.drawable.transparent_strip, R.drawable.transparent_strip };

			 private int[] selected_strips = { R.drawable.strip, R.drawable.strip,
			   R.drawable.strip, R.drawable.strip, R.drawable.strip,R.drawable.strip,
			   R.drawable.strip, R.drawable.strip };

			 private int[] selected_icons = { R.drawable.ic_launcher,
			   R.drawable.ic_home_black_24dp, R.drawable.ic_visibility_black_24dp,
			   R.drawable.ic_favorite_black_24dp, R.drawable.ic_info_black_24dp,
			   R.drawable.ic_share_black_24dp, R.drawable.ic_grade_black_24dp,
			   R.drawable.logout };*/

	public DrawerListAdapter(Context context, int layoutResourceId,
							 String[] data, int[] icons, String language) {

		super(context, layoutResourceId, data);

		/*type = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");*/

		this.data = data;
		this.icons = icons;
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.language = language;

		
	}

	public void setChecheckedPosition(int pos) {

		this.checkedPosition = pos;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder holder = null;
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();

		
			if (row == null) {

				row = inflater.inflate(layoutResourceId, parent, false);

				holder = new ViewHolder();

				holder.txtTitle = (TextView) row.findViewById(R.id.sidemenu_name);
			//	holder.txtTitle.setTypeface(type);

				// Log.e();

				row.setTag(holder);
			} else {
				holder = (ViewHolder) row.getTag();
			}

			/*if (position == checkedPosition) {
				holder.txtTitle.setTextColor(context.getResources().getColor(
						R.color.materail_dark_pink));
				holder.imgIcon.setImageResource(selected_icons[position]);
				holder.stripimage.setImageResource(selected_strips[position]);
			} else {
				holder.txtTitle.setTextColor(context.getResources().getColor(
						android.R.color.black));
				holder.imgIcon.setImageResource(unselected_icons[position]);
				holder.stripimage.setImageResource(unselected_strips[position]);
			}*/

		if(language.equalsIgnoreCase("En")) {
			holder.txtTitle.setText(data[position]);
			holder.txtTitle.setCompoundDrawablesWithIntrinsicBounds(icons[position], 0, 0, 0);
		}else if(language.equalsIgnoreCase("Ar")){
			holder.txtTitle.setText(data[position]);
			holder.txtTitle.setGravity(Gravity.RIGHT);
			holder.txtTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, icons[position], 0);
		}


		return row;
	}

	static class ViewHolder {

		RelativeLayout layout;
		ImageView imgIcon,stripimage;
		TextView txtTitle;
	}
}