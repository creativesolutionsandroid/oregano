package com.cs.oregano.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.GPSTracker;
import com.cs.oregano.JSONParser;
import com.cs.oregano.MainActivity;
import com.cs.oregano.PreparationTimeDBhelper;
import com.cs.oregano.R;
import com.cs.oregano.adapters.HorizontalListAdapter;
import com.cs.oregano.adapters.PromoAdapter;
import com.cs.oregano.fragments.OrderFragment;
import com.cs.oregano.model.HorizontalListView;
import com.cs.oregano.model.Order;
import com.cs.oregano.model.PreparationTime;
import com.cs.oregano.model.PromoType2;
import com.cs.oregano.model.Promos;
import com.cs.oregano.widgets.DateTimePicker;
import com.cs.oregano.widgets.NetworkUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobile.connect.PWConnect;
import com.mobile.connect.checkout.dialog.PWConnectCheckoutActivity;
import com.mobile.connect.checkout.meta.PWConnectCheckoutCreateToken;
import com.mobile.connect.checkout.meta.PWConnectCheckoutPaymentMethod;
import com.mobile.connect.checkout.meta.PWConnectCheckoutSettings;
import com.mobile.connect.exception.PWException;
import com.mobile.connect.exception.PWProviderNotInitializedException;
import com.mobile.connect.payment.PWAccount;
import com.mobile.connect.payment.PWCurrency;
import com.mobile.connect.payment.PWPaymentParams;
import com.mobile.connect.service.PWConnectService;
import com.mobile.connect.service.PWProviderBinder;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by CS on 15-06-2016.
 */
public class OrderConfirmation extends AppCompatActivity implements DateTimePicker.DateWatcher {
    Toolbar toolbar;
    TextView userName, mobileNo, storeNameTextView, storeAddressTextView, totalItems, totalAmount, estTime, netTotal, vatAmount, amount;
    TextView storeAddressTxt;
    TextView changeEstTime;
    TextView vatPercent, receipt_close;
    ImageView minvoice;
    HorizontalListView horizontalListView;
    HorizontalListAdapter mAdapter;
    private PromoAdapter mPromoAdapter;
    private ArrayList<Promos> promosList = new ArrayList<>();
    String promocodeStr = "No", promoIdStr = "", promoTypeStr = "";

    String expectedtime1, time1;
    public static float tax;

    DecimalFormat decim = new DecimalFormat();


    //    EditText promocode;
    float remainingBonusInt;
    TextView promocode;
    RelativeLayout promoLayout, recieptLayout;
    ImageView promoCancel, promoArrow;
    RadioButton onlinePayment, cashPayment;
    ImageView orderTypeImg;
    int totalItemsCount;
    int totalAmountVal;
    int storeDistance;
    float distanceKilometersFloat;
    int distanceKilometers;
    static final int TIME_DIALOG_ID = 1111;
    List<Date> listTimes;
    private int hour;
    private int minute;
    Context ctx;
    boolean checkStatus;
    private Handler handler = new Handler();
    boolean killHandler = false;
    Date oldTime = null;
    Calendar c;
    private String deviceToken;
    String userId;
    ProgressDialog dialog;
    Button confirmOrder, editOrder;
    int paymentMode = 3;
    String mobileNumberFormat = "";
    //    String promocodeStr = "No";
    String orderType, fullHours;
    String pickerViewOpen, exp_time_to_dumy;
    String endTime, openTimeStr, todayDate, tomorrowDate;
    String changeTimeIsYes = "false", payerTimeIsYes = "false", No_DistanceStr = "true";
    String expTimeTo;
    Button backBtn;
    boolean flag = true;
    int minuts, check_count, changeMints;
    String freeOrderId = "";
    private Timer timer = new Timer();
    private String expChangeStr = "0000";

    GPSTracker gps;
    private double lat, longi;


    private String setCardResponse = null;

    private String getCardResponse = null;

    private String timeResponse = null;
    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    private String distanceResponse = null;
    private int trafficMins;
    String secs;
    ArrayList<PreparationTime> timesList = new ArrayList<>();

    int PreparationTime = 0, TravelTime = 0;
    Date kitchenStartDate = null;

    SharedPreferences userPrefs;
    String response;
    private String storeId, storeName, storeAddress;
    private String address, addressLat, addressLong, addressId, landmark;
    private Double latitude, longitude;
    private GoogleMap map;
    MarkerOptions markerOptions;
    private DataBaseHelper myDbHelper;
    private PreparationTimeDBhelper myPrepTimeDbHelper;
    float vat = 5;

    ArrayList<Order> orderList = new ArrayList<>();
    ArrayList<PromoType2> promoType2ArrayList = new ArrayList<>();
    JSONObject promoObj = new JSONObject();
    JSONArray promoArray = new JSONArray();


    public static final String PREFS_NAME = "MCOMMERCE_SAMPLE";
    public static final String ACCOUNTS = "ACCOUNTS";

    private PWProviderBinder _binder;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            _binder = (PWProviderBinder) service;
            try {
                // replace by custom sandbox access
                _binder.initializeProvider(PWConnect.PWProviderMode.LIVE, "Hyperpay.Oregano.mcommerce", "a9ac6927646211e69325035d15b6ff20");
            } catch (PWException ee) {
                ee.printStackTrace();
            }
            Log.i("mainactivity", "bound to remote service...!");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            _binder = null;
        }
    };

    /**
     * A list of the stored accounts
     */
    private List<PWAccount> accounts = new ArrayList<>();

    /**
     * Reference to the preferences where the accounts are stored
     */
    private SharedPreferences sharedSettings;

    SharedPreferences languagePrefs;
    String language;

    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefsEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order_confirmation1);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.order_confirmation_arabic);
        }


        // start the PWConnect service
        startService(new Intent(OrderConfirmation.this, PWConnectService.class));
        bindService(new Intent(OrderConfirmation.this, PWConnectService.class), serviceConnection, Context.BIND_AUTO_CREATE);

        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor = orderPrefs.edit();
        sharedSettings = getSharedPreferences(PREFS_NAME, 0);
        storeAddressTxt = (TextView) findViewById(R.id.store_address_txt);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        response = userPrefs.getString("user_profile", null);

        myDbHelper = new DataBaseHelper(OrderConfirmation.this);
        myPrepTimeDbHelper = new PreparationTimeDBhelper(OrderConfirmation.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        orderList = myDbHelper.getOrderInfo();

        orderType = Constants.ORDER_TYPE;

        try {

            myPrepTimeDbHelper.createDataBase();

        } catch (IOException ioe) {


        }

        try {

            myPrepTimeDbHelper.openDataBase();

        } catch (SQLException sqle) {
            throw sqle;
        }

        storeId = getIntent().getExtras().getString("storeId");
        storeName = getIntent().getExtras().getString("storeName");
        storeAddress = getIntent().getExtras().getString("storeAddress");
        latitude = getIntent().getExtras().getDouble("latitude");
        longitude = getIntent().getExtras().getDouble("longitude");
        fullHours = getIntent().getExtras().getString("full_hours");

        userName = (TextView) findViewById(R.id.user_name);
        mobileNo = (TextView) findViewById(R.id.mobile_number);
        storeNameTextView = (TextView) findViewById(R.id.store_detail_name);
        storeAddressTextView = (TextView) findViewById(R.id.store_detail_address);
        onlinePayment = (RadioButton) findViewById(R.id.online_payment);
        cashPayment = (RadioButton) findViewById(R.id.cash_on_pickup);
        totalItems = (TextView) findViewById(R.id.totalItems);
        totalAmount = (TextView) findViewById(R.id.totalAmount);
        estTime = (TextView) findViewById(R.id.estTime);
        orderTypeImg = (ImageView) findViewById(R.id.order_type_img);
//        promocode = (EditText) findViewById(R.id.promo_code);
        changeEstTime = (TextView) findViewById(R.id.change_esttime);
        confirmOrder = (Button) findViewById(R.id.confirm_order_btn);
        editOrder = (Button) findViewById(R.id.edit_order_btn);
        promoLayout = (RelativeLayout) findViewById(R.id.promo_layout);
        promoCancel = (ImageView) findViewById(R.id.promo_cancel);
        promoArrow = (ImageView) findViewById(R.id.promo_arrow);
        promocode = (TextView) findViewById(R.id.promo_code);

        recieptLayout = (RelativeLayout) findViewById(R.id.receipt_layout);
        receipt_close = (TextView) findViewById(R.id.receipt_close);
        amount = (TextView) findViewById(R.id.amount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        vatPercent = (TextView) findViewById(R.id.vatPercent);
        netTotal = (TextView) findViewById(R.id.net_total);
        minvoice = (ImageView) findViewById(R.id.invoice);

        horizontalListView = (HorizontalListView) findViewById(R.id.horizontal_listview);
        mAdapter = new HorizontalListAdapter(OrderConfirmation.this, orderList, language);
        mPromoAdapter = new PromoAdapter(OrderConfirmation.this, promosList);
        horizontalListView.setAdapter(mAdapter);
        listTimes = new ArrayList<>();

        tax = myDbHelper.getTotalOrderPrice() * (vat / 100);

        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieptLayout.setVisibility(View.GONE);
            }
        });


        minvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recieptLayout.setVisibility(View.VISIBLE);
            }
        });
        final DecimalFormat decim = new DecimalFormat("0.00");
        amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
        vatAmount.setText("" + decim.format(tax));
        netTotal.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));

        if (response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                userName.setText(userObjuect.getString("fullName"));
                mobileNo.setText("+" + userObjuect.getString("mobile"));
                mobileNumberFormat = userObjuect.getString("mobile");
            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
        if (orderType.equalsIgnoreCase("Delivery")) {
            storeName = getIntent().getExtras().getString("your_address");
            storeAddress = getIntent().getExtras().getString("landmark");
            if (language.equalsIgnoreCase("En")) {
                storeAddressTxt.setText("Your Address");
                orderTypeImg.setImageResource(R.drawable.delivery_txt);
            } else if (language.equalsIgnoreCase("Ar")) {
                storeAddressTxt.setText("عنوانك");
                orderTypeImg.setImageResource(R.drawable.delivery_txt_arabic);
            }
            addressId = getIntent().getExtras().getString("address_id");
            if (addressId == null || addressId.equalsIgnoreCase("")) {
                Toast.makeText(OrderConfirmation.this, "Please select a delivery address", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
            storeNameTextView.setTextSize(12);
            storeAddressTextView.setTextSize(12);
        }
        if (orderType.equalsIgnoreCase("Carryout")) {
            if (language.equalsIgnoreCase("En")) {
                orderTypeImg.setImageResource(R.drawable.carry_txt);
            } else if (language.equalsIgnoreCase("Ar")) {
                storeAddressTxt.setText("عنوانك");
                orderTypeImg.setImageResource(R.drawable.carry_txt_arabic);
            }
        }
        if (language.equalsIgnoreCase("En")) {
            totalItems.setText("" + myDbHelper.getTotalOrderQty());
        } else if (language.equalsIgnoreCase("Ar")) {
            horizontalListView.setSelection(orderList.size() - 1);
            totalItems.setText("" + myDbHelper.getTotalOrderQty());
        }
        storeNameTextView.setText(WordUtils.capitalizeFully(storeName));
        if (!storeAddress.equals("null")) {
            storeAddressTextView.setText(WordUtils.capitalizeFully(storeAddress));
        } else {
            storeAddressTextView.setText("");
        }
        totalItemsCount = myDbHelper.getTotalOrderQty();


        amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
        vatAmount.setText("" + decim.format(tax));
        netTotal.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
        totalAmount.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        gps = new GPSTracker(OrderConfirmation.this);
        if (gps.canGetLocation()) {

            lat = gps.getLatitude();
            longi = gps.getLongitude();
            // \n is for new line
//            Toast.makeText(getActivity(), "Your Location is - \nLat: " + lat + "\nLong: " + longi, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }

      if (totalItems.getText().toString() == "0"){

          getSupportActionBar().setDisplayShowTitleEnabled(false);
          getSupportActionBar().setDisplayHomeAsUpEnabled(false);
      }

        changeEstTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pickerViewOpen = "open";
                exp_time_to_dumy = "";
                changeExpTimeMethod();
                //showDialog(TIME_DIALOG_ID);
//                new TimePickerDialog(ctx, timePickerListener, hour, minute,
//                        false).show();

            }

        });

        onlinePayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!totalAmount.getText().toString().equalsIgnoreCase("free")) {
                    if (isChecked)
                        cashPayment.setChecked(false);
                    paymentMode = 3;
                } else {
                    onlinePayment.setChecked(false);
                }
            }
        });

        cashPayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    onlinePayment.setChecked(false);
                paymentMode = 2;
            }
        });

        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmOrder.setClickable(false);
                Log.i("TAG", "mode " + Integer.toString(paymentMode));
                if (paymentMode == 3) {
                    Calendar now = Calendar.getInstance();
                    Intent i = new Intent(OrderConfirmation.this, PWConnectCheckoutActivity.class);
                    PWConnectCheckoutSettings settings = null;
                    PWPaymentParams genericParams = null;

                    try {
                        if (mobileNumberFormat == null) {
                            mobileNumberFormat = "";
                        } else {
                            mobileNumberFormat = mobileNumberFormat + "@oregano.com";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {

                        float total_amount = Float.parseFloat(totalAmount.getText().toString().replace(" SR", ""));
                        // configure amount, currency, and subject of the transaction
                        genericParams = _binder.getPaymentParamsFactory().createGenericPaymentParams(Double.parseDouble(String.valueOf(total_amount)), PWCurrency.SAUDI_ARABIA_RIYAL, "test subject");
                        // configure payment params with customer data
//                        genericParams.setCustomerGivenName("Aliza");
//                        genericParams.setCustomerFamilyName("Foo");
//                        genericParams.setCustomerAddressCity("Sampletown");
//                        genericParams.setCustomerAddressCountryCode("SA");
//                        genericParams.setCustomerAddressState("PA");
//                        genericParams.setCustomerAddressStreet("123 Grande St");
//                        genericParams.setCustomerAddressZip("1234");
                        genericParams.setCustomerEmail(mobileNumberFormat);
//                        genericParams.setCustomerIP("255.0.255.0");
                        genericParams.setCustomIdentifier(storeId + now.get(Calendar.YEAR) + now.get(Calendar.MONTH) + now.get(Calendar.DATE) + estTime.getText().toString().replace(" ", "").replace(":", ""));


                        // create the settings for the payment screens
                        settings = new PWConnectCheckoutSettings();
                        settings.setHeaderDescription("Food & Beverages");
                        settings.setHeaderIconResource(R.drawable.ic_launcher);
//                        settings.setPaymentVATAmount(4.5);
                        settings.setSupportedDirectDebitCountries(new String[]{"SA"});
                        settings.setSupportedPaymentMethods(new PWConnectCheckoutPaymentMethod[]{PWConnectCheckoutPaymentMethod.VISA, PWConnectCheckoutPaymentMethod.MASTERCARD,
                                PWConnectCheckoutPaymentMethod.DIRECT_DEBIT});
                        // ask the user if she wants to store the account
                        settings.setCreateToken(PWConnectCheckoutCreateToken.PROMPT);

                        // retrieve the stored accounts from the settings
//                        accounts = _binder.getAccountFactory().deserializeAccountList(sharedSettings.getString(ACCOUNTS, _binder.getAccountFactory().serializeAccountList(new ArrayList<PWAccount>())));
                        settings.setStoredAccounts(accounts);

                        i.putExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_SETTINGS, settings);
                        i.putExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_GENERIC_PAYMENT_PARAMS, genericParams);
                        startActivityForResult(i, PWConnectCheckoutActivity.CONNECT_CHECKOUT_ACTIVITY);
                    } catch (PWException e) {
                        Log.e("connect", "error creating the payment page", e);
                    }
                } else {
                    new InsertOrder().execute();
                }
            }
        });

        editOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderConfirmation.this, MainActivity.class);
                intent.putExtra("startWith", 2);
                startActivity(intent);
            }
        });

        promoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetPromocodeResponse().execute(Constants.GET_PROMOS_URL + userId + "&DToken=" + SplashActivity.regid);
            }
        });

        promoCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promoLayout.setClickable(true);
                promocode.setText("");
                promoArrow.setVisibility(View.VISIBLE);
                promoCancel.setVisibility(View.GONE);
                promocode.setHint("Apply Promotion");
                totalItems.setText("" + myDbHelper.getTotalOrderQty());

                DecimalFormat decim = new DecimalFormat("0.00");
                amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
                float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
                vatAmount.setText("" + decim.format(tax));
                netTotal.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                totalAmount.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                promoTypeStr = "";
                remainingBonusInt = 0;
                if (language.equalsIgnoreCase("En")) {
                    promocode.setHint("Apply Promotion");
                } else if (language.equalsIgnoreCase("Ar")) {
                    promocode.setHint("قدم للعرض");
                }
            }
        });

        new getTrafficTime().execute();

        timer.schedule(new MyTimerTask(), 30000, 30000);
        new GetStoredCards().execute(Constants.GET_SAVED_CARDS_URL + userId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDateChanged(Calendar c) {

    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new GetCurrentTime().execute();
                }
            });
        }
    }

    public void changeExpTimeMethod() {
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH/mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat4 = new SimpleDateFormat("dd/MM/yyyy/HH/mm", Locale.US);
        SimpleDateFormat timeFormat5 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        payerTimeIsYes = "false";
        String st_time;
        String ed_time;
        final String st_time1, ed_time1;
        if (fullHours.equalsIgnoreCase("true")) {
            Date current24Date = null, currentServerDate = null, currentServerDate1;
            try {
                current24Date = timeFormat.parse(timeResponse);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String currentTime = timeFormat1.format(current24Date);
            try {
                currentServerDate = timeFormat1.parse(currentTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar now = Calendar.getInstance();
            now.setTime(currentServerDate);
            now.add(Calendar.MINUTE, changeMints);
            currentServerDate = now.getTime();

            Calendar now1 = Calendar.getInstance();
            now1.setTime(currentServerDate);
            now1.add(Calendar.MINUTE, changeMints - 1);
            currentServerDate1 = now1.getTime();

            st_time = timeFormat1.format(currentServerDate);
            ed_time = timeFormat1.format(currentServerDate1);
            st_time = todayDate + "/" + st_time;
            ed_time = tomorrowDate + "/" + ed_time;
        } else {
            endTime = getIntent().getExtras().getString("end_time");
            endTime = endTime;
            if (endTime.equals("12:00AM")) {
                endTime = "11:59PM";
            }
            openTimeStr = getIntent().getExtras().getString("start_time");
            Date date21 = null, date1 = null, date2 = null;
            try {
//            date21 = timeFormat3.parse(endTime);
                date1 = timeFormat5.parse(openTimeStr);
                date2 = timeFormat5.parse(endTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date current24Date = null, currentServerDate = null, currentServerDate1;
            try {
                current24Date = timeFormat.parse(timeResponse);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String currentTime = timeFormat1.format(current24Date);
            try {
                currentServerDate = timeFormat1.parse(currentTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar now = Calendar.getInstance();
            now.setTime(currentServerDate);
            now.add(Calendar.MINUTE, changeMints);
            currentServerDate = now.getTime();

            st_time = timeFormat1.format(currentServerDate);
            ed_time = timeFormat1.format(date2);
            Date st_time2Date = null;
            try {
                st_time2Date = timeFormat1.parse(st_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String ETimeString = timeFormat2.format(date2);
            String[] parts = ETimeString.split(":");
            int endHour = Integer.parseInt(parts[0]);
            int endMinute = Integer.parseInt(parts[1]);

            String st_time2str = timeFormat2.format(st_time2Date);
            String[] parts1 = st_time2str.split(":");
            int startHour = Integer.parseInt(parts1[0]);
            int startMinute = Integer.parseInt(parts1[1]);

            String CTimeString = timeFormat2.format(current24Date);
            String[] parts2 = CTimeString.split(":");
            int currentHour = Integer.parseInt(parts2[0]);
            int currentMinute = Integer.parseInt(parts2[1]);

            if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 || 0 > 59)))) {
                if (startHour < 0 || (startHour > 5 || (startHour == 5 && (startMinute > 59 || 0 > 59)))) {
                    st_time = todayDate + "/" + st_time;
                    ed_time = todayDate + "/" + ed_time;
                } else {
                    st_time = tomorrowDate + "/" + st_time;
                    ed_time = todayDate + "/" + ed_time;
                }
            } else {
                if (currentHour < 0 || (currentHour > 5 || (currentHour == 5 && (currentMinute > 59 || 0 > 59)))) {
                    st_time = todayDate + "/" + st_time;
                    ed_time = tomorrowDate + "/" + ed_time;
                } else {
                    st_time = todayDate + "/" + st_time;
                    ed_time = todayDate + "/" + ed_time;
                }
            }

        }
        st_time1 = st_time;
        ed_time1 = ed_time;

        Date st_time2Date = null;
        try {
            st_time2Date = timeFormat4.parse(st_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String st_time2str = timeFormat2.format(st_time2Date);
        String[] parts1 = st_time2str.split(":");
        int startHour = Integer.parseInt(parts1[0]);
        int startMinute = Integer.parseInt(parts1[1]);
//        new TimePickerDialog(ctx, timePickerListener, startHour, startMinute,
//                false).show();


        final Dialog mDateTimeDialog = new Dialog(OrderConfirmation.this);
        // Inflate the root layout
        final RelativeLayout mDateTimeDialogView = (RelativeLayout) getLayoutInflater().inflate(R.layout.date_time_picker, null);
        // Grab widget instance
        final DateTimePicker mDateTimePicker = (DateTimePicker) mDateTimeDialogView.findViewById(R.id.DateTimePicker);
        mDateTimePicker.initData(st_time2Date);
        mDateTimePicker.setDateChangedListener(this);

        // Update demo edittext when the "OK" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime)).setOnClickListener(new View.OnClickListener
                () {
            public void onClick(View v) {
                SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MMM/yyyy/HH/mm", Locale.US);
                SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy/HH/mm", Locale.US);
                SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
                SimpleDateFormat timeFormat6 = new SimpleDateFormat("HH:mm:ss", Locale.US);
                SimpleDateFormat timeFormat3 = new SimpleDateFormat("HH:mm", Locale.US);
                SimpleDateFormat timeFormat4 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                SimpleDateFormat timeFormat5 = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                SimpleDateFormat df5 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                mDateTimePicker.clearFocus();
                String result_string = mDateTimePicker.getDay() + "/" + String.valueOf(mDateTimePicker.getMonth()) + "/" + String.valueOf(mDateTimePicker.getYear())
                        + "/" + String.valueOf(mDateTimePicker.getHour()) + "/" + String.valueOf(mDateTimePicker.getMinute());
//                edit_text.setText(result_string);
                Date pickerDate = null, pickerDate1 = null, stDate = null, edDate = null;
                String storeST, storeET;
                try {
                    pickerDate1 = timeFormat5.parse(timeResponse);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                storeST = getIntent().getExtras().getString("start_time");
                storeET = getIntent().getExtras().getString("end_time");
                try {
                    pickerDate = timeFormat.parse(result_string);
                    Log.i("TAG", "st_time " + storeST);
                    Log.i("TAG", "ed_time " + storeET);
                    stDate = df5.parse(storeST);
                    edDate = df5.parse(storeET);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (pickerDate.after(pickerDate1) && pickerDate.before(edDate)) {
                    for (Date d : listTimes) {
                        Log.i("CHANGE TIME", "FOR");
                        Date prayerDate = d;
                        Date prayerEndTime;
                        Calendar prayerEnd = Calendar.getInstance();
                        prayerEnd.setTime(d);
                        String estdate = timeFormat4.format(prayerEnd.getTime());
                        prayerEnd.add(Calendar.MINUTE, 20);
                        prayerEndTime = prayerEnd.getTime();
                        String payerString = timeFormat3.format(prayerDate);
                        String payerEndString = timeFormat3.format(prayerEndTime);
                        String CTimeString = timeFormat3.format(pickerDate);
                        String[] startParts = payerString.split(":");
                        String[] endParts = payerEndString.split(":");
                        String[] currentParts = CTimeString.split(":");
                        int startHourInteger = Integer.parseInt(startParts[0]);
                        int startMintInteger = Integer.parseInt(startParts[1]);
                        int endHourInteger = Integer.parseInt(endParts[0]);
                        int endMintInteger = Integer.parseInt(endParts[1]);
                        int currentHour = Integer.parseInt(currentParts[0]);
                        int currentMinute = Integer.parseInt(currentParts[1]);
                        int c = (int) (currentHour * 60) + (int) currentMinute;
                        int p = (int) (startHourInteger * 60) + (int) startMintInteger - (int) 5;
                        int f = (int) (endHourInteger * 60) + (int) endMintInteger;
                        if (c > p && c < f) {
                            Log.i("CHANGE TIME", "C>P");
                            expTimeTo = timeFormat2.format(prayerEndTime);
                            Log.i("TAG", "sec " + timeFormat6.format(prayerEndTime));
                            String time1 = timeFormat6.format(prayerEndTime);
                            expectedtime1 = estdate + expTimeTo;
                            estTime.setText(estdate + " " + expTimeTo);
                            Log.i("TAG","exptime1 "+estdate + " " + expTimeTo);
                            payerTimeIsYes = "true";
                            expChangeStr = expTimeTo;
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));

                            if (language.equalsIgnoreCase("En")) {
                                // set title
                                alertDialogBuilder.setTitle("Oregano");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("It's prayer time from " + payerString + " to " + payerEndString + " we are closed. Please select any other time (or) your order will be considered after prayer time")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                            } else if (language.equalsIgnoreCase("Ar")) {
                                alertDialogBuilder.setTitle("اوريجانو");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("اوقات الصلاة من %@ الى %@ الفرع مغلق الان . من فضلك حدد اي وقت اخر (أو) سيتم تنفيذ طلبك" + payerEndString + " د ا " + payerString + " ء الصلاة مباشرة")
                                        .setCancelable(false)
                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                            }


                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();
                        }
                    }

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(pickerDate);
                    int prep = PreparationTime + TravelTime;
                    calendar.add(Calendar.MINUTE, -prep);
                    kitchenStartDate = calendar.getTime();
                    SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                    Log.i("TAG", "PreparationTime " + PreparationTime);
                    Log.i("TAG", "TravelTime " + TravelTime);
                    Log.i("TAG", "kitchenStartDate " + df3.format(kitchenStartDate));
//                    Log.i("TAG","PrayingTime " + PrayingTime);
                    if (payerTimeIsYes.equals("false")) {
                        expTimeTo = df5.format(pickerDate);
                        expChangeStr = expTimeTo;
                        SimpleDateFormat estdate = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        String date = estdate.format(pickerDate);
                        String time = timeFormat2.format(pickerDate);
                        expChangeStr = expTimeTo;
                        SimpleDateFormat time24 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
                        expectedtime1 = df5.format(pickerDate);
                        estTime.setText(date + " " + time);
                        Log.i("TAG","exptime2 "+date + " " + time);
                        No_DistanceStr = "false";
                        payerTimeIsYes = "false";
                        changeTimeIsYes = "true";
                    }

                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Oregano");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Order can't be processed for selected time, Please select different time.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }


                mDateTimeDialog.dismiss();
            }
        });

        // Cancel the dialog when the "Cancel" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDateTimeDialog.cancel();
            }
        });

        // Reset Date and Time pickers when the "Reset" button is clicked

//        ((Button) mDateTimeDialogView.findViewById(R.id.ResetDateTime)).setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                mDateTimePicker.reset();
//            }
//        });

        // Setup TimePicker
        // No title on the dialog window
        mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Set the dialog content view
        mDateTimeDialog.setContentView(mDateTimeDialogView);
        // Display the dialog
        mDateTimeDialog.show();

    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;
            updateTime(hour, minute, 1);

        }

    };


    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTime(int hours, int mins, int atTime) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        Date parseD = doParse(aTime);
        if (hours < 6 && timeSet.equals("AM") || hours == 12 && timeSet.equals("AM")) {
            Toast.makeText(ctx, "At this time not available estimaton time and order requests", Toast.LENGTH_SHORT).show();
            return;
        }
        if (atTime == 1)
//            if(oldTime!=null){
////                int diff = getTimeCaluculation();
//                Log.v("srinu", "old time not null: "+diff);
//                if(diff<0){
//                    Toast.makeText(ctx,"Please select future time",Toast.LENGTH_SHORT).show();
//                    return;
//                }
//            }
        /*if(atTime == 1||atTime == 2){
            oldTime = parseD;


            Log.v("srinu", "old time: ");
        }else{
            Log.v("srinu", "old time nnnn: ");
        }*/
            oldTime = parseD;
        checkStatus = false;

        Calendar cal = Calendar.getInstance();
        cal.setTime(parseD);
//        for(Date itemD : listTimes){
//            Log.v("srinu", "item cal: " + getTimeCaluculation());
//            int calC = getTimeCaluculation();
//            if(calC<=20&&calC>0){
//                checkStatus = true;
//                if(atTime == 1||atTime == 2)
//                    cal.add(Calendar.MINUTE,20);
//                //showAlert("Prayer time");
//            }
//        }
        if (atTime != 0) {
            if (!checkStatus) {
                //estTime.setText(getTimeStr(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE)));
                //showAlert("There are no prayers found at this time");
            }
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df5 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat time = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat time4 = new SimpleDateFormat("HH:mm", Locale.US);
        String exp_date = df5.format(c.getTime());
        expectedtime1 = exp_date + getTimeStr(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));

        String time2 = getTimeStr(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
        Date time1 = null;

        try {
            time1 = time4.parse(time2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String time3 = time.format(time1);
        estTime.setText(exp_date + " " + time3);
        Log.i("TAG","exptime3 "+exp_date + " " + time3);

        /*if(timeSet.equals("AM")){
            Toast.makeText(getApplicationContext(),"Please select time between 12:00 PM to 11:59 PM",Toast.LENGTH_SHORT).show();
        }
        else{
            btnClick.setText(aTime);
        }*/
    }

    public void getTimeCaluculation() {
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat4 = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
        SimpleDateFormat df5 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        Date current24Date = null, currentServerDate = null;
        try {
            current24Date = timeFormat.parse(timeResponse);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String currentTime = timeFormat.format(current24Date);
        try {
            currentServerDate = timeFormat.parse(currentTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (changeTimeIsYes.equals("false")) {
            Log.i("CHANGE TIME", "YES");
            minuts = 0;
            check_count = 0;
            changeMints = 0;
            check_count = myDbHelper.getTotalOrderQty();
            if (No_DistanceStr.equals("true")) {
//                int dist = distanceKilometers + 1;
//                minuts = minuts + dist;

//                    int pizzaCount = 0;
//                    for(Order order: orderList){
//                        if(order.getMainCategoryId().equals("7")|| order.getMainCategoryId().equals("8") || order.getMainCategoryId().equals("4") || order.getMainCategoryId().equals("6")){
//                            pizzaCount = pizzaCount + Integer.parseInt(order.getQty());
//                        }
//                    }
//                    if(pizzaCount == 0){
//                        Log.i("TAG0", ""+pizzaCount);
//                        check_count = (check_count * 90 / 60) + 1;
//                    }else if(pizzaCount <= 2){
//                        if(myDbHelper.getTotalOrderQty() > pizzaCount){
//                            int remainQty = myDbHelper.getTotalOrderQty() - pizzaCount;
//
//                            if((remainQty * 90 / 60) > 10){
//                                check_count = (remainQty * 90 / 60) + 1;
//                            }else{
//                                check_count = 10 + 1;
//                            }
//
//                        }else {
//                            check_count = 10 + 1;
//                        }
//                    }else if(pizzaCount >2 && pizzaCount <= 4){
//                        if(myDbHelper.getTotalOrderQty() > pizzaCount){
//                            int remainQty = myDbHelper.getTotalOrderQty() - pizzaCount;
//                            if((remainQty * 90 / 60) > 15){
//                                check_count = (remainQty * 90 / 60) + 1;
//                            }else{
//                                check_count = 15 + 1;
//                            }
//                        }else {
//                            Log.i("TAG48", "" + pizzaCount);
//
//                            check_count = 15 + 1;
//                        }
//                    }else if( pizzaCount > 4){
//                        if(myDbHelper.getTotalOrderQty() > pizzaCount){
//                            int remainQty = myDbHelper.getTotalOrderQty() - pizzaCount;
//                            if((remainQty * 90 / 60) > 20){
//                                check_count = (remainQty * 90 / 60) + 1;
//                            }else{
//                                check_count = 20 + 1;
//                            }
//                        }else {
//                            Log.i("TAG8", "" + pizzaCount);
//                            check_count = 20 + 1;
//                        }
//                    }

                int QtyStr = 0;
                float prepareTimeCount = 0;

                for (int i = 1; i <= 11; i++) {
                    QtyStr = myDbHelper.getCategoryOrderCount(Integer.toString(i));
//                for(Order order:orderList){

//                    QtyStr = Integer.parseInt(order.getQty());
                    if (QtyStr != 0) {
                        timesList = myPrepTimeDbHelper.getPrepTime(Integer.toString(i));

                        Log.i("TAG", "QtyStr " + QtyStr);
                        float prepareTime = 0;
                        if (QtyStr == 1) {
                            prepareTime = Float.parseFloat(timesList.get(0).getQty1());
                        } else if (QtyStr == 2) {
                            prepareTime = Float.parseFloat(timesList.get(0).getQty2());
                        } else if (QtyStr == 3) {
                            prepareTime = Float.parseFloat(timesList.get(0).getQty3());
                        } else if (QtyStr == 4) {
                            prepareTime = Float.parseFloat(timesList.get(0).getQty4());
                        } else if (QtyStr == 5) {
                            prepareTime = Float.parseFloat(timesList.get(0).getQty5());
                        } else if (QtyStr > 5) {
                            prepareTime = Float.parseFloat(timesList.get(0).getQty5Plus());
                        }

                        Log.i("TAG", "prepareTime " + prepareTime);

                        prepareTimeCount = (prepareTimeCount > prepareTime) ? prepareTimeCount : prepareTime;

                        PreparationTime = (int) prepareTimeCount + 1;

                        Log.i("TAG", "prepareTimeCount " + prepareTimeCount);
                    }
                }

                check_count = (int) Math.ceil(prepareTimeCount);

                Log.i("TAG", "check_count " + check_count);
//                Toast.makeText(getApplicationContext(), "traffic "+trafficMins+" prep "+check_count, Toast.LENGTH_SHORT).show();


                TravelTime = trafficMins;
                Log.i("TAG", "traveltime " + TravelTime);
                if (orderType.equalsIgnoreCase("Delivery")) {
                    minuts = trafficMins;
                    Log.i("TAG", "trafficMins " + trafficMins);
//                    changeMints = check_count;
                    if (minuts <= 5) {
                        minuts = 5;
                    }
                    minuts = minuts + check_count;
                    changeMints = minuts;
                } else {
                    minuts = trafficMins;
                    changeMints = check_count;
                    if (minuts <= 5) {
                        minuts = 5;
                    }
                    if (minuts < check_count) {
                        minuts = check_count;
                    }

                }

            }

            Calendar now = Calendar.getInstance();
            now.add(Calendar.MINUTE,1);
            kitchenStartDate = now.getTime();
            SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            Log.i("TAG","kitchen time "+df3.format(kitchenStartDate));
            now.setTime(currentServerDate);
            now.add(Calendar.MINUTE, minuts);
            currentServerDate = now.getTime();
            for (Date d : listTimes) {
                Log.i("CHANGE TIME", "FOR");
                Date prayerDate = d;
                Date prayerEndTime;
                Calendar prayerEnd = Calendar.getInstance();
                prayerEnd.setTime(d);
                prayerEnd.add(Calendar.MINUTE, 20);
                prayerEndTime = prayerEnd.getTime();
                String payerString = timeFormat1.format(prayerDate);
                String payerEndString = timeFormat1.format(prayerEndTime);
                String CTimeString = timeFormat1.format(currentServerDate);
                String[] startParts = payerString.split(":");
                String[] endParts = payerEndString.split(":");
                String[] currentParts = CTimeString.split(":");
                int startHourInteger = Integer.parseInt(startParts[0]);
                int startMintInteger = Integer.parseInt(startParts[1]);
                int endHourInteger = Integer.parseInt(endParts[0]);
                int endMintInteger = Integer.parseInt(endParts[1]);
                int CTHourInteger = Integer.parseInt(currentParts[0]);
                int CTMintInteger = Integer.parseInt(currentParts[1]);
                int c = (int) (CTHourInteger * 60) + (int) CTMintInteger;
                int p = (int) (startHourInteger * 60) + (int) startMintInteger - (int) 5;
                int f = (int) (endHourInteger * 60) + (int) endMintInteger;
                if (c > p && c < f) {
                    Log.i("CHANGE TIME", "C>P");
                    expTimeTo = df5.format(prayerEndTime);
                    Calendar e = Calendar.getInstance();
                    SimpleDateFormat estdate = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                    String exp_date = estdate.format(e.getTime());
                    String date = estdate.format(prayerEndTime);
                    String time = timeFormat2.format(prayerEndTime);
                    expectedtime1 = df5.format(prayerEndTime);
                    estTime.setText(date + " " + time);
                    Log.i("TAG","exptime4 "+date + " " + time);
                    payerTimeIsYes = "true";
                }
            }
            if (payerTimeIsYes.equals("false")) {
                expTimeTo = df5.format(currentServerDate);
                Calendar c = Calendar.getInstance();
                SimpleDateFormat estdate = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                String exp_date = estdate.format(c.getTime());
                String date = estdate.format(currentServerDate);
                String time = timeFormat2.format(currentServerDate);
                expectedtime1 = df5.format(currentServerDate);
                estTime.setText(date + " " + time);
                Log.i("TAG","exptime5 "+date + " " + time);
            }
        } else {
            Log.i("CHANGE TIME", "NO");
            Date prayerDate = null;
            try {
                prayerDate = df5.parse(expTimeTo);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                String payerString = timeFormat1.format(prayerDate);
                String CTimeString = timeFormat1.format(currentServerDate);
                String[] prayerParts = payerString.split(":");
                String[] currentParts = CTimeString.split(":");
                int startHourInteger = Integer.parseInt(prayerParts[0]);
                int startMintInteger = Integer.parseInt(prayerParts[1]);
                int CTHourInteger = Integer.parseInt(currentParts[0]);
                int CTMintInteger = Integer.parseInt(currentParts[1]);
                int c = (int) (CTHourInteger * 60) + (int) CTMintInteger + minuts;
                int p = (int) (startHourInteger * 60) + (int) startMintInteger;
                if (c >= p) {
                    changeTimeIsYes = "false";
                    getTimeCaluculation();
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }


        String startTime = expTimeTo;
        if (fullHours.equalsIgnoreCase("true")) {
            return;
        }

        openTimeStr = getIntent().getExtras().getString("start_time");
        endTime = getIntent().getExtras().getString("end_time");
        endTime = endTime;
        Log.i("start TIME", "" + openTimeStr);
        Log.i("end TIME", "" + endTime);

//        if(endTime.equals("12:00AM")){
//            endTime = "11:59PM";
//        }
        Log.i("TAG", "exptime " + expTimeTo);
        Log.i("TAG", "end time " + endTime);
        Date date21 = null, date1 = null, date2 = null;
        try {
//            date21 = timeFormat3.parse(endTime);
            date1 = timeFormat2.parse(expTimeTo);
            date2 = df5.parse(endTime);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                date1 = df5.parse(expTimeTo);
                date2 = df5.parse(endTime);

            } catch (Exception e1) {
                e.printStackTrace();
            }
        }


        String st_time = timeFormat1.format(date1);
        String ed_time = timeFormat1.format(date2);
        String currentServerDate1 = timeFormat1.format(current24Date);

        String[] parts = ed_time.split(":");
        int endHour = Integer.parseInt(parts[0]);
        int endMinute = Integer.parseInt(parts[1]);

        String[] parts1 = st_time.split(":");
        int startHour = Integer.parseInt(parts1[0]);
        int startMinute = Integer.parseInt(parts1[1]);

        String[] parts2 = currentServerDate1.split(":");
        int currentHour = Integer.parseInt(parts2[0]);
        int currentMinute = Integer.parseInt(parts2[1]);

        if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 || 0 > 59)))) {
            if (startHour < 0 || (startHour > 5 || (startHour == 5 && (startMinute > 59 || 0 > 59)))) {
                st_time = todayDate + " " + st_time;
                ed_time = todayDate + " " + ed_time;
            } else {
                st_time = tomorrowDate + " " + st_time;
                ed_time = todayDate + " " + ed_time;
            }
        } else {
            if (currentHour < 0 || (currentHour > 5 || (currentHour == 5 && (currentMinute > 59 || 0 > 59)))) {
                st_time = todayDate + " " + st_time;
                ed_time = tomorrowDate + " " + ed_time;
            } else {
                st_time = todayDate + " " + st_time;
                ed_time = todayDate + " " + ed_time;
            }
        }

        Log.i("DATE TAG ddd", st_time + "  " + ed_time);

        Date date3 = null, date4 = null;
        try {
//            date21 = timeFormat3.parse(endTime);
            date3 = timeFormat4.parse(st_time);
            date4 = timeFormat4.parse(ed_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date3.before(date4)) {
            return;
        } else {
            onBackPressed();

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));

            if (language.equalsIgnoreCase("En")) {
                // set title
                alertDialogBuilder.setTitle("Oregano");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Sorry Store is closed! You can't make the order from this store.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
            } else if (language.equalsIgnoreCase("Ar")) {
                // set title
                alertDialogBuilder.setTitle("اوريجانو");

                // set dialog message
                alertDialogBuilder
                        .setMessage("نأسف الفرع مغلق ، لا يمكنك إتمام الطلب من ذلك الفرع")
                        .setCancelable(false)
                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
            }
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

        }

//        long difference = d2.getTime() - d1.getTime();
//        int days = (int) (difference / (1000*60*60*24));
//        int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
//        int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);

    }


    public static String getSourceCode(String requestURL) {
        String response = "";
        try {
            URL url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream(), "UTF-8"));
                while ((line = br.readLine()) != null) {
                    response += line;
                }

            } else {
                response = "";
            }
        } catch (IOException e) {
            e.printStackTrace();
            return response;
        }
        return response;
    }

    public class GetTimes extends AsyncTask<Void, String, String> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(OrderConfirmation.this);
            pd.setMessage("Please wait...");
            pd.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            String res = getSourceCode("http://muslimsalat.com/riyadh/daily.json?key=api_key");
            Log.d("Responce", "" + res);
            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s == null) {
                pd.dismiss();
            } else {
                pd.dismiss();
                listTimes.clear();
                try {
                    JSONObject resObj = new JSONObject(s);
                    JSONArray array = resObj.getJSONArray("items");
                    SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);

                    String date = String.valueOf(array.getJSONObject(0).getString("date_for"));
                    String israq = String.valueOf(array.getJSONObject(0).getString("shurooq"));
                    String zohar = String.valueOf(array.getJSONObject(0).getString("dhuhr"));
                    String asra = String.valueOf(array.getJSONObject(0).getString("asr"));
                    String magrib = String.valueOf(array.getJSONObject(0).getString("maghrib"));
                    String isha = String.valueOf(array.getJSONObject(0).getString("isha"));

                    Log.i("TAG", "date " + date);
                    Log.i("TAG", "isha " + isha);

                    Date israq1 = null;
                    Date zohar1 = null;
                    Date asra1 = null;
                    Date magrib1 = null;
                    Date isha1 = null;
                    try {
                        israq1 = datetime.parse(date + " " + israq);
                        zohar1 = datetime.parse(date + " " + zohar);
                        asra1 = datetime.parse(date + " " + asra);
                        magrib1 = datetime.parse(date + " " + magrib);
                        isha1 = datetime.parse(date + " " + isha);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    listTimes.add(israq1);
                    listTimes.add(zohar1);
                    listTimes.add(asra1);
                    listTimes.add(magrib1);
                    listTimes.add(isha1);

                    Log.i("TAG", "namaz " + magrib1);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                getTimeCaluculation();

                super.onPostExecute(s);
            }
        }
    }

    String getTimeStr(int hours, int mins) {
        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        return aTime;
    }

    Date doParse(String timeStr) {
        try {

            SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.US); //if 24 hour format
            return format.parse(timeStr);
        } catch (Exception e) {

            Log.e("Exception is ", e.toString());
        }
        return null;
    }


    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
            dialog = ProgressDialog.show(OrderConfirmation.this, "",
                    "Calculating time...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                //24.70321657, 46.68097073
//                distanceResponse = jParser
//                        .getJSONFromUrl(URL_DISTANCE + "24.70321657,46.68097073&destinations="+ latitude +","+ longitude+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");

                if (orderType.equalsIgnoreCase("Delivery")) {
                    lat = Double.parseDouble(getIntent().getStringExtra("user_latitude"));
                    longi = Double.parseDouble(getIntent().getStringExtra("user_longitude"));

                    Log.i("TAG", "" + URL_DISTANCE + latitude + "," + longitude + "&destinations=" + lat + "," + longi + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                    distanceResponse = jParser
                            .getJSONFromUrl(URL_DISTANCE + latitude + "," + longitude + "&destinations=" + lat + "," + longi + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                } else {
                    distanceResponse = jParser
                            .getJSONFromUrl(URL_DISTANCE + lat + "," + longi + "&destinations=" + latitude + "," + longitude + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                }
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Oregano");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        secs = jo3.getString("value");
                        trafficMins = Integer.parseInt(secs) / 60;

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            dialog.dismiss();
            new GetCurrentTime().execute();
            super.onPostExecute(result);

        }

    }


    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
            if (flag) {
                dialog = ProgressDialog.show(OrderConfirmation.this, "",
                        "Loading. Please Wait....");
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            } else {
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (serverTime == null) {
                if (flag) {
                    dialog.dismiss();
                }
            } else if (serverTime.equals("no internet")) {
                if (flag) {
                    dialog.dismiss();
                }
                Toast.makeText(OrderConfirmation.this, "Please check internet connection", Toast.LENGTH_SHORT).show();

            } else {

                Log.i("TIME IF TAG", "" + flag);
                if (flag) {
                    dialog.dismiss();
                    try {
                        JSONObject jo = new JSONObject(result1);
                        timeResponse = jo.getString("DateTime");
//                        timeResponse = "03/07/2018 05:00 PM";
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    Log.i("TIME TAG", "" + flag);
                    Date d1 = null;
                    Date d2 = null;
                    try {
                        d1 = timeFormat.parse(timeResponse);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    todayDate = timeFormat1.format(d1);
                    Calendar prayerEnd = Calendar.getInstance();
                    prayerEnd.setTime(d1);
                    prayerEnd.add(Calendar.DATE, 1);
                    d2 = prayerEnd.getTime();
                    tomorrowDate = timeFormat1.format(d2);
                    Log.i("DATE TAG TTTT", todayDate + "  " + tomorrowDate);

                    new GetTimes().execute();
                    flag = false;
                    Log.i("TIME IFINSIDE TAG", "" + flag);
                } else {
                    try {
                        JSONObject jo = new JSONObject(result1);
                        timeResponse = jo.getString("DateTime");
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    getTimeCaluculation();
                }
            }


            super.onPostExecute(result1);
        }
    }


    public class InsertOrder extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        String currentTime;
        String kitchenStart;
        String mamount, mvatamount;
        String estimatedTime, total_amt;
        JSONObject parent = new JSONObject();
        InputStream inputStream = null;
        String result = "";

        String itemId = "", qty = "", comments = "", total_price = null, item_price = "", sizes = "", additionals = "", additionalsPices = "";
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat6 = new SimpleDateFormat("dd-MM-yyyy HH:MM", Locale.US);
        SimpleDateFormat timeFormat5 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
            dialog = ProgressDialog.show(OrderConfirmation.this, "",
                    "Please wait...");
//            promocodeStr = promocode.getText().toString();
//            if(promocodeStr.length()!=5){
//                promocodeStr = "No";
//            }

            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());


            SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);

            kitchenStart = df3.format(kitchenStartDate);

//            String dtc = "2014-04-02T07:59:02.111Z";
//            SimpleDateFormat readDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//            df3.setTimeZone(TimeZone.getTimeZone("GMT+3")); // missing line
            df3.setTimeZone(TimeZone.getDefault());
            Date date = null;
            try {
                date = df3.parse(String.valueOf(kitchenStart));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat writeDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            writeDate.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
            String s = writeDate.format(date);
            String s1 = df3.format(date);

            Log.e("TAG", "date  " + s1);
            SimpleDateFormat df4 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            currentTime = df4.format(c.getTime());
            estimatedTime = expectedtime1;
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
            Date date2 = null;

            try {
                date2 = timeFormat5.parse(estimatedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            dateFormat1.setTimeZone(TimeZone.getDefault());
            Date date1 = null;
            try {
                date1 = timeFormat5.parse(String.valueOf(estimatedTime));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat writeDate1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS", Locale.US);
            writeDate1.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
            String est = writeDate1.format(date1);

            Log.i("TAG", "est " + est);

            estimatedTime = dateFormat1.format(date2).replace("a.m", "AM").replace("p.m", "PM").replace("am", "AM").replace("pm", "PM").replace("AM.", "AM").replace("PM.", "PM");
            Log.e("TAG", "est1 " + estimatedTime.replace("a.m", "AM").replace("p.m", "PM").replace("am", "AM").replace("pm", "PM").replace("AM.", "AM").replace("PM.", "PM"));
            total_amt = totalAmount.getText().toString().replace(" SR", "");
            DecimalFormat dec = new DecimalFormat("0.00");
            mamount = dec.format(Float.parseFloat(String.valueOf(myDbHelper.getTotalOrderPrice())));

            float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
            mvatamount = dec.format(tax);

            String version = "";
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                version = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }


            if (total_amt.equalsIgnoreCase("free")) {
                paymentMode = 4;
            }

            try {
                JSONArray mainItem = new JSONArray();
                JSONArray subItem = new JSONArray();
                JSONArray promoItem = new JSONArray();


                JSONObject mainObj = new JSONObject();
                mainObj.put("UserId", userId);
                mainObj.put("StoreId", storeId);
                mainObj.put("OrderType", Constants.ORDER_TYPE);
                mainObj.put("ExpectedTime", estimatedTime);
                mainObj.put("PreparationTime", PreparationTime-1);
                mainObj.put("TravelTime", TravelTime);
                mainObj.put("KichenStartTime", s);
                mainObj.put("OrderDate", currentTime);
                mainObj.put("IsFavorite", false);
                mainObj.put("FavoriteName", "");
                mainObj.put("comments", "Android v" + version + "," + getResources().getConfiguration().locale);
                mainObj.put("PaymentMode", Integer.toString(paymentMode));
                mainObj.put("Total_Price", Constants.convertToArabic(total_amt));
                mainObj.put("OrderStatus", "New");
                mainObj.put("Device_token", SplashActivity.regid);
                mainObj.put("VatPercentage", "5");
                mainObj.put("SubTotal", Constants.convertToArabic(mamount));
                mainObj.put("VatCharges", Constants.convertToArabic(mvatamount));

                if (orderType.equalsIgnoreCase("Delivery")) {
                    mainObj.put("AddressID", addressId);
                }
                mainItem.put(mainObj);

                JSONObject promoObj = new JSONObject();
                promoObj.put("DeviceToken", SplashActivity.regid);
                promoObj.put("promotionCode", promocodeStr);
                if (promoTypeStr.equals("6")) {
                    promoObj.put("BonusAmt", 0);
                } else {
                    promoObj.put("BonusAmt", remainingBonusInt);
                }

                promoItem.put(promoObj);

                for (Order order : orderList) {
                    JSONObject subObj = new JSONObject();

                    JSONArray subItem1 = new JSONArray();
                    JSONArray subItem2 = new JSONArray();

                    subObj.put("ItemPrice", Constants.convertToArabic(dec.format(Float.parseFloat(order.getItemPrice()))));
                    subObj.put("Qty", order.getQty());
                    subObj.put("Comments", order.getComment());
                    subObj.put("ItemId", order.getItemId());
                    subObj.put("Size", order.getItemTypeId());
                    if (!order.getAdditionalId().equals("")) {
                        String[] addIdParts = order.getAdditionalId().split(",");
                        String[] addPriceParts = order.getAdditionalPrice().split(",");
                        for (int i = 0; i < addIdParts.length; i++) {
                            JSONObject subObj1 = new JSONObject();
                            if (addPriceParts[i] != null) {
                                subObj1.put("AdditionalID", addIdParts[i]);
                                subObj1.put("AdditionalPrice", Constants.convertToArabic(addPriceParts[i]));
                                subItem2.put(subObj1);
                            }
                        }
                    }

                    subItem1.put(subObj);
                    if (subItem2.length() > 0) {
                        subItem1.put(subItem2);
                    }
                    subItem.put(subItem1);
                }

                if (promoTypeStr.equals("6")) {
                    if (promoArray != null && promoArray.length() > 0) {
                        subItem.put(promoArray);
                    }
                }

                parent.put("MainItem", mainItem);
                parent.put("SubItem", subItem);
                if (remainingBonusInt != 0) {
                    parent.put("Promotion", promoItem);
                }
                Log.i("TAG", parent.toString());
            } catch (JSONException je) {

            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.INSERT_ORDER_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(parent.toString());

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            result = convertInputStreamToString(inputStream);
                            return result;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + result);
            } else {
                return "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                if (result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Oregano");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
//                    Constants.orderNow = false;
                    myDbHelper.deleteOrderTable();
                    try {
                        double number;
                        number = myDbHelper.getTotalOrderPrice();
                        DecimalFormat decim = new DecimalFormat("0.00");
                        CheckoutActivity.amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
                        float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
                        CheckoutActivity.vatAmount.setText("" + decim.format(tax));
                        CheckoutActivity.netTotal.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                        CheckoutActivity.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()) + tax);
                        CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        OrderFragment.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                        CategoryItems.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()) );
//                        CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
//                    try {
//                        new Handler().postDelayed(new Runnable() {
//
//                            @Override
//                            public void run() {
//                                orderPrefsEditor.putString("order_status", "close");
//                                orderPrefsEditor.commit();
//                            }
//                        }, Integer.parseInt(secs));
//                    }catch (Exception e){
//
//                    }

                    String order_number = "";
                    try {
                        JSONObject jo = new JSONObject(result);
                        order_number = jo.getString("Success");

                        String[] orderNo = order_number.split("-");
                        String[] parts = order_number.split(",");
                        final String orderId = parts[0];

                        Date current24Date = null, currentServerDate = null;
                        Date expectedTimeDate = null, expectedTime24 = null;
                        try {
                            current24Date = timeFormat.parse(timeResponse);
                            expectedTime24 = dateFormat1.parse(estimatedTime);
                            Log.i("TAG", "expected " + expectedTime24);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG", "expected " + expectedTime24);
                        String currentTime = timeFormat1.format(current24Date);
                        String expTimeStr = timeFormat5.format(expectedTime24);
                        try {
                            currentServerDate = timeFormat1.parse(currentTime);
                            expectedTimeDate = timeFormat5.parse(expTimeStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long diff = expectedTimeDate.getTime() - currentServerDate.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        int expMins = (int) diffMinutes * 60 * 1000;
                        Log.i("TAG", "mins response: " + expMins);
                        orderPrefsEditor.putString("order_id", orderId);
                        orderPrefsEditor.putString("order_status", "open");
                        orderPrefsEditor.commit();
                        try {
                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    orderPrefsEditor.putString("order_id", orderId);
                                    orderPrefsEditor.putString("order_status", "close");
                                    orderPrefsEditor.commit();
                                }
                            }, expMins);
                        } catch (Exception e) {
                        }

                        Intent intent = new Intent(OrderConfirmation.this, OrderDetails.class);
                        intent.putExtra("storeId", storeId);
                        intent.putExtra("storeName", storeName);
                        intent.putExtra("storeAddress", storeAddress);
                        intent.putExtra("latitude", latitude);
                        intent.putExtra("longitude", longitude);
                        intent.putExtra("amount", amount.getText().toString());
                        intent.putExtra("vatamount", vatAmount.getText().toString());
                        intent.putExtra("netamount", netTotal.getText().toString());
                        intent.putExtra("total_amt", totalAmount.getText().toString());
                        intent.putExtra("total_items", totalItems.getText().toString());
                        intent.putExtra("expected_time", expectedtime1);
                        intent.putExtra("payment_mode", Integer.toString(paymentMode));
                        intent.putExtra("order_type", orderType);
                        intent.putExtra("order_number", order_number);
                        startActivity(intent);

                    } catch (JSONException je) {
                        je.printStackTrace();
                        try {
                            JSONObject jo = new JSONObject(result);
                            String msg = jo.getString("Failure");
                            if (msg.equalsIgnoreCase("Promotion Already Used")) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.view.ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));

                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("Oregano");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Promotion Already Used")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    promoLayout.setClickable(true);
                                                    promocode.setText("");
                                                    promoArrow.setVisibility(View.VISIBLE);
                                                    promoCancel.setVisibility(View.GONE);
                                                    promocode.setHint("Apply Promotion");
                                                    DecimalFormat decim = new DecimalFormat("0.00");
                                                    amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
                                                    float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
                                                    vatAmount.setText("" + decim.format(tax));
                                                    netTotal.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                                                    totalAmount.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                                                    if (language.equalsIgnoreCase("En")) {
                                                        promocode.setHint("Apply Promotion");
                                                    } else if (language.equalsIgnoreCase("Ar")) {
                                                        promocode.setHint("قدم للعرض");
                                                    }
                                                    dialog.dismiss();
                                                }
                                            });
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    // set title
                                    alertDialogBuilder.setTitle("اوريجانو");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("")
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    promoLayout.setClickable(true);
                                                    promocode.setText("");
                                                    promoArrow.setVisibility(View.VISIBLE);
                                                    promoCancel.setVisibility(View.GONE);
                                                    promocode.setHint("Apply Promotion");

                                                    DecimalFormat decim = new DecimalFormat("0.00");
                                                    amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
                                                    float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
                                                    vatAmount.setText("" + decim.format(tax));
                                                    netTotal.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                                                    totalAmount.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                                                    if (language.equalsIgnoreCase("En")) {
                                                        promocode.setHint("Apply Promotion");
                                                    } else if (language.equalsIgnoreCase("Ar")) {
                                                        promocode.setHint("قدم للعرض");
                                                    }
                                                    dialog.dismiss();
                                                }
                                            });
                                }

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                                alertDialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            dialog.dismiss();
            super.onPostExecute(result1);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
    }

    public class GetStoredCards extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    JSONParser jParser = new JSONParser();

                    getCardResponse = jParser
                            .getJSONFromUrl(arg0[0]);
                    Log.i("TAG", "user response:" + getCardResponse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                getCardResponse = "no internet";
            }
            return getCardResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (getCardResponse == null) {

            } else if (getCardResponse.equals("no internet")) {
                Toast.makeText(OrderConfirmation.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {

                try {
                    JSONObject jo = new JSONObject(getCardResponse);
                    JSONArray ja = jo.getJSONArray("Success");
                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jo1 = ja.getJSONObject(i);
                        String sTokenStr = jo1.getString("SToken");


                        List<PWAccount> sToken = _binder.getAccountFactory().deserializeAccountList(sTokenStr);
                        accounts.addAll(sToken);
                    }
                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }


                super.onPostExecute(result1);
            }
        }
    }


    public class InsertCardDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    JSONParser jParser = new JSONParser();

                    setCardResponse = jParser
                            .getJSONFromUrl(arg0[0]);
                    Log.i("TAG", "user response:" + setCardResponse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                setCardResponse = "no internet";
            }
            return setCardResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (setCardResponse == null) {

            } else if (getCardResponse.equals("no internet")) {
                Toast.makeText(OrderConfirmation.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {

                super.onPostExecute(result1);
            }
        }
    }

    public class GetPromocodeResponse extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            promosList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
            dialog = ProgressDialog.show(OrderConfirmation.this, "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... params) {

            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
//            promoFlag = false;
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderConfirmation.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(OrderConfirmation.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i < ja.length(); i++) {

                                    Promos prom = new Promos();
                                    JSONObject jo1 = ja.getJSONObject(i);


                                    String PromoID = jo1.getString("PromoID");
                                    String PromoCode = jo1.getString("PromoCode");
                                    String category = jo1.getString("category");
                                    String itemprice = jo1.getString("itemprice");
                                    String Percentage = jo1.getString("Percentage");
                                    String Img = jo1.getString("Img");
                                    String Desc_En = jo1.getString("Desc_En");
                                    String Desc_Ar = jo1.getString("Desc_Ar");
                                    String PromoTitle_Ar = jo1.getString("PromoTitle_Ar");
                                    String PromoTitle_En = jo1.getString("PromoTitle_En");
                                    String ItemId = jo1.getString("ItemId");
                                    String StoreId = jo1.getString("StoreId");
                                    String Additonals = jo1.getString("Additonals");
                                    String Size = jo1.getString("Size");
                                    String PromoType = jo1.getString("PromoType");
                                    String RemainingBonus = jo1.getString("RemainingBonus");

                                    prom.setPromoID(PromoID);
                                    prom.setPromoCode(PromoCode);
                                    prom.setPercentage(Percentage);
                                    prom.setDescription(Desc_En);
                                    prom.setDescriptionAr(Desc_Ar);
                                    prom.setPromoTitle(PromoTitle_En);
                                    prom.setPromoTitleAr(PromoTitle_Ar);
                                    prom.setItemId(ItemId);
                                    prom.setStoreId(StoreId);
                                    prom.setAdditionals(Additonals);
                                    prom.setSize(Size);
                                    prom.setPromoType(PromoType);
                                    prom.setRemainingBonus(RemainingBonus);
                                    prom.setItemPrice(itemprice);
                                    prom.setCategory(category);
                                    prom.setImage(Img);

                                    promosList.add(prom);

                                }
                                dialog();
                            } catch (JSONException je) {
                                Toast.makeText(OrderConfirmation.this, "Sorry! No active promotions", Toast.LENGTH_SHORT).show();
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Oregano");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(OrderConfirmation.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            mPromoAdapter.notifyDataSetChanged();
            super.onPostExecute(result);
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {
            Log.i("mobile.connect.checkout", "user canceled the checkout process/error");
//            Toast.makeText(OrderConfirmation.this, "Checkout cancelled or an error occurred.", Toast.LENGTH_SHORT).show();
            confirmOrder.setClickable(true);
        } else if (resultCode == RESULT_OK) {
            Toast.makeText(OrderConfirmation.this, "Thank you for shopping!", Toast.LENGTH_SHORT).show();
            new InsertOrder().execute();
            // if the user added a new account, store it
            if (data.hasExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_RESULT_ACCOUNT)) {
                Log.i("mainactivity", "checkout went through, callback has an account");
                accounts.clear();
                ArrayList<PWAccount> newAccounts = data.getExtras().getParcelableArrayList(PWConnectCheckoutActivity.CONNECT_CHECKOUT_RESULT_ACCOUNT);
                accounts.addAll(newAccounts);

                try {
                    Log.i("Card", "  " + _binder.getAccountFactory().serializeAccountList(accounts).toString().replace(" ", ""));
//                    new InsertCardDetails().execute(Constants.SAVE_CARD_DETAILS_URL+userId+"&sToken="+_binder.getAccountFactory().serializeAccountList(newAccounts).replace(" ",""));
//                    sharedSettings.edit().putString(ACCOUNTS, _binder.getAccountFactory().serializeAccountList(accounts)).commit();
                } catch (PWProviderNotInitializedException e) {
                    e.printStackTrace();
                }
            } else {
                Log.i("mainactivity", "checkout went through, callback has transaction result");
            }
        }
    }


    public void dialog() {

        final Dialog dialog2 = new Dialog(OrderConfirmation.this);

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (language.equalsIgnoreCase("En")) {
            dialog2.setContentView(R.layout.promo_list);
        } else if (language.equalsIgnoreCase("Ar")) {
            dialog2.setContentView(R.layout.promo_list_arabic);
        }

        dialog2.setCanceledOnTouchOutside(true);
        ListView lv = (ListView) dialog2.findViewById(R.id.promos_list);
        ImageView cancel = (ImageView) dialog2.findViewById(R.id.cancel_button);
        lv.setAdapter(mPromoAdapter);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                dialog2.dismiss();

                if (promosList.get(position).getStoreId().equals(storeId) || promosList.get(position).getStoreId().equals("0")) {
                    remainingBonusInt = 0;
                    // full free drink
                    if (promosList.get(position).getPromoType().equals("1")) {
//                        if(promosList.get(position).getItemId().equals("0") && promosList.get(position).getSize().equals("0")){
                        float highestPrice = 0;
                        for (Order order : orderList) {
                            if ((order.getSubCategoryId().equals("7") || order.getSubCategoryId().equals("8")) && order.getItemTypeId().equals("1")) {
                                int qty = Integer.parseInt(order.getQty());
                                float tprice = Float.parseFloat(order.getTotalAmount());
                                tprice = tprice / qty;
                                Log.i("TAG", "Check");
                                if (highestPrice < tprice) {
                                    highestPrice = (highestPrice > tprice) ? highestPrice : tprice;

                                    freeOrderId = order.getOrderId();
//                                        promoDic=[[NSMutableDictionary alloc]init];
//                                        promoDic=[arrayObjects objectAtIndex:i];


                                }
                            }
                        }

                        if (highestPrice != 0) {

                            remainingBonusInt = Float.parseFloat(promosList.get(position).getItemPrice());
                            promocodeStr = promosList.get(position).getPromoCode();
                            if (language.equalsIgnoreCase("En")) {
                                promocode.setText(promosList.get(position).getPromoTitle() + " " + remainingBonusInt + " SR Redeem");
                            } else if (language.equalsIgnoreCase("Ar")) {
                                promocode.setText(promosList.get(position).getPromoTitleAr() + " " + remainingBonusInt + " ريال مجانا ");
                            }

                            promoIdStr = promosList.get(position).getPromoID();
                            promoTypeStr = promosList.get(position).getPromoType();
                            float totalPriceInt = myDbHelper.getTotalOrderPrice();
                            highestPrice = totalPriceInt - remainingBonusInt;
                            if (highestPrice == 0) {
                                totalAmount.setText("Free");
                                paymentMode = 4;
                            } else {
                                totalAmount.setText("" + decim.format(highestPrice + tax));
                                amount.setText("" + decim.format(highestPrice));
                                netTotal.setText("" + decim.format(highestPrice + tax));
                            }
                            promoLayout.setClickable(false);
                            promoArrow.setVisibility(View.GONE);
                            promoCancel.setVisibility(View.VISIBLE);

                        } else {
                            promocodeStr = "";
                            if (language.equalsIgnoreCase("En")) {
                                promocode.setHint("Apply Promotion");
                                Toast.makeText(OrderConfirmation.this, "Specified item not available in cart", Toast.LENGTH_SHORT).show();
                            } else if (language.equalsIgnoreCase("Ar")) {
                                promocode.setHint("قدم للعرض");
                                Toast.makeText(OrderConfirmation.this, "المنتجات المحددة غير موجودة في سلة الشراء", Toast.LENGTH_SHORT).show();
                            }

                            promoIdStr = "";
                            promoTypeStr = "";

                            float totalPriceInt = myDbHelper.getTotalOrderPrice();

                            totalAmount.setText("" + decim.format(totalPriceInt + tax));
                            amount.setText("" + decim.format(totalPriceInt));
                            netTotal.setText("" + decim.format(totalPriceInt + tax));
//                                paymentMode = 3;
                        }
                    } else if (promosList.get(position).getPromoType().equals("6")) {
                        remainingBonusInt = Float.parseFloat(promosList.get(position).getItemPrice());
                        float totalPriceInt = myDbHelper.getTotalOrderPrice();
                        totalItems.setText("" + (myDbHelper.getTotalOrderQty() + 1));
                        if (remainingBonusInt <= totalPriceInt) {
//                            remainingBonusInt = 0;
//                           PromoType2 promo = new /PromoType2();
//                           promo.setItemId(promosList.get(position).getItemId());
//                           promo.setItemPrice("0");
//                           promo.setQty("1");
//                           promo.setSize(promosList.get(position).getSize());
//                           promo.setComments("Free");
//                           promo.setAdditionalID(promosList.get(position).getAdditionals());
//                           promo.setAdditionalPrice("0");

//                           promoType2ArrayList.add(promo);

                            try {
                                Log.i("TAG", "promo array lenght " + promoArray.length());
                                if (promoArray != null && promoArray.length() == 0) {

                                    JSONObject subObj = new JSONObject();

                                    JSONArray subItem2 = new JSONArray();

                                    subObj.put("ItemPrice", "0");
                                    subObj.put("Qty", "1");
                                    subObj.put("Comments", "Free");
                                    subObj.put("ItemId", promosList.get(position).getItemId());
                                    subObj.put("Size", promosList.get(position).getSize());

                                    JSONObject subObj1 = new JSONObject();
                                    subObj1.put("AdditionalID", promosList.get(position).getAdditionals());
                                    subObj1.put("AdditionalPrice", "0");
                                    subItem2.put(subObj1);

                                    promoArray.put(subObj);
                                    if (subItem2.length() > 0) {
                                        promoArray.put(subItem2);
                                    }
                                }
                                Log.i("TAG", "promo array " + promoArray.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            promocodeStr = promosList.get(position).getPromoCode();
                            promoIdStr = promosList.get(position).getPromoID();
                            promoTypeStr = promosList.get(position).getPromoType();

                            if (language.equalsIgnoreCase("En")) {
                                promocode.setText(promosList.get(position).getPromoTitle());
                            } else if (language.equalsIgnoreCase("Ar")) {
                                promocode.setText(promosList.get(position).getPromoTitleAr());
                            }

                            promoLayout.setClickable(false);
                            promoArrow.setVisibility(View.GONE);
                            promoCancel.setVisibility(View.VISIBLE);
                        } else {
                            promocodeStr = "";
                            if (language.equalsIgnoreCase("En")) {
                                promocode.setHint("Apply Promotion");
                                Toast.makeText(OrderConfirmation.this, "Specified item not available in cart", Toast.LENGTH_SHORT).show();
                            } else if (language.equalsIgnoreCase("Ar")) {
                                promocode.setHint("قدم للعرض");
                                Toast.makeText(OrderConfirmation.this, "المنتجات المحددة غير موجودة في سلة الشراء", Toast.LENGTH_SHORT).show();
                            }

                            promoIdStr = "";
                            promoTypeStr = "";

                            float totalPriceInt1 = myDbHelper.getTotalOrderPrice();

                            totalAmount.setText("" + decim.format(totalPriceInt1 + tax));
                            amount.setText("" + decim.format(totalPriceInt1));
                            netTotal.setText("" + decim.format(totalPriceInt1 + tax));
//                                paymentMode = 3;
                        }
                    }
                }

            }
        });


        dialog2.show();

    }
}
