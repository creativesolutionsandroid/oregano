package com.cs.oregano.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.oregano.Constants;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by CS on 25-06-2016.
 */
public class StoreDetailsActivity extends AppCompatActivity{
    private String storeId, storeName, storeAddress, storePhoneNumber, stTime, edTime, storeImage;
    private Double latitude, longitude;
    private double lat, longi;
    private boolean is24x7;

    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    private String distanceResponse = null;

    Toolbar toolbar;
    LinearLayout storeCall;
    ImageView storeIcon;

    private ArrayList<String> storeHours = new ArrayList<>();
    String response;

    CardView inviteFriends, shareLocation, rateTheStore, getDirectionsText;

    private TextView phoneNo, storeNameTextView, storeAddressTextView, storeHoursTextView,
             favoriteStoreText, travelTimeText, orderNowText;

    private LinearLayout storeHoursLayout, phoneNumberLayout;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.store_details);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.store_details_arabic);
        }


        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        storeId = getIntent().getExtras().getString("storeId");
        storeName = getIntent().getExtras().getString("storeName");
        storeImage = getIntent().getExtras().getString("storeImage");
        storeAddress = getIntent().getExtras().getString("storeAddress");
        latitude = getIntent().getExtras().getDouble("latitude");
        longitude = getIntent().getExtras().getDouble("longitude");
        lat = getIntent().getExtras().getDouble("lat");
        longi = getIntent().getExtras().getDouble("longi");
        stTime = getIntent().getExtras().getString("start_time");
        edTime = getIntent().getExtras().getString("end_time");
        is24x7 = getIntent().getExtras().getBoolean("is24x7");


        storeCall = (LinearLayout) findViewById(R.id.store_detail_call);
        phoneNo = (TextView) findViewById(R.id.phone_number);
        storeNameTextView = (TextView) findViewById(R.id.store_detail_name);
        storeAddressTextView = (TextView) findViewById(R.id.store_detail_address);
        storeHoursTextView = (TextView) findViewById(R.id.store_detail_hours);
        inviteFriends = (CardView) findViewById(R.id.store_detail_invite);
        shareLocation = (CardView) findViewById(R.id.store_detail_share);
        rateTheStore = (CardView) findViewById(R.id.store_detail_rate);
        getDirectionsText = (CardView) findViewById(R.id.store_detail_directions);
        travelTimeText = (TextView) findViewById(R.id.store_travel_time);
        storeIcon = (ImageView) findViewById(R.id.store_icon);
        storeHoursLayout = (LinearLayout) findViewById(R.id.store_hours_layout);
        phoneNumberLayout = (LinearLayout) findViewById(R.id.phone_number_layout);
//        orderNowText = (TextView) findViewById(R.id.ordernow_btn);

        storeNameTextView.setText(storeName);
        storeAddressTextView.setText(storeAddress);
        SimpleDateFormat time = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat datetime = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        Date st = null, et=null;
        try {
            st = datetime.parse(stTime);
            et = datetime.parse(edTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String starttime = time.format(st);
        String endtime = time.format(et);
        storeHoursTextView.setText(starttime+" - "+endtime);
        Glide.with(StoreDetailsActivity.this).load(Constants.IMAGE_URL+storeImage).into(storeIcon);

        phoneNumberLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + storePhoneNumber));
                        startActivity(intent);
                    }
                }else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + storePhoneNumber));
                    startActivity(intent);
                }

            }
        });

        storeHoursLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StoreDetailsActivity.this, StoreHoursActivity.class);
                intent.putStringArrayListExtra("open_hours", storeHours);
                startActivity(intent);
            }
        });

        shareLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,
                        storeName +".\n http://maps.google.com/maps?saddr="+ lat + "," + longi+"&daddr=" + latitude + "," + longitude);
//                intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
//                        "");
                startActivity(Intent.createChooser(intent, "Share"));
            }
        });

        inviteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "I would love to have a Pizaa with you at Oregano in " + storeName +
                                ".\n http://maps.google.com/maps?saddr="+ lat + "," + longi+"&daddr=" + latitude + "," + longitude);
//                intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
//                        "");
                startActivity(Intent.createChooser(intent, "Share"));
            }
        });

        getDirectionsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Uri gmmIntentUri = Uri.parse("google.navigation:q=17.4474905,78.3412107");
//                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                mapIntent.setPackage("com.google.android.apps.maps");
//                startActivity(mapIntent);


                Uri gmmIntentUri = Uri.parse("google.navigation:q="+ latitude + "," + longitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);


//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr="+ lat + "," + longi+"&daddr=" + latitude + "," + longitude));
//                startActivity(intent);
            }
        });


        new getTrafficTime().execute();
        new GetStoreDetails().execute("http://www.ircfood.com/api/StoreInformationApi?storeId="+storeId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public class GetStoreDetails extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetailsActivity.this);
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser.getJSONFromUrl(arg0[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if(result == null){

            } else if(result.equals("no internet")){
                Toast.makeText(StoreDetailsActivity.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    JSONArray ja = new JSONArray(result);
                    JSONObject jo = ja.getJSONObject(0);

                    storePhoneNumber = jo.getString("phone");
                    phoneNo.setText(jo.getString("phone"));
                    if (jo.getBoolean("isSunClose")) {
                        storeHours.add("Closed");
                    } else {
                        storeHours.add(jo.getString("sunST") + " to " + jo.getString("sunET"));
                    }

                    if (jo.getBoolean("isMonClose")) {
                        storeHours.add("Closed");
                    } else {
                        storeHours.add(jo.getString("monST") + " to " + jo.getString("monET"));
                    }

                    if (jo.getBoolean("isTueClose")) {
                        storeHours.add("Closed");
                    } else {
                        storeHours.add(jo.getString("tueST") + " to " + jo.getString("tueET"));
                    }

                    if (jo.getBoolean("isWedClose")) {
                        storeHours.add("Closed");
                    } else {
                            storeHours.add(jo.getString("wedST") + " to " + jo.getString("wedET"));

                    }

                    if (jo.getBoolean("isThuClose")) {
                        storeHours.add("Closed");
                    } else {
                            storeHours.add(jo.getString("thuST") + " to " + jo.getString("thuET"));

                    }

                    if (jo.getBoolean("isFriClose")) {
                        storeHours.add("Closed");
                    } else {
                        storeHours.add(jo.getString("friST") + " to " + jo.getString("friET"));
                    }

                    if (jo.getBoolean("isSatClose")) {
                        storeHours.add("Closed");
                    } else {
                        storeHours.add(jo.getString("satST") + " to " + jo.getString("satET"));
                    }

                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }


                //detailsAdapter.notifyDataSetChanged();
                super.onPostExecute(result);
            }
        }
    }


    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(StoreDetailsActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + lat +","+ longi +"&destinations="+ latitude +","+ longitude+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(StoreDetailsActivity.this);

                    // set title
                    alertDialogBuilder.setTitle("Oregano");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
//                        if(language.equalsIgnoreCase("En")) {
                            travelTimeText.setText(secs);
//                        }else if(language.equalsIgnoreCase("Ar")){
//                            travelTimeText.setText(secs+ "  وقت السفر" );
//                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            super.onPostExecute(result);

        }

    }


    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(StoreDetailsActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + storePhoneNumber));
                    startActivity(intent);
                }
                else {
                    Toast.makeText(StoreDetailsActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

}
