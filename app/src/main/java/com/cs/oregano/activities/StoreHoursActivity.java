package com.cs.oregano.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.cs.oregano.R;

import java.util.ArrayList;

/**
 * Created by CS on 10-07-2016.
 */
public class StoreHoursActivity extends AppCompatActivity {
    private ArrayList<String> storeHours = new ArrayList<>();
    TextView sundayHours, mondayHours, tuesdayHours, wednesdayHours, thursdayHours, fridayHours, saturdayHours;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.store_timings);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.store_timings_arabic);
        }


        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sundayHours = (TextView) findViewById(R.id.store_time_sunday);
        mondayHours = (TextView) findViewById(R.id.store_time_monday);
        tuesdayHours = (TextView) findViewById(R.id.store_time_tuesday);
        wednesdayHours = (TextView) findViewById(R.id.store_time_wednesday);
        thursdayHours = (TextView) findViewById(R.id.store_time_thursday);
        fridayHours = (TextView) findViewById(R.id.store_time_friday);
        saturdayHours = (TextView) findViewById(R.id.store_time_saturday);
        storeHours = getIntent().getExtras().getStringArrayList("open_hours");

        if(storeHours.size() > 0) {
            sundayHours.setText(storeHours.get(0));
            mondayHours.setText(storeHours.get(1));
            tuesdayHours.setText(storeHours.get(2));
            wednesdayHours.setText(storeHours.get(3));
            thursdayHours.setText(storeHours.get(4));
            fridayHours.setText(storeHours.get(5));
            saturdayHours.setText(storeHours.get(6));
        }else{
            sundayHours.setText("Closed");
            mondayHours.setText("Closed");
            tuesdayHours.setText("Closed");
            wednesdayHours.setText("Closed");
            thursdayHours.setText("Closed");
            fridayHours.setText("Closed");
            saturdayHours.setText("Closed");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
