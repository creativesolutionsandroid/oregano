package com.cs.oregano.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.R;
import com.cs.oregano.adapters.CommentsAdapter;
import com.cs.oregano.model.Rating;
import com.cs.oregano.model.RatingDetails;
import com.cs.oregano.widgets.CustomGridView;
import com.cs.oregano.widgets.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by CS on 28-02-2017.
 */

public class RatingActivity extends Activity {

    ArrayList<RatingDetails> ratingsArrayList = new ArrayList<>();
    AlertDialog alertDialog2;
    LinearLayout feedback_layout;
    MaterialRatingBar rateBar;
    TextView rate_text;
    public static EditText comment;
    TextView close_dialog, wrong_text;
    Button submit;
    String userId, orderid;
    float rating;
    public static ArrayList<Integer> pos = new ArrayList<>();
    public static ArrayList<String> str = new ArrayList<>();
    public static ArrayList<String> ratingId = new ArrayList<>();

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;

    ArrayList<Rating> rating1 = new ArrayList<>();
    ArrayList<Rating> rating2 = new ArrayList<>();
    ArrayList<Rating> rating3 = new ArrayList<>();
    ArrayList<Rating> rating4 = new ArrayList<>();
    ArrayList<Rating> rating5 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.rating_layout);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.rating_layout_arabic);
        }

        userId = getIntent().getStringExtra("userid");
        orderid = getIntent().getStringExtra("orderid");
        rating = getIntent().getFloatExtra("rating",0);
        ratingsArrayList = (ArrayList<RatingDetails>) getIntent().getSerializableExtra(
        "array");

        feedback_layout = (LinearLayout) findViewById(R.id.what_went_wrong);
        submit = (Button) findViewById(R.id.submit);
        close_dialog = (TextView) findViewById(R.id.rating_cancel);
        rate_text = (TextView)findViewById(R.id.rating_text);
        wrong_text = (TextView)findViewById(R.id.wrong_text);
        rateBar = (MaterialRatingBar) findViewById(R.id.ratingBar_layout);
        comment = (EditText) findViewById(R.id.comment);
        comment.setVisibility(View.GONE);

        final CustomGridView gridView = (CustomGridView) findViewById(R.id.remarks_grid);

        rateBar.setRating(rating);
        pos.clear();
        str.clear();
        ratingId.clear();

        try {
            str.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        LayerDrawable stars = (LayerDrawable) rateBar.getProgressDrawable();
//        stars.getDrawable(2).setColorFilter(Color.parseColor("#26ce61"),PorterDuff.Mode.SRC_ATOP); // for filled stars
//        stars.getDrawable(1).setColorFilter(Color.parseColor("#FFFF00"),PorterDuff.Mode.SRC_ATOP); // for half filled stars
//        stars.getDrawable(0).setC

        for(int i=0; i<ratingsArrayList.get(0).getArrayList().size(); i++){
            if(ratingsArrayList.get(0).getArrayList().get(i).getRating().equals("1")){
                Rating r1 = new Rating();
                r1.setRatingId(ratingsArrayList.get(0).getArrayList().get(i).getRatingId());
                r1.setComment(ratingsArrayList.get(0).getArrayList().get(i).getComment());
                r1.setComment_ar(ratingsArrayList.get(0).getArrayList().get(i).getComment_ar());
                r1.setRating(ratingsArrayList.get(0).getArrayList().get(i).getRating());
                rating1.add(r1);
            }
            else if(ratingsArrayList.get(0).getArrayList().get(i).getRating().equals("2")){
                Rating r1 = new Rating();
                r1.setRatingId(ratingsArrayList.get(0).getArrayList().get(i).getRatingId());
                r1.setComment(ratingsArrayList.get(0).getArrayList().get(i).getComment());
                r1.setComment_ar(ratingsArrayList.get(0).getArrayList().get(i).getComment_ar());
                r1.setRating(ratingsArrayList.get(0).getArrayList().get(i).getRating());
                rating2.add(r1);
            }
            else if(ratingsArrayList.get(0).getArrayList().get(i).getRating().equals("3")){
                Rating r1 = new Rating();
                r1.setRatingId(ratingsArrayList.get(0).getArrayList().get(i).getRatingId());
                r1.setComment(ratingsArrayList.get(0).getArrayList().get(i).getComment());
                r1.setComment_ar(ratingsArrayList.get(0).getArrayList().get(i).getComment_ar());
                r1.setRating(ratingsArrayList.get(0).getArrayList().get(i).getRating());
                rating3.add(r1);
            }
            else if(ratingsArrayList.get(0).getArrayList().get(i).getRating().equals("4")){
                Rating r1 = new Rating();
                r1.setRatingId(ratingsArrayList.get(0).getArrayList().get(i).getRatingId());
                r1.setComment(ratingsArrayList.get(0).getArrayList().get(i).getComment());
                r1.setComment_ar(ratingsArrayList.get(0).getArrayList().get(i).getComment_ar());
                r1.setRating(ratingsArrayList.get(0).getArrayList().get(i).getRating());
                rating4.add(r1);
            }
            else if(ratingsArrayList.get(0).getArrayList().get(i).getRating().equals("5")){
                Rating r1 = new Rating();
                r1.setRatingId(ratingsArrayList.get(0).getArrayList().get(i).getRatingId());
                r1.setComment(ratingsArrayList.get(0).getArrayList().get(i).getComment());
                r1.setComment_ar(ratingsArrayList.get(0).getArrayList().get(i).getComment_ar());
                r1.setRating(ratingsArrayList.get(0).getArrayList().get(i).getRating());
                rating5.add(r1);
            }
        }
        if(rating == 1){
            if(language.equalsIgnoreCase("En")) {
                rate_text.setText("Terrible");
                wrong_text.setText("Tell us what went wrong ?");
            }else if(language.equalsIgnoreCase("Ar")){
                rate_text.setText("فضيع");
                wrong_text.setText("أخبرنا أين كان الخطأ؟");
            }
            CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating1, language);
            gridView.setAdapter(mAdapter);
        }
        else if(rating == 2){
            if(language.equalsIgnoreCase("En")) {
                rate_text.setText("Bad");
                wrong_text.setText("Tell us what went wrong ?");
            }else if(language.equalsIgnoreCase("Ar")){
                rate_text.setText("سئ");
                wrong_text.setText("أخبرنا أين كان الخطأ؟");
            }
            CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating2, language);
            gridView.setAdapter(mAdapter);
        }
        else if(rating == 3){
            if(language.equalsIgnoreCase("En")) {
                rate_text.setText("Ok");
                wrong_text.setText("Tell us what went wrong ?");
            }else if(language.equalsIgnoreCase("Ar")){
                rate_text.setText("حسنا");
                wrong_text.setText("أخبرنا أين كان الخطأ؟");
            }
            CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating3, language);
            gridView.setAdapter(mAdapter);
        }
        else if(rating == 4){
            if(language.equalsIgnoreCase("En")) {
                rate_text.setText("Good");
                wrong_text.setText("Where do you feel we should improve ?");
            }else if(language.equalsIgnoreCase("Ar")){
                rate_text.setText("جيد");
                wrong_text.setText("أين تجد شعورك فيما يجب تحسينه؟");
            }
            CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating4, language);
            gridView.setAdapter(mAdapter);
        }
        else if(rating == 5){
            if(language.equalsIgnoreCase("En")) {
                rate_text.setText("Excellent");
                wrong_text.setText("What do you like best about us ?");
            }else if(language.equalsIgnoreCase("Ar")){
                rate_text.setText("ممتاز");
                wrong_text.setText("ما هو الشئ الممتاز لدينا وأنت تفضله؟");
            }
            CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating5, language);
            gridView.setAdapter(mAdapter);
        }

        rateBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if(rating<1){
                    if(language.equalsIgnoreCase("En")) {
                        rate_text.setText("Terrible");
                        wrong_text.setText("Tell us what went wrong ?");
                    }else if(language.equalsIgnoreCase("Ar")){
                        rate_text.setText("فضيع");
                        wrong_text.setText("أخبرنا أين كان الخطأ؟");
                    }
                    pos.clear();
                    str.clear();
                    ratingId.clear();
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    rateBar.setRating(1);
                    feedback_layout.setVisibility(View.VISIBLE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating1, language);
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
//                    if(pos !=-1 && ratingsArrayList.get(0).getRatingDetails().get(pos).getRemarks().equalsIgnoreCase("Other")){
//                        comment.setVisibility(View.VISIBLE);
//                    }
                }
                else if(rating == 1){
                    if(language.equalsIgnoreCase("En")) {
                        rate_text.setText("Terrible");
                        wrong_text.setText("Tell us what went wrong ?");
                    }else if(language.equalsIgnoreCase("Ar")){
                        rate_text.setText("فضيع");
                        wrong_text.setText("أخبرنا أين كان الخطأ؟");
                    }
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    feedback_layout.setVisibility(View.VISIBLE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating1, language);
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
//                    if(pos !=-1 && ratingsArrayList.get(0).getRatingDetails().get(pos).getRemarks().equalsIgnoreCase("Other")){
//                        comment.setVisibility(View.VISIBLE);
//                    }
                }
                else if(rating == 2){
                    if(language.equalsIgnoreCase("En")) {
                        rate_text.setText("Bad");
                        wrong_text.setText("Tell us what went wrong ?");
                    }else if(language.equalsIgnoreCase("Ar")){
                        rate_text.setText("سئ");
                        wrong_text.setText("أخبرنا أين كان الخطأ؟");
                    }
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    feedback_layout.setVisibility(View.VISIBLE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating2, language);
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
//                    if(pos !=-1 && ratingsArrayList.get(0).getRatingDetails().get(pos).getRemarks().equalsIgnoreCase("Other")){
//                        comment.setVisibility(View.VISIBLE);
//                    }
                }
                else if(rating == 3){
                    if(language.equalsIgnoreCase("En")) {
                        rate_text.setText("Ok");
                        wrong_text.setText("Tell us what went wrong ?");
                    }else if(language.equalsIgnoreCase("Ar")){
                        rate_text.setText("حسنا");
                        wrong_text.setText("أخبرنا أين كان الخطأ؟");
                    }
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating3, language);
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
//                    if(pos !=-1 && ratingsArrayList.get(0).getRatingDetails().get(pos).getRemarks().equalsIgnoreCase("Other")){
//                        comment.setVisibility(View.VISIBLE);
//                    }
                }
                else if(rating == 4){
                    if(language.equalsIgnoreCase("En")) {
                        rate_text.setText("Good");
                        wrong_text.setText("Where do you feel we should improve ?");
                    }else if(language.equalsIgnoreCase("Ar")){
                        rate_text.setText("جيد");
                        wrong_text.setText("أين تجد شعورك فيما يجب تحسينه؟");
                    }
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating4, language);
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
//                    if(pos !=-1 && ratingsArrayList.get(0).getRatingDetails().get(pos).getRemarks().equalsIgnoreCase("Other")){
//                        comment.setVisibility(View.VISIBLE);
//                    }
                }
                else if(rating == 5){
                    if(language.equalsIgnoreCase("En")) {
                        rate_text.setText("Excellent");
                        wrong_text.setText("What do you like best about us ?");
                    }else if(language.equalsIgnoreCase("Ar")){
                        rate_text.setText("ممتاز");
                        wrong_text.setText("ما هو الشئ الممتاز لدينا وأنت تفضله؟");
                    }
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating5, language);
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rateBar.getRating() > 0) {
                    JSONObject parent = new JSONObject();
                    boolean vaild = false;
                    try {

                        JSONArray mainItem = new JSONArray();

                        if (str != null && str.size() > 0 ) {
                            for (int i = 0; i < str.size(); i++) {
                                JSONObject mainObj = new JSONObject();
                                mainObj.put("UserId", userId);
                                mainObj.put("OrderId", orderid);
                                mainObj.put("RatingId", ratingId.get(i));
                                mainObj.put("Ratings", rateBar.getRating());
                                if (str.get(i).equals("Other") || str.get(i).equals("الآخرين")) {
                                    if (comment.getText().toString().length() > 0) {
                                        mainObj.put("Comments", comment.getText().toString());
                                    }
                                    else {
                                        mainObj.put("Comments", str.get(i));
                                    }
                                } else {
                                    mainObj.put("Comments", str.get(i));
                                }
                                mainItem.put(mainObj);
                            }
                            parent.put("usrRating", mainItem);
                        } else {
                            JSONObject mainObj = new JSONObject();
                                mainObj.put("UserId", userId);
                                mainObj.put("OrderId", orderid);
                                mainObj.put("RatingId", "");
                                mainObj.put("Ratings", rateBar.getRating());
                                mainObj.put("Comments", "");
                                mainItem.put(mainObj);
                                parent.put("usrRating", mainItem);

//                            if (rateBar.getRating() == 5) {
//                                if(language.equalsIgnoreCase("En")) {
//                                    Toast.makeText(RatingActivity.this, "Please select what do you like best about us ?", Toast.LENGTH_SHORT).show();
//                                }else if(language.equalsIgnoreCase("Ar")){
//                                    Toast.makeText(RatingActivity.this, "Please select What do you like best about us ?", Toast.LENGTH_SHORT).show();
//                                }
//                             } else
//                                 if (rateBar.getRating() == 4) {
//                                if(language.equalsIgnoreCase("En")) {
//                                    Toast.makeText(RatingActivity.this, "Please select where do you feel we should improve ?", Toast.LENGTH_SHORT).show();
//                                }else if(language.equalsIgnoreCase("Ar")){
//                                    Toast.makeText(RatingActivity.this, "Please select What do you like best about us ?", Toast.LENGTH_SHORT).show();
//                                }
//                                } else {
//                                if(language.equalsIgnoreCase("En")) {
//                                    Toast.makeText(RatingActivity.this, "Please tell us what went wrong ?", Toast.LENGTH_SHORT).show();
//                                }else if(language.equalsIgnoreCase("Ar")){
//                                    Toast.makeText(RatingActivity.this, "Please select What do you like best about us ?", Toast.LENGTH_SHORT).show();
//                                }
//                                }
                        }

                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    Log.i("TAG", "" + parent.toString());
//                    if (vaild)
                        new SubmitRating().execute(parent.toString());
                }
            }
        });
    }
    public class SubmitRating extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        String response;
        ProgressDialog dialog;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(RatingActivity.this);
            dialog = ProgressDialog.show(RatingActivity.this, "",
                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.RATING_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(RatingActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            Toast.makeText(RatingActivity.this, "Rating submitted successfully.", Toast.LENGTH_SHORT).show();
                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
