package com.cs.oregano.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.GPSTracker;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.adapters.TrackItemsAdapter;
import com.cs.oregano.adapters.TrackStepsAdapter;
import com.cs.oregano.model.Rating;
import com.cs.oregano.model.RatingDetails;
import com.cs.oregano.model.TrackItems;
import com.cs.oregano.model.TrackSteps;
import com.cs.oregano.model.Track_itemtypes;
import com.cs.oregano.model.Track_types;
import com.cs.oregano.widgets.NetworkUtil;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by CS on 20-06-2016.
 */
public class TrackOrderSteps extends AppCompatActivity {
    Toolbar toolbar;
    ListView trackListView, orderListView;
    TextView emptyView;
    TextView title, orderId_tv, totalAmount, promoAmount, netAmount, vatAmount, mamount;
    public static TextView totalQty;
    RelativeLayout summary_layout,list_expand;
    ImageView share, cancelOrder, plus;
    ArrayList<TrackSteps> arrayList = new ArrayList<>();
    Boolean isFirstTime = true;

    DecimalFormat decim =new DecimalFormat("0.00");

    TrackStepsAdapter mAdapter;
    TrackItemsAdapter mOrderAdapter;
    private Timer timer = new Timer();

    public static String driverName, driverNumber, driverId;

    Double lat, longi;
    String  latitude, longitude, phone, totalPrice, subTotal, vatCharges, vatPercentage;
    String orderNumber, OrderStatus , StoreName, StoreNameAr, OrderType = "", StoreAddress, TrackingTime, ExpectedTime, address;
    String orderId;
    SharedPreferences userPrefs;
    String userId;
    SharedPreferences languagePrefs;
    String language;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;

    private ArrayList<TrackItems> ItemsList = new ArrayList<>();
    ArrayList<Track_itemtypes> list = new ArrayList<>();

    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefsEditor;
    Boolean show=true;


    int ratingFromService;
    RelativeLayout rate_layout;
    MaterialRatingBar ratingBar;
    TextView cancel;
    final ArrayList<RatingDetails> rateArray = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.track_order_steps);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.track_order_step_arabic);
        }
        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor  = orderPrefs.edit();
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        orderId = getIntent().getExtras().getString("orderId");
        trackListView = (ListView) findViewById(R.id.track_list);
        orderListView = (ListView) findViewById(R.id.items_list);
        orderId_tv = (TextView) findViewById(R.id.orderId);
        totalQty = (TextView) findViewById(R.id.totalQty);
        totalAmount = (TextView) findViewById(R.id.totalAmount);
        promoAmount = (TextView) findViewById(R.id.promoAmount);
        mamount = (TextView) findViewById(R.id.subAmount);
        netAmount = (TextView) findViewById(R.id.netAmount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        plus = (ImageView) findViewById(R.id.list_plus);
        summary_layout = (RelativeLayout) findViewById(R.id.summary_layout);
        emptyView = (TextView) findViewById(R.id.empty_view);
        title = (TextView) findViewById(R.id.header_title);
        ratingBar = (MaterialRatingBar) findViewById(R.id.ratingBar1);
        cancel = (TextView) findViewById(R.id.skip_rating);
        rate_layout = (RelativeLayout) findViewById(R.id.rate_layout);
        rate_layout.setVisibility(View.GONE);
        share = (ImageView) findViewById(R.id.track_share);
        cancelOrder = (ImageView) findViewById(R.id.cancel_order);
        list_expand = (RelativeLayout) findViewById(R.id.list_expand);
        list_expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(show){
                    orderListView.setVisibility(View.VISIBLE);
                    summary_layout.setVisibility(View.VISIBLE);
                    plus.setImageResource(R.drawable.minus);
                    show=false;
                }
                else {
                    orderListView.setVisibility(View.GONE);
                    summary_layout.setVisibility(View.GONE);
                    plus.setImageResource(R.drawable.plus);
                    show=true;
                }
            }
        });
        if(language.equalsIgnoreCase("En")){
        }else if(language.equalsIgnoreCase("Ar")){
            title.setText("تابع الطلب");
            emptyView.setText("لا يوجد منتجات في السلة");
        }

        summary_layout.setVisibility(View.GONE);
        orderListView.setVisibility(View.GONE);

        mOrderAdapter = new TrackItemsAdapter(TrackOrderSteps.this, list, language);
        orderListView.setAdapter(mOrderAdapter);

        mAdapter = new TrackStepsAdapter(TrackOrderSteps.this, arrayList, language);
        trackListView.setAdapter(mAdapter);
//        trackListView.setEmptyView(emptyView);
        driverName = "";
        driverNumber = "";
        driverId = "";
        new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL+orderId+"&userId="+userId);
//        new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL);

        Log.e("TAG","order id"+orderId);
        Log.e("TAG","user id"+userId);
        timer.schedule(new MyTimerTask(), 30000, 30000);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog();
            }
        });

        cancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(TrackOrderSteps.this, android.R.style.Theme_Material_Light_Dialog));
                String title= "", msg= "", posBtn = "", negBtn = "";
                if(language.equalsIgnoreCase("En")){
                    posBtn = "Yes";
                    negBtn = "No";
                    title = "Oregano";
                    msg = "Do you want to cancel the order?";
                }else if(language.equalsIgnoreCase("Ar")){
                    posBtn = "نعم";
                    negBtn = "لا";
                    title = "اوريجانو";
                    msg = "هل ترغب في إلغاء الطلب ؟";
                }
                // set title
                alertDialogBuilder.setTitle(title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                JSONObject parent = new JSONObject();
                                try {

                                    JSONArray mainItem = new JSONArray();

                                    JSONObject mainObj = new JSONObject();
                                    mainObj.put("OrderId", orderId);
                                    mainObj.put("UserId", userId);
                                    mainObj.put("Device_token", SplashActivity.regid);

                                    mainItem.put(mainObj);

                                    parent.put("CancelOrder", mainItem);
                                }catch (JSONException je){
                                    je.printStackTrace();
                                }
                                new CancelOrder().execute(parent.toString());
                            }
                        }).setNegativeButton(negBtn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });


        trackListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(arrayList.size()==5){
                    if(arrayList.get(3).getStatus().equalsIgnoreCase("On The Way!")){
                        if(arrayList.get(4).getTrackTime().equals("") && !arrayList.get(3).getTrackTime().equals("")){
                            Intent i = new Intent(TrackOrderSteps.this, LiveTrackingActivity.class);
                            i.putExtra("driver_name", driverName);
                            i.putExtra("driver_number", driverNumber);
                            i.putExtra("driver_id", driverId);
                            i.putExtra("order_id", arrayList.get(3).getOrderId());
                            i.putExtra("exp_time", arrayList.get(3).getExpectedTime());
                            startActivity(i);
                        }
                    }
                }
            }
        });
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new GetOrderTrackDetails().cancel(true);
                        new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL+orderId+"&userId="+userId);
//                        new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL);
                    }
                });
        }
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        timer.cancel();
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public class GetOrderTrackDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {

            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderSteps.this);
            if(isFirstTime) {
                dialog = ProgressDialog.show(TrackOrderSteps.this, "",
                        "Please wait...");
                isFirstTime = false;
            }
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            arrayList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(TrackOrderSteps.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(TrackOrderSteps.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String failure = jo.getString("Failure");
                            } catch (JSONException jse) {

                                try {
                                    JSONArray ja = jo.getJSONArray("Received");
                                    cancelOrder.setEnabled(true);
                                    cancelOrder.setAlpha(1f);
                                    cancelOrder.setClickable(true);
//                                for (int i = 0; i<ja.length(); i++) {

                                    TrackSteps ts = new TrackSteps();
                                    JSONObject jo1 = ja.getJSONObject(0);
                                    orderNumber = jo1.getString("InvoiceNo");
                                    orderId = jo1.getString("OrderId");
                                    final String[] order_no = orderNumber.split("-");
                                    orderId_tv.setText("#"+order_no[1]);
                                    OrderStatus = "Order!";
                                    StoreName = jo1.getString("StoreName");
                                    StoreNameAr = jo1.getString("StoreName_ar");
                                    OrderType = jo1.getString("OrderType");
                                    StoreAddress = jo1.getString("StoreAddress");
                                    TrackingTime = jo1.getString("TrackingTime");
                                    ExpectedTime = jo1.getString("ExpectedTime");
                                    address = jo1.getString("Address");
                                    latitude = jo1.getString("Latitude");
                                    longitude = jo1.getString("Longitude");
                                    phone = jo1.getString("phone");
                                    totalPrice = jo1.getString("Total_Price");
                                    subTotal = jo1.getString("SubTotal");
                                    vatCharges = jo1.getString("VatCharges");
                                    vatPercentage = jo1.getString("VatPercentage");
                                    float promoAmt = jo1.getInt("PromoAmt");
                                    float total = Float.parseFloat(subTotal) - Float.parseFloat(String.valueOf(promoAmt));
                                    ratingFromService = jo1.getInt("Rating");
                                    promoAmount.setText(""+decim.format(Float.parseFloat(String.valueOf(promoAmt))));
                                    mamount.setText(""+decim.format(Float.parseFloat(String.valueOf(total))));
                                    vatAmount.setText(""+decim.format(Float.parseFloat(vatCharges)));
                                    netAmount.setText(""+decim.format(Float.parseFloat(String.valueOf(totalPrice))));
                                    totalAmount.setText(""+decim.format(Float.parseFloat(subTotal)));
                                    totalQty.setText(jo1.getString("QuantityCount"));

                                    ItemsList.clear();
                                    list.clear();
                                    TrackItems ti = new TrackItems();
                                    ArrayList<Track_itemtypes> itemtypesList = new ArrayList<>();
                                    JSONArray itemsArray = jo1.getJSONArray("items");
                                    for(int i=0; i<itemsArray.length(); i++){
                                        Track_itemtypes items = new Track_itemtypes();
                                        ArrayList<Track_types> itemsList = new ArrayList<>();

                                        JSONObject jo2 = itemsArray.getJSONObject(i);
                                        items.setItem_name(jo2.getString("ItmName"));
                                        items.setItem_name_ar(jo2.getString("ItmName_ar"));
                                        items.setPrice(jo2.getString("price"));
                                        items.setQuantity(jo2.getString("Quantity"));

                                        JSONArray AddlArray = jo2.getJSONArray("oAdd");
                                        for(int j=0; j<AddlArray.length(); j++){
                                            Track_types types = new Track_types();

                                            JSONObject jo3 = AddlArray.getJSONObject(j);
                                            types.setAddl_name(jo3.getString("AName"));
                                            types.setAddl_name_ar(jo3.getString("ANameAr"));
                                            types.setAddl_price(jo3.getString("APrice"));

                                            itemsList.add(types);
                                        }
                                        items.setItemtypes(itemsList);
                                        itemtypesList.add(items);
                                        list.add(items);
                                    }
                                    ti.setItems(itemtypesList);
                                    ItemsList.add(ti);

                                    RatingDetails rd = new RatingDetails();
                                    JSONArray jA = jo1.getJSONArray("Ratings");
                                    JSONObject obj = jA.getJSONObject(0);
                                    rd.setGivenrating("5");
                                    JSONArray ratingArray = obj.getJSONArray("5");
                                    ArrayList<Rating> ratingsArrayList = new ArrayList<>();
                                    for (int i = 0; i < ratingArray.length(); i++) {
                                        JSONObject object1 = ratingArray.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);

                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray1 = obj.getJSONArray("4");
                                    rd.setGivenrating("4");
                                    for (int i = 0; i < ratingArray1.length(); i++) {
                                        JSONObject object1 = ratingArray1.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray2 = obj.getJSONArray("3");
                                    rd.setGivenrating("3");
                                    for (int i = 0; i < ratingArray2.length(); i++) {
                                        JSONObject object1 = ratingArray2.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);

                                    JSONArray ratingArray3 = obj.getJSONArray("2");
                                    rd.setGivenrating("2");
                                    for (int i = 0; i < ratingArray3.length(); i++) {
                                        JSONObject object1 = ratingArray3.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray4 = obj.getJSONArray("1");
                                    rd.setGivenrating("1");
                                    for (int i = 0; i < ratingArray4.length(); i++) {
                                        JSONObject object1 = ratingArray4.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);

                                    rateArray.add(rd);

//

                                    ts.setOrderNumber(orderNumber);
                                    ts.setStatus(OrderStatus);
                                    ts.setStoreName(StoreName);
                                    ts.setStoreNameAr(StoreNameAr);
                                    ts.setStoreAddress(StoreAddress);
                                    ts.setExpectedTime(ExpectedTime);
                                    ts.setTrackTime(TrackingTime);
                                    ts.setAddress(address);
                                    ts.setOrderId(orderId);


                                    arrayList.add(ts);

//                                }
                                } catch (JSONException je) {
                                    je.printStackTrace();

                                }


                                try {
                                    JSONArray ja = jo.getJSONArray("Cancel");
                                    cancelOrder.setVisibility(View.GONE);
                                    share.setClickable(false);
                                    share.setEnabled(false);
                                    share.setAlpha(0.5f);
//                                    cancelOrder.setClickable(true);
//                                for (int i = 0; i<ja.length(); i++) {

                                    TrackSteps ts = new TrackSteps();
                                    JSONObject jo1 = ja.getJSONObject(0);
                                    TrackingTime = jo1.getString("TrackingTime");
                                    OrderStatus = "Cancel!";
                                    ts.setOrderNumber(orderNumber);
                                    ts.setStatus(OrderStatus);
                                    ts.setStoreName(StoreName);
                                    ts.setStoreNameAr(StoreNameAr);
                                    ts.setStoreAddress(StoreAddress);
                                    ts.setExpectedTime(ExpectedTime);
                                    ts.setTrackTime(TrackingTime);
                                    ts.setAddress(address);
                                    ts.setOrderId(orderId);


                                    arrayList.add(ts);

//                                }
                                } catch (JSONException jsone) {

                                    try {
                                        JSONArray ja = jo.getJSONArray("Confirmed");
                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setAlpha(1f);
                                        cancelOrder.setClickable(true);
//                                for (int i = 0; i<ja.length(); i++) {

                                        TrackSteps ts = new TrackSteps();
                                        JSONObject jo1 = ja.getJSONObject(0);
                                        TrackingTime = jo1.getString("TrackingTime");
                                        OrderStatus = "Confirmed!";

                                        ts.setOrderNumber(orderNumber);
                                        ts.setStatus(OrderStatus);
                                        ts.setStoreName(StoreName);
                                        ts.setStoreNameAr(StoreNameAr);
                                        ts.setStoreAddress(StoreAddress);
                                        ts.setExpectedTime(ExpectedTime);
                                        ts.setTrackTime(TrackingTime);
                                        ts.setAddress(address);
                                        ts.setOrderId(orderId);


                                        arrayList.add(ts);

//                                }
                                    } catch (JSONException je) {

                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setAlpha(1f);
                                        cancelOrder.setClickable(true);
                                        TrackSteps ts = new TrackSteps();
                                        OrderStatus = "Confirmed!";

                                        ts.setOrderNumber(orderNumber);
                                        ts.setStatus(OrderStatus);
                                        ts.setStoreName(StoreName);
                                        ts.setStoreNameAr(StoreNameAr);
                                        ts.setStoreAddress(StoreAddress);
                                        ts.setExpectedTime(ExpectedTime);
                                        ts.setTrackTime("");
                                        ts.setAddress(address);
                                        ts.setOrderId(orderId);
                                        arrayList.add(ts);
                                    }

                                    try {
                                        JSONArray ja = jo.getJSONArray("Ready");

                                        cancelOrder.setEnabled(false);
                                        cancelOrder.setAlpha(0.5f);
                                        cancelOrder.setClickable(false);
//                                for (int i = 0; i<ja.length(); i++) {

                                        TrackSteps ts = new TrackSteps();
                                        JSONObject jo1 = ja.getJSONObject(0);
                                        TrackingTime = jo1.getString("TrackingTime");
                                        OrderStatus = "Ready!";

                                        ts.setOrderNumber(orderNumber);
                                        ts.setStatus(OrderStatus);
                                        ts.setStoreName(StoreName);
                                        ts.setStoreNameAr(StoreNameAr);
                                        ts.setStoreAddress(StoreAddress);
                                        ts.setExpectedTime(ExpectedTime);
                                        ts.setTrackTime(TrackingTime);
                                        ts.setAddress(address);
                                        ts.setOrderId(orderId);


                                        arrayList.add(ts);

//                                }
                                    } catch (JSONException je) {

                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setAlpha(1f);
                                        cancelOrder.setClickable(true);
                                        TrackSteps ts = new TrackSteps();
                                        OrderStatus = "Ready!";

                                        ts.setOrderNumber(orderNumber);
                                        ts.setStatus(OrderStatus);
                                        ts.setStoreName(StoreName);
                                        ts.setStoreNameAr(StoreNameAr);
                                        ts.setStoreAddress(StoreAddress);
                                        ts.setExpectedTime(ExpectedTime);
                                        ts.setTrackTime("");
                                        ts.setAddress(address);
                                        ts.setOrderId(orderId);


                                        arrayList.add(ts);

                                    }

                                    if (OrderType.equalsIgnoreCase("Delivery")) {
                                        try {
                                            JSONArray ja = jo.getJSONArray("OnTheWay");

                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
//                                for (int i = 0; i<ja.length(); i++) {

                                            TrackSteps ts = new TrackSteps();
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            TrackingTime = jo1.getString("TrackingTime");
                                            JSONArray driverArray = jo1.getJSONArray("DriverName");
                                            JSONObject driverObj = driverArray.getJSONObject(0);
                                            driverName = driverObj.getString("FullName");
                                            driverNumber = driverObj.getString("Mobile");
                                            driverId = jo1.getString("UserId");
                                            OrderStatus = "On The Way!";

                                            ts.setOrderNumber(orderNumber);
                                            ts.setStatus(OrderStatus);
                                            ts.setStoreName(StoreName);
                                            ts.setStoreNameAr(StoreNameAr);
                                            ts.setStoreAddress(StoreAddress);
                                            ts.setExpectedTime(ExpectedTime);
                                            ts.setTrackTime(TrackingTime);
                                            ts.setAddress(address);
                                            ts.setOrderId(orderId);

                                            arrayList.add(ts);

//                                }
                                        } catch (JSONException je) {

                                            TrackSteps ts = new TrackSteps();
                                            OrderStatus = "On The Way!";

                                            ts.setOrderNumber(orderNumber);
                                            ts.setStatus(OrderStatus);
                                            ts.setStoreName(StoreName);
                                            ts.setStoreNameAr(StoreNameAr);
                                            ts.setStoreAddress(StoreAddress);
                                            ts.setExpectedTime(ExpectedTime);
                                            ts.setTrackTime("");
                                            ts.setAddress(address);
                                            ts.setOrderId(orderId);

                                            arrayList.add(ts);

                                        }


                                        try {
                                            JSONArray ja = jo.getJSONArray("Delivered");

                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
//                                for (int i = 0; i<ja.length(); i++) {

                                            TrackSteps ts = new TrackSteps();
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            TrackingTime = jo1.getString("TrackingTime");
                                            OrderStatus = "Delivered!";

                                            if(ratingFromService == 0) {
                                                rate_layout.setVisibility(View.VISIBLE);
                                                cancel.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        rate_layout.setVisibility(View.GONE);
                                                    }
                                                });

                                                ratingBar.setRating(0);
                                                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                                    @Override
                                                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                        if (fromUser) {
                                                            Intent intent = new Intent(TrackOrderSteps.this, RatingActivity.class);
                                                            intent.putExtra("rating", rating);
                                                            intent.putExtra("array", rateArray);
                                                            intent.putExtra("userid", userId);
                                                            intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                                            startActivity(intent);
                                                            rate_layout.setVisibility(View.GONE);
                                                        }
                                                    }
                                                });
                                            }

                                            ts.setOrderNumber(orderNumber);
                                            ts.setStatus(OrderStatus);
                                            ts.setStoreName(StoreName);
                                            ts.setStoreNameAr(StoreNameAr);
                                            ts.setStoreAddress(StoreAddress);
                                            ts.setExpectedTime(ExpectedTime);
                                            ts.setTrackTime(TrackingTime);
                                            ts.setAddress(address);
                                            ts.setOrderId(orderId);


                                            arrayList.add(ts);

//                                }
                                        } catch (JSONException je) {

                                            TrackSteps ts = new TrackSteps();
                                            OrderStatus = "Delivered!";

                                            ts.setOrderNumber(orderNumber);
                                            ts.setStatus(OrderStatus);
                                            ts.setStoreName(StoreName);
                                            ts.setStoreNameAr(StoreNameAr);
                                            ts.setStoreAddress(StoreAddress);
                                            ts.setExpectedTime(ExpectedTime);
                                            ts.setTrackTime("");
                                            ts.setAddress(address);
                                            ts.setOrderId(orderId);


                                            arrayList.add(ts);
                                        }

                                    } else {
                                        try {
                                            JSONArray ja = jo.getJSONArray("Delivered");

                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
//                                for (int i = 0; i<ja.length(); i++) {

                                            TrackSteps ts = new TrackSteps();
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            TrackingTime = jo1.getString("TrackingTime");
                                            OrderStatus = "Served!";

                                            if(ratingFromService == 0) {

                                                rate_layout.setVisibility(View.VISIBLE);
                                                cancel.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        rate_layout.setVisibility(View.GONE);
                                                    }
                                                });

                                                ratingBar.setRating(0);
                                                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                                    @Override
                                                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                        if (fromUser) {
                                                            Intent intent = new Intent(TrackOrderSteps.this, RatingActivity.class);
                                                            intent.putExtra("rating", rating);
                                                            intent.putExtra("array", rateArray);
                                                            intent.putExtra("userid", userId);
                                                            intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                                            startActivity(intent);
                                                            rate_layout.setVisibility(View.GONE);
                                                        }
                                                    }
                                                });
                                    }

                                            ts.setOrderNumber(orderNumber);
                                            ts.setStatus(OrderStatus);
                                            ts.setStoreName(StoreName);
                                            ts.setStoreNameAr(StoreNameAr);
                                            ts.setStoreAddress(StoreAddress);
                                            ts.setExpectedTime(ExpectedTime);
                                            ts.setTrackTime(TrackingTime);
                                            ts.setAddress(address);
                                            ts.setOrderId(orderId);


                                            arrayList.add(ts);

//                                }
                                        } catch (JSONException je) {

                                            TrackSteps ts = new TrackSteps();
                                            OrderStatus = "Served!";

                                            ts.setOrderNumber(orderNumber);
                                            ts.setStatus(OrderStatus);
                                            ts.setStoreName(StoreName);
                                            ts.setStoreNameAr(StoreNameAr);
                                            ts.setStoreAddress(StoreAddress);
                                            ts.setExpectedTime(ExpectedTime);
                                            ts.setTrackTime("");
                                            ts.setAddress(address);
                                            ts.setOrderId(orderId);


                                            arrayList.add(ts);
                                        }

                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(TrackOrderSteps.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            try {
                if(dialog != null) {
                    dialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (orderPrefs.getString("order_id", "").equals(orderId)){
                if (orderPrefs.getString("order_status", "close").equalsIgnoreCase("close")){
                    cancelOrder.setEnabled(false);
                    cancelOrder.setAlpha(0.5f);
                    cancelOrder.setClickable(false);
                }else{
                    cancelOrder.setEnabled(true);
                    cancelOrder.setAlpha(1f);
                    cancelOrder.setClickable(true);
                }
            }else{
                cancelOrder.setEnabled(false);
                cancelOrder.setAlpha(0.5f);
                cancelOrder.setClickable(false);
            }
            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }

    public void dialog() {

        final Dialog dialog2 = new Dialog(TrackOrderSteps.this);

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if(language.equalsIgnoreCase("En")){
            dialog2.setContentView(R.layout.track_order_share_dialog);
        }else if(language.equalsIgnoreCase("Ar")){
            dialog2.setContentView(R.layout.track_order_share_dialog);
        }
        dialog2.setCanceledOnTouchOutside(true);
        TextView share = (TextView) dialog2.findViewById(R.id.share_order);
        TextView getDirection = (TextView) dialog2.findViewById(R.id.get_direction);
        TextView callStore = (TextView) dialog2.findViewById(R.id.call_store);
        TextView cancel = (TextView) dialog2.findViewById(R.id.cancel_button);


        if(OrderType.equalsIgnoreCase("Delivery")){
            getDirection.setVisibility(View.GONE);
        }

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessLocation()) {
                        requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                    } else {
                        getGPSCoordinates();
                    }
                }else {
                    getGPSCoordinates();
                }

            }
        });

        getDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+ latitude + "," + longitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        callStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(TrackOrderSteps.this, android.R.style.Theme_Material_Light_Dialog));

//                        if(language.equalsIgnoreCase("En")) {
                            // set title
                            alertDialogBuilder.setTitle("Oregano");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("Please allow phone call permission in settings")
                                    .setCancelable(false)
                                    .setNeutralButton("Settings", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                    Uri.fromParts("package", getPackageName(), null));
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    })
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
//                        }else if(language.equalsIgnoreCase("Ar")){
//                            // set title
//                            alertDialogBuilder.setTitle("اوريجانو");
//
//                            // set dialog message
//                            alertDialogBuilder
//                                    .setMessage("الرقم السري غير صحيح")
//                                    .setCancelable(false)
//                                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.dismiss();
//                                        }
//                                    });
//                        }


                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();

                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +phone));
                        startActivity(intent);
                    }
                }else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +phone));
                    startActivity(intent);
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });




        dialog2.show();

    }



    public void getGPSCoordinates(){
        gps = new GPSTracker(TrackOrderSteps.this);
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "Oregano Order Details \n Order No: " + orderNumber + "\n Expected Time: " + ExpectedTime + "\n Store: " + StoreName + "\n Amount: " + totalPrice + "\nhttp://maps.google.com/maps?saddr="+lat+","+longi+"&daddr=" + latitude + "," + longitude);
                ;

                startActivity(Intent.createChooser(intent, "Share"));
                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(TrackOrderSteps.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(TrackOrderSteps.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    public class CancelOrder extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String  networkStatus;
        JSONObject parent = new JSONObject();
        InputStream inputStream = null;
        String result = "";
         ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderSteps.this);
            dialog = ProgressDialog.show(TrackOrderSteps.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.INSERT_ORDER_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(arg0[0]);

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            result = convertInputStreamToString(inputStream);
                            return result;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + result);
            }else {
                return "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(TrackOrderSteps.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Oregano");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    try {
                        JSONObject jo= new JSONObject(result);

                            JSONArray ja = jo.getJSONArray("Success");
                        new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL+orderId+"&userId="+userId);
                        Toast.makeText(TrackOrderSteps.this,ja.toString().replace("[","").replace("]","").replace("\"",""),Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result1);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

}
