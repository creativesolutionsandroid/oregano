package com.cs.oregano.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.oregano.Constants;
import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.adapters.OrderHistoryAdapter;
import com.cs.oregano.model.OrderHistory;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 19-06-2016.
 */
public class OrderHistoryActivity extends AppCompatActivity {
    private DataBaseHelper myDbHelper;
    private ArrayList<OrderHistory> transactionsList = new ArrayList<>();
    private SwipeMenuListView orderHistoryListView;
    TextView emptyView;
    private OrderHistoryAdapter mAdapter;
    SharedPreferences userPrefs;
    String userId;
    TextView title;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.order_history);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.order_history);
        }
        myDbHelper = new DataBaseHelper(OrderHistoryActivity.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        orderHistoryListView = (SwipeMenuListView) findViewById(R.id.fav_order_listview);
        emptyView = (TextView) findViewById(R.id.empty_view);
        title = (TextView) findViewById(R.id.header_title);
        mAdapter = new OrderHistoryAdapter(OrderHistoryActivity.this, transactionsList, language);
        orderHistoryListView.setAdapter(mAdapter);
        Log.i("TAG","url "+Constants.ORDER_HISTORY_URL+userId);
        new GetOrderDetails().execute(Constants.ORDER_HISTORY_URL+userId);

        orderHistoryListView.setEmptyView(emptyView);

        if(language.equalsIgnoreCase("En")){
            title.setText("Order History");
        }else if(language.equalsIgnoreCase("Ar")){
            title.setText("جميع الطلبات");
            emptyView.setText("لا يوجد منتجات في السلة");
        }

        orderHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(OrderHistoryActivity.this, TrackOrderSteps.class);
                intent.putExtra("orderId",transactionsList.get(position).getOrderId());
                startActivity(intent);
            }
        });

        SwipeMenuCreator creator2 = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                switch (menu.getViewType()) {
                    case 0:
                        // create menu of type 0
                        SwipeMenuItem viewItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        viewItem.setBackground(new ColorDrawable(Color.rgb(0x80,
                                0x80, 0x88)));
                        // set item width
                        viewItem.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            viewItem.setTitle("View");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            viewItem.setTitle("مراجعة");
                        }
                        // set item title fontsize
                        viewItem.setTitleSize(18);
                        // set item title font color
                        viewItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(viewItem);

                        // create "delete" item
                        SwipeMenuItem reorderItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        reorderItem.setBackground(new ColorDrawable(Color.rgb(0x25,
                                0xAE, 0x88)));
                        // set item width
                        reorderItem.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            reorderItem.setTitle("Re-order");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            reorderItem.setTitle("إعادة الطلب");
                        }
                        // set item title fontsize
                        reorderItem.setTitleSize(18);
                        // set item title font color
                        reorderItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(reorderItem);

                        // create "delete" item
                        SwipeMenuItem deleteItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                                0x3F, 0x25)));
                        // set item width
                        deleteItem.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            deleteItem.setTitle("Delete");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            deleteItem.setTitle("حذف");
                        }
                        // set item title fontsize
                        deleteItem.setTitleSize(18);
                        // set item title font color
                        deleteItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(deleteItem);
                        break;
                    case 1:
                        // create menu of type 1
                        SwipeMenuItem viewItem1 = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        viewItem1.setBackground(new ColorDrawable(Color.rgb(0x80,
                                0x80, 0x88)));
                        // set item width
                        viewItem1.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            viewItem1.setTitle("View");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            viewItem1.setTitle("مراجعة");
                        }
                        // set item title fontsize
                        viewItem1.setTitleSize(18);
                        // set item title font color
                        viewItem1.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(viewItem1);

                        // create "delete" item
                        SwipeMenuItem reorderItem1 = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        reorderItem1.setBackground(new ColorDrawable(Color.rgb(0x25,
                                0xAE, 0x88)));
                        // set item width
                        reorderItem1.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            reorderItem1.setTitle("Re-order");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            reorderItem1.setTitle("إعادة الطلب");
                        }
                        // set item title fontsize
                        reorderItem1.setTitleSize(18);
                        // set item title font color
                        reorderItem1.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(reorderItem1);
                        break;
                }
            }

        };
        orderHistoryListView.setMenuCreator(creator2);

        // step 2. listener item click event
        orderHistoryListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        String orderId1 = transactionsList.get(position).getOrderId();
                        Intent intent = new Intent(OrderHistoryActivity.this, ViewDetailsActivity.class);
                        intent.putExtra("orderid",orderId1);
                        startActivity(intent);
                        break;
                    case 1:
                        String orderId = transactionsList.get(position).getOrderId();
                        new GetCheckoutOrderDetails().execute(Constants.ORDERD_DETAILS_URL+orderId+"&Userid="+userId);
//                        new DeleteFavOrder().execute(Constants.DELETE_ORDER_FROM_HISTORY+transactionsList.get(position).getOrderId());
                        break;
                    case 2:
                        new DeleteFavOrder().execute(Constants.DELETE_ORDER_FROM_HISTORY+transactionsList.get(position).getOrderId());
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }



    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderHistoryActivity.this);
            dialog = ProgressDialog.show(OrderHistoryActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderHistoryActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

//                        try {
//                            JSONObject jo= new JSONObject(result);
//                            String s = jo.getString("Success");
                        new GetOrderDetails().execute(Constants.ORDER_HISTORY_URL+userId);
//                            Toast.makeText(FavoriteOrdersActivity.this, "Order deleted successfully", Toast.LENGTH_SHORT).show();

//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Toast.makeText(FavoriteOrdersActivity.this, "Can not reach server", Toast.LENGTH_SHORT).show();
//                        }

                    }
                }

            }else {
                Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    public class GetCheckoutOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderHistoryActivity.this);
            dialog = ProgressDialog.show(OrderHistoryActivity.this, "",
                    "Loading items...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderHistoryActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            HashMap<String, String> values = new HashMap<>();

                            JSONObject jo = new JSONObject(result);

                            JSONArray ja = jo.getJSONArray("SubItem");
                            myDbHelper.deleteOrderTable();
                            for(int i = 0; i< ja.length(); i++){
                                String ids = "0", additionalsStr = "",additionalsStrAr = "", additionalsPrice = "";
                                String categoryId = "", itemId = "", itemName = "",itemNameAr = "", itemImage = "", itemDesc = "",itemDescAr = "", itemType = "";
                                String price = "",nonDelivery= "";
                                float additionPrice = 0;
                                float priceAd = 0, finalPrice = 0;
                                int quantity = 0;
                                JSONArray ja1 = ja.getJSONArray(i);
                                for(int j=0;j<ja1.length();j++){
                                    if(j==0){
                                        JSONObject jo1 = ja1.getJSONObject(0);
                                        itemId = jo1.getString("ItemId");
                                        itemName = jo1.getString("ItemName");
                                        itemNameAr = jo1.getString("ItemName_Ar");
                                        itemImage = jo1.getString("Images");
                                        itemDesc = jo1.getString("Description");
                                        itemDescAr = jo1.getString("Description_Ar");
                                        itemType = jo1.getString("Size");
                                        quantity = jo1.getInt("Qty");
                                        price = jo1.getString("ItemPrice");
                                        categoryId = jo1.getString("RamadanCategoryId");
                                        nonDelivery = jo1.getString("IsDelivery");
                                    }else{
                                        JSONArray ja2 = ja1.getJSONArray(j);
                                        for(int k =0; k< ja2.length(); k++){
                                            JSONObject jo2 = ja2.getJSONObject(k);
                                            if(!jo2.getString("AdditionalID").equals("0")) {
                                                if (ids.equalsIgnoreCase("0")) {
                                                    ids = jo2.getString("AdditionalID");
                                                    additionalsStr = jo2.getString("AdditionalName");
                                                    additionalsStrAr = jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = jo2.getString("AdditionalPrice");
                                                }else{
                                                    ids = ids +","+ jo2.getString("AdditionalID");
                                                    additionalsStr = additionalsStr +","+jo2.getString("AdditionalName");
                                                    additionalsStrAr = additionalsStrAr +","+jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = additionalsPrice + ","+jo2.getString("AdditionalPrice");
                                                }
                                                additionPrice = additionPrice + Float.parseFloat(jo2.getString("AdditionalPrice"));
                                            }

                                        }

                                        priceAd = Float.parseFloat(price) + additionPrice;
                                    }
                                }




                                finalPrice = priceAd * quantity;


                                values.put("mainCategoryId", categoryId);
                                values.put("subCategoryId", categoryId);
                                values.put("itemId", itemId);
                                values.put("itemName", itemName);
                                values.put("itemImage", itemImage);
                                values.put("description", itemDesc);
                                values.put("itemTypeId", itemType);
                                values.put("itemPrice", price);
                                values.put("itemPriceAd", String.valueOf((priceAd)));
                                values.put("additionalId", ids);
                                values.put("additionalName", additionalsStr);
                                values.put("additionalPrice", additionalsPrice);
                                values.put("qty", Integer.toString(quantity));
                                values.put("totalAmount", String.valueOf(finalPrice));
                                values.put("size", itemType);
                                values.put("comment", "");
                                values.put("itemNameAr", itemNameAr);
                                values.put("descriptionAr", itemDescAr);
                                values.put("additionalNameAr", additionalsStrAr);
                                values.put("nonDelivery", nonDelivery);
                                myDbHelper.insertOrder(values);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            Intent intent = new Intent(OrderHistoryActivity.this, CheckoutActivity.class);
            startActivity(intent);

            super.onPostExecute(result);

        }

    }


    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderHistoryActivity.this);
            dialog = ProgressDialog.show(OrderHistoryActivity.this, "",
                    "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            transactionsList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderHistoryActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i<ja.length(); i++) {

                                    OrderHistory oh = new OrderHistory();
                                    JSONObject jo1 = ja.getJSONObject(i);
                                    String orderId = jo1.getString("odrId");

                                    String storeName = jo1.getString("Sname");
                                    String storeName_ar = jo1.getString("Sname_ar");
//                                    String storeId = jo1.getString("UserId");
                                    String orderDate = jo1.getString("Odate");
//                                    boolean isFavourite = jo1.getBoolean("IsFavorite");
                                    String total_Price = jo1.getString("TotPrice");
                                    String orderType = jo1.getString("OrderType");
                                    String orderStatus = jo1.getString("OrderStatus");
                                    String invoiceNo = jo1.getString("InvoiceNo");
                                    String itemDetails = "", itemDetailsAr = "";

                                    JSONArray ja1 = jo1.getJSONArray("items");
                                    for(int j=0;j<ja1.length();j++){
                                        JSONObject jo2 = ja1.getJSONObject(j);
                                        if(itemDetails.equalsIgnoreCase("") && itemDetailsAr.equalsIgnoreCase("")){
                                            itemDetails = jo2.getString("ItmName");
                                            itemDetailsAr = jo2.getString("ItmName_ar");
                                        }else{
                                            itemDetails = itemDetails + ", "+jo2.getString("ItmName");
                                            itemDetailsAr = itemDetailsAr+ ", "+jo2.getString("ItmName_ar");
                                        }
                                    }


                                    oh.setOrderId(orderId);
//                                    oh.setStoreId(storeId);
                                    oh.setOrderDate(orderDate);
                                    oh.setOrderId(orderId);

                                    oh.setStoreName(storeName);
                                    oh.setStoreName_ar(storeName_ar);
//                                    oh.setIsFavorite(isFavourite);
                                    oh.setTotalPrice(total_Price);
//                                    oh.setStatus(status);
                                    oh.setOrderStatus(orderStatus);
                                    oh.setInvoiceNo(invoiceNo);
                                    oh.setItemDetails(itemDetails);
                                    oh.setItemDetails_ar(itemDetailsAr);
                                    oh.setOrderType(orderType);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                    transactionsList.add(oh);

                                }
                            }catch (JSONException je){
                                je.printStackTrace();
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Oregano");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }
}
