package com.cs.oregano.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 19-06-2016.
 */
public class SaveAddressActivity extends AppCompatActivity {

    RelativeLayout addressHome,addressWork, addressOther;
    TextView saveAddress, homeTxt, workTxt, otherTxt;
    EditText flatNo, landmark, otherAddress, addressSelected;

    String addressType, response;
    ImageView homeRight, workRight, otherRight;

    String address, latitude, longitude;

    SharedPreferences userPrefs;
    String userId;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.save_address);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.save_address_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        address = getIntent().getExtras().getString("address");
        latitude = getIntent().getExtras().getString("latitude");
        longitude = getIntent().getExtras().getString("longitude");
        saveAddress = (TextView) findViewById(R.id.save_address);
        flatNo = (EditText) findViewById(R.id.flat_no);
        landmark = (EditText) findViewById(R.id.landmark);
        otherAddress = (EditText) findViewById(R.id.address_extra);
        addressSelected = (EditText) findViewById(R.id.address);

        addressHome = (RelativeLayout) findViewById(R.id.address_home);
        addressWork = (RelativeLayout) findViewById(R.id.address_work);
        addressOther = (RelativeLayout) findViewById(R.id.other_address);

        homeRight = (ImageView) findViewById(R.id.home_right);
        workRight = (ImageView) findViewById(R.id.work_right);
        otherRight = (ImageView) findViewById(R.id.other_right);

        homeTxt = (TextView) findViewById(R.id.home_txt);
        workTxt = (TextView) findViewById(R.id.work_txt);
        otherTxt = (TextView) findViewById(R.id.other_txt);

        addressSelected.setText(address);

        addressHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "1";
                homeRight.setVisibility(View.VISIBLE);
                workRight.setVisibility(View.GONE);
                otherRight.setVisibility(View.GONE);
                otherAddress.setVisibility(View.VISIBLE);
                if(language.equalsIgnoreCase("En")){
                    otherAddress.setHint("Eg. My Home");
                }else if(language.equalsIgnoreCase("Ar")){
                    otherAddress.setHint("منزلي");
                }


            }
        });

        addressWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "2";
                homeRight.setVisibility(View.GONE);
                workRight.setVisibility(View.VISIBLE);
                otherRight.setVisibility(View.GONE);
                otherAddress.setVisibility(View.VISIBLE);
                if(language.equalsIgnoreCase("En")){
                    otherAddress.setHint("Eg. My Office");
                }else if(language.equalsIgnoreCase("Ar")){
                    otherAddress.setHint("مكتبي");
                }
            }
        });

        addressOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "3";
                homeRight.setVisibility(View.GONE);
                workRight.setVisibility(View.GONE);
                otherRight.setVisibility(View.VISIBLE);
                otherAddress.setVisibility(View.VISIBLE);
                if(language.equalsIgnoreCase("En")){
                    otherAddress.setHint("Eg. Friend Home");
                }else if(language.equalsIgnoreCase("Ar")){
                    otherAddress.setHint("منزل صديق");
                }
            }
        });

        saveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String flatNumber = flatNo.getText().toString().replace(" ","%20");
                String landMark = landmark.getText().toString().replace(" ","%20");
                String address = addressSelected.getText().toString().replace(" ","%20");
                String addressOther = otherAddress.getText().toString().replace(" ","%20");
                if(addressOther.length() == 0){
                    otherAddress.setError("");
                }else if(addressType == null){
                    if(language.equalsIgnoreCase("En")){
                        Toast.makeText(SaveAddressActivity.this, "Please select address type", Toast.LENGTH_SHORT).show();
                    }else if(language.equalsIgnoreCase("Ar")){
                        Toast.makeText(SaveAddressActivity.this, "من فضلك اختر نوع العنوان", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    new SaveAddressDetails().execute(Constants.SAVE_ADDRESS_URL+userId+"?UsrAddress="+address+"&HouseNo="+flatNumber+"&LandMark="+landMark+"&AddressType="+addressType+"&Latitude="+latitude+"&Longitude="+longitude+"&HouseName="+addressOther);
                }


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public class SaveAddressDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(SaveAddressActivity.this);
            dialog = ProgressDialog.show(SaveAddressActivity.this, "",
                    "Saving address...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(SaveAddressActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(SaveAddressActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try{
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            if(s.equals("1")){
                                setResult(RESULT_OK);
                                finish();
                            }
                        }catch (JSONException je){
                            setResult(RESULT_CANCELED);
                            finish();
                        }

                    }
                }

            }else {
                Toast.makeText(SaveAddressActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
}
