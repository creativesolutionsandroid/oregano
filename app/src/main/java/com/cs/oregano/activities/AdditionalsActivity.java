package com.cs.oregano.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.oregano.Constants;
import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.adapters.AdditionalsListAdapter;
import com.cs.oregano.fragments.OrderFragment;
import com.cs.oregano.model.AdditionalPrices;
import com.cs.oregano.model.Additionals;
import com.cs.oregano.model.ItemPrices;
import com.cs.oregano.model.Modifiers;
import com.cs.oregano.model.SelectedAddtionals;
import com.cs.oregano.widgets.NetworkUtil;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by CS on 09-06-2016.
 */
public class AdditionalsActivity extends AppCompatActivity implements View.OnClickListener{

    RelativeLayout exLargeLayout, largeLayout, mediumLayout, smallLayout, pcs4Layout, pcs8Layout, oneItemLayout, addItemLayout;
    LinearLayout type4Layout, type2Layout, singleLayout, itemDetails, additionalsLayout;
    LinearLayout crustLayout, regularLayout, wheatLayout, glutenLayout;
    LinearLayout pennyLayout, regularPennyLayout, glutenPennyLayout;
    private ExpandableListView mExpListView;
    AdditionalsListAdapter mAdapter;
    ImageView item_image, writeComment;
    ImageView exlarge, large, medium, small, pcs4, pcs8, single, plusBtn, minusBtn;
    TextView itemName1, itemDesc1, exLargePrice, largePrice, mediumPrice, smallPrice, pcs8Price, pcs4Price, singlePrice, itemTitle, itemDescription;
    public static TextView itemQty, itemQuantity, itemPrice;
    Toolbar toolbar;

    AlertDialog alertDialog;

    LinearLayout.LayoutParams crustParams , crustParams1;

    private ArrayList<ItemPrices> pricesList = new ArrayList<>();
    private ArrayList<Modifiers> modifiersList = new ArrayList<>();
    private ArrayList<Modifiers> crustList = new ArrayList<>();
//    private ArrayList<Modifiers> pennyList = new ArrayList<>();

    String crustId = "", crustName, crustNameAr;
    float crustPrice;
    boolean isCrustSelected;
    boolean isPennyRequired = false;

    String response;
    String itemId, categoryId, itemName, itemNameAr, itemDesc, itemDescAr, itemImage, isDelivery;
    public static String itemType;
    String size;
    private int lastExpandedPosition = 0;
    String price;
    public static float priceAd;
    public static float finalPrice;
    DecimalFormat dec =new DecimalFormat("0.00");
    public static int quantity;
    private DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.additionals_layout);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.additionals_layout_arabic);
        }

        myDbHelper = new DataBaseHelper(AdditionalsActivity.this);
        Constants.COMMENTS = "";

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        itemId = getIntent().getStringExtra("item_id");
        categoryId = getIntent().getStringExtra("category_id");
        itemName = getIntent().getStringExtra("item_name");
        itemDesc = getIntent().getStringExtra("item_desc");
        itemImage = getIntent().getStringExtra("item_image");
        itemNameAr = getIntent().getStringExtra("item_nameAr");
        itemDescAr = getIntent().getStringExtra("item_descAr");
        isDelivery = getIntent().getStringExtra("delivery");
        itemDetails = (LinearLayout) findViewById(R.id.item_details);
        itemName1 = (TextView) findViewById(R.id.item_name);
        itemDesc1 = (TextView) findViewById(R.id.item_description);
        item_image = (ImageView) findViewById(R.id.item_image);
        if(language.equalsIgnoreCase("En")){
            itemName1.setText(itemName+":");
            itemDesc1.setText(WordUtils.capitalizeFully(itemDesc));
        }else if(language.equalsIgnoreCase("Ar")){
            itemName1.setText(itemNameAr+":");
            itemDesc1.setText(itemDescAr);
        }

        Glide.with(AdditionalsActivity.this).load("http://www.ircfood.com/images/"+itemImage).into(item_image);
        type4Layout = (LinearLayout) findViewById(R.id.four_types);
        type2Layout = (LinearLayout) findViewById(R.id.two_types48);
        singleLayout = (LinearLayout) findViewById(R.id.single_layout);
        exLargeLayout = (RelativeLayout) findViewById(R.id.ex_large_layout);
        largeLayout = (RelativeLayout) findViewById(R.id.large_layout);
        mediumLayout = (RelativeLayout) findViewById(R.id.medium_layout);
        smallLayout = (RelativeLayout) findViewById(R.id.small_layout);
        pcs8Layout = (RelativeLayout) findViewById(R.id.pcs8_layout);
        pcs4Layout = (RelativeLayout) findViewById(R.id.pcs4_layout);
        oneItemLayout = (RelativeLayout) findViewById(R.id.one_item);
        addItemLayout = (RelativeLayout) findViewById(R.id.add_layout);
        crustLayout = (LinearLayout) findViewById(R.id.crust_layout);
        pennyLayout = (LinearLayout) findViewById(R.id.penny_layout);
        regularLayout = (LinearLayout) findViewById(R.id.regular_layout);
        writeComment = (ImageView) findViewById(R.id.write_comment);
        additionalsLayout = (LinearLayout) findViewById(R.id.additionals_layout);

        crustLayout.setVisibility(View.GONE);
        pennyLayout.setVisibility(View.GONE);

        wheatLayout = (LinearLayout) findViewById(R.id.wheat_layout);
        glutenLayout = (LinearLayout) findViewById(R.id.gluten_layout);

        regularPennyLayout = (LinearLayout) findViewById(R.id.penny_regular_layout);
        glutenPennyLayout = (LinearLayout) findViewById(R.id.penny_gluten_layout);

        exlarge = (ImageView) findViewById(R.id.ex_large);
        large = (ImageView) findViewById(R.id.large);
        medium = (ImageView) findViewById(R.id.medium);
        small = (ImageView) findViewById(R.id.small);
        pcs8 = (ImageView) findViewById(R.id.pcs8);
        pcs4 = (ImageView) findViewById(R.id.pcs4);
        single = (ImageView) findViewById(R.id.single);
        plusBtn = (ImageView) findViewById(R.id.plus_btn);
        minusBtn = (ImageView) findViewById(R.id.minus_btn);


        exLargePrice = (TextView) findViewById(R.id.ex_large_price);
        largePrice = (TextView) findViewById(R.id.large_price);
        mediumPrice = (TextView) findViewById(R.id.medium_price);
        smallPrice = (TextView) findViewById(R.id.small_price);
        pcs8Price = (TextView) findViewById(R.id.pcs8_price);
        pcs4Price = (TextView) findViewById(R.id.pcs4_price);
        singlePrice = (TextView) findViewById(R.id.single_price);
        itemTitle = (TextView) findViewById(R.id.item_title);
        itemDescription = (TextView) findViewById(R.id.item_desc);

        itemQty = (TextView) findViewById(R.id.qty);
        itemQuantity = (TextView) findViewById(R.id.item_qty);
        itemPrice = (TextView) findViewById(R.id.item_price);

        mExpListView = (ExpandableListView) findViewById(R.id.expandableListView);

        priceAd = 0;
        finalPrice = 0;
        quantity = 0;
//        itemType = "1";

        pricesList = (ArrayList<ItemPrices>) getIntent().getSerializableExtra(
                "item_prices");

        Collections.sort(pricesList, ItemPrices.priceSort);

        additionalsLayout.setVisibility(View.INVISIBLE);

        if(pricesList.size() == 4){
            type4Layout.setVisibility(View.VISIBLE);
            type2Layout.setVisibility(View.GONE);
            singleLayout.setVisibility(View.GONE);

            DecimalFormat dec = new DecimalFormat("0.00");
            exLargePrice.setText(dec.format(Float.parseFloat(Constants.convertToArabic(pricesList.get(3).getPrice()))));
            largePrice.setText(dec.format(Float.parseFloat(Constants.convertToArabic(pricesList.get(2).getPrice()))));
            mediumPrice.setText(dec.format(Float.parseFloat(Constants.convertToArabic(pricesList.get(1).getPrice()))));
            smallPrice.setText(dec.format(Float.parseFloat(Constants.convertToArabic(pricesList.get(0).getPrice()))));
        }else if(pricesList.size() == 2){
            if(pricesList.get(0).getItemTypeId() == 5){

                DecimalFormat dec = new DecimalFormat("0.00");
                type2Layout.setVisibility(View.VISIBLE);
                type4Layout.setVisibility(View.GONE);
                singleLayout.setVisibility(View.GONE);
                pcs8Price.setText(dec.format(Float.parseFloat(pricesList.get(1).getPrice())));
                pcs4Price.setText(dec.format(Float.parseFloat(pricesList.get(0).getPrice())));
            }else if(pricesList.get(0).getItemTypeId() == 3){
                DecimalFormat dec = new DecimalFormat("0.00");
                type4Layout.setVisibility(View.VISIBLE);
                type2Layout.setVisibility(View.GONE);
                singleLayout.setVisibility(View.GONE);
                mediumLayout.setVisibility(View.GONE);
                smallLayout.setVisibility(View.GONE);
                exLargePrice.setText(dec.format(Float.parseFloat(Constants.convertToArabic(pricesList.get(1).getPrice()))));
                largePrice.setText(dec.format(Float.parseFloat(Constants.convertToArabic(pricesList.get(0).getPrice()))));

            }else{
                DecimalFormat dec = new DecimalFormat("0.00");
                type4Layout.setVisibility(View.VISIBLE);
                type2Layout.setVisibility(View.GONE);
                singleLayout.setVisibility(View.GONE);
                exLargeLayout.setVisibility(View.GONE);
                mediumLayout.setVisibility(View.GONE);
                largePrice.setText(dec.format(Float.parseFloat(Constants.convertToArabic(pricesList.get(1).getPrice()))));
                smallPrice.setText(dec.format(Float.parseFloat(Constants.convertToArabic(pricesList.get(0).getPrice()))));
            }
        }else{
            if(!getIntent().getStringExtra("additional_id").equalsIgnoreCase("0")){
                additionalsLayout.setVisibility(View.VISIBLE);
            }else{
                additionalsLayout.setVisibility(View.INVISIBLE);
            }

            itemDetails.setVisibility(View.GONE);
            if(language.equalsIgnoreCase("En")){
                itemTitle.setText(itemName+":");
                itemDescription.setText(WordUtils.capitalizeFully(itemDesc));
            }else if(language.equalsIgnoreCase("Ar")){
                itemTitle.setText(itemNameAr+":");
                itemDescription.setText(itemDescAr);
            }
            DecimalFormat dec = new DecimalFormat("0.00");
            singlePrice.setText(dec.format(Float.parseFloat(Constants.convertToArabic(pricesList.get(0).getPrice()))));
            itemType = Integer.toString(pricesList.get(0).getItemTypeId());
            size = "0";
//            mAdapter.notifyDataSetChanged();
//            additionalsLayout.setVisibility(View.VISIBLE);

            if(quantity == 0){
                priceAd = Float.parseFloat(pricesList.get(0).getPrice());
                price = pricesList.get(0).getPrice();
                finalPrice = priceAd;
                quantity = 1;
                itemQuantity.setText("1");
                itemQty.setText("1");
                itemPrice.setText(dec.format(Float.parseFloat(pricesList.get(0).getPrice())));
            }else{
                float additionalPrice = 0;

                for(SelectedAddtionals sa: AdditionalsListAdapter.additionalsList){
                    for(int i = 0; i< sa.getAdditionalPriceList().size();i++) {
                        if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                            additionalPrice = additionalPrice + Float.parseFloat(Constants.convertToArabic(sa.getAdditionalPriceList().get(i).getPrice()));
                        }
                    }
                }


                priceAd = (Float.parseFloat(pricesList.get(0).getPrice()) + additionalPrice);
                price = pricesList.get(0).getPrice();
                finalPrice = priceAd * quantity;
                itemPrice.setText(""+dec.format(finalPrice));
            }
        }

        mAdapter = new AdditionalsListAdapter(AdditionalsActivity.this, modifiersList, categoryId, language);
        mExpListView.setAdapter(mAdapter);
        exLargeLayout.setOnClickListener(this);
        largeLayout.setOnClickListener(this);
        mediumLayout.setOnClickListener(this);
        smallLayout.setOnClickListener(this);
        pcs8Layout.setOnClickListener(this);
        pcs4Layout.setOnClickListener(this);
//        oneItemLayout.setOnClickListener(this);
        plusBtn.setOnClickListener(this);
        minusBtn.setOnClickListener(this);
        addItemLayout.setOnClickListener(this);
        regularLayout.setOnClickListener(this);
        wheatLayout.setOnClickListener(this);
        glutenLayout.setOnClickListener(this);
        writeComment.setOnClickListener(this);
        regularPennyLayout.setOnClickListener(this);
        glutenPennyLayout.setOnClickListener(this);


        item_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AdditionalsActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.bigimage_popup;
//                if(language.equalsIgnoreCase("En")){
//                    layout = R.layout.bigimage_popup;
//                }else if(language.equalsIgnoreCase("Ar")){
//                    layout = R.layout.bigimage_popup;
//                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                ImageView img = (ImageView) dialogView.findViewById(R.id.big_image);
                TextView close = (TextView) dialogView.findViewById(R.id.image_close);

                Glide.with(AdditionalsActivity.this).load("http://www.ircfood.com/images/"+itemImage).into(img);

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });


        if(!getIntent().getStringExtra("additional_id").equalsIgnoreCase("0")){
            new GetCategoryItems().execute(Constants.ADDITIONALS_URL+getIntent().getStringExtra("additional_id"));
        }else{
            additionalsLayout.setVisibility(View.INVISIBLE);
        }


        mExpListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    mExpListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });


        crustParams = new LinearLayout.LayoutParams(
                0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
        crustParams1 = new LinearLayout.LayoutParams(
                0, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        float additionalPrice = 0;
        switch (v.getId()) {

            case R.id.ex_large_layout:
                Log.i("TAG", "Clicked");
                itemType = "4";
                size = "4";
                if (categoryId.equalsIgnoreCase("7") || categoryId.equalsIgnoreCase("8") || categoryId.equalsIgnoreCase("4")) {
                    crustLayout.setVisibility(View.VISIBLE);
                    glutenLayout.setVisibility(View.GONE);
                    regularLayout.setLayoutParams(crustParams);
                    wheatLayout.setLayoutParams(crustParams);
                    if (!crustId.equalsIgnoreCase("1") && !crustId.equalsIgnoreCase("2")) {
                        isCrustSelected = false;
                    }
                    if(crustId.equals("3")){
                        for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                            if(crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("3")){
                                for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                                    if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                        crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                                    }
                                }
                            }
                        }
                        additionalPrice = crustPrice;
                    }
                }
                additionalsLayout.setVisibility(View.VISIBLE);
                if(language.equalsIgnoreCase("En")) {
                    exlarge.setImageResource(R.drawable.ex_largebig_select);
                    large.setImageResource(R.drawable.large_normal);
                    medium.setImageResource(R.drawable.medium_normal);
                    small.setImageResource(R.drawable.small_normal);
                }else if(language.equalsIgnoreCase("Ar")){
                    exlarge.setImageResource(R.drawable.ex_largebig_select_arabic);
                    large.setImageResource(R.drawable.large_normal_arabic);
                    medium.setImageResource(R.drawable.medium_normal_arabic);
                    small.setImageResource(R.drawable.small_normal_arabic);
                }

                if (pricesList.size() == 4) {


                    if (quantity == 0) {
                        priceAd = Float.parseFloat(pricesList.get(3).getPrice());
                        price = pricesList.get(3).getPrice();
                        finalPrice = priceAd;
                        quantity = 1;
                        itemQuantity.setText("1");
                        itemQty.setText("1");
                        DecimalFormat dec =new DecimalFormat("0.00");
                        itemPrice.setText(dec.format(Float.parseFloat(pricesList.get(3).getPrice())) );
                    } else {

                        for (SelectedAddtionals sa : AdditionalsListAdapter.additionalsList) {
                            for (int i = 0; i < sa.getAdditionalPriceList().size(); i++) {
                                if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                                    additionalPrice = additionalPrice + Float.parseFloat(Constants.convertToArabic(sa.getAdditionalPriceList().get(i).getPrice()));
                                }
                            }
                        }


                        priceAd = (Float.parseFloat(pricesList.get(3).getPrice()) + additionalPrice);
                        price = pricesList.get(3).getPrice();
                        finalPrice = priceAd * quantity;
                        DecimalFormat dec =new DecimalFormat("0.00");
                        itemPrice.setText("" +dec.format( finalPrice) );
                    }
                }else if(pricesList.size() == 2){
                    if(pricesList.get(0).getItemTypeId() == 3){
                        if (quantity == 0) {
                            priceAd = Float.parseFloat(pricesList.get(1).getPrice());
                            price = pricesList.get(1).getPrice();
                            finalPrice = priceAd;
                            quantity = 1;
                            itemQuantity.setText("1");
                            itemQty.setText("1");
                            DecimalFormat dec =new DecimalFormat("0.00");
                            itemPrice.setText(dec.format(Float.parseFloat(pricesList.get(1).getPrice())) );
                        } else {
                            for (SelectedAddtionals sa : AdditionalsListAdapter.additionalsList) {
                                for (int i = 0; i < sa.getAdditionalPriceList().size(); i++) {
                                    if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                                        additionalPrice = additionalPrice + Float.parseFloat(Constants.convertToArabic(sa.getAdditionalPriceList().get(i).getPrice()));
                                    }
                                }
                            }


                            priceAd = (Float.parseFloat(pricesList.get(1).getPrice()) + additionalPrice);
                            price = pricesList.get(1).getPrice();
                            finalPrice = priceAd * quantity;
                            DecimalFormat dec =new DecimalFormat("0.00");
                            itemPrice.setText("" + dec.format(Float.parseFloat(String.valueOf(finalPrice))) );
                        }
                    }
                }

//                AdditionalsListAdapter.isAdded = false;
//                AdditionalsListAdapter.additionalsPrice = 0;
//                AdditionalsListAdapter.modifierIdList.clear();
//                AdditionalsListAdapter.dummyModifierIdList.clear();
//                AdditionalsListAdapter.additionalsList.clear();
//                AdditionalsListAdapter.additionalsIdList.clear();
                mAdapter.notifyDataSetChanged();
                break;
            case R.id.large_layout:
                additionalPrice = 0;
                itemType = "3";
                size = "3";
                if(language.equalsIgnoreCase("En")) {
                    exlarge.setImageResource(R.drawable.ex_largebig_normal);
                    large.setImageResource(R.drawable.large_select);
                    medium.setImageResource(R.drawable.medium_normal);
                    small.setImageResource(R.drawable.small_normal);
                }else if(language.equalsIgnoreCase("Ar")){
                    exlarge.setImageResource(R.drawable.ex_largebig_normal_arabic);
                    large.setImageResource(R.drawable.large_select_arabic);
                    medium.setImageResource(R.drawable.medium_normal_arabic);
                    small.setImageResource(R.drawable.small_normal_arabic);
                }

                mAdapter.notifyDataSetChanged();
                if(categoryId.equalsIgnoreCase("7")|| categoryId.equalsIgnoreCase("8") || categoryId.equalsIgnoreCase("4")){
                    crustLayout.setVisibility(View.VISIBLE);
                    glutenLayout.setVisibility(View.GONE);
                    regularLayout.setLayoutParams(crustParams);
                    wheatLayout.setLayoutParams(crustParams);
                    if(!crustId.equalsIgnoreCase("1") && !crustId.equalsIgnoreCase("2")){
                        isCrustSelected = false;
                    }

                    if(crustId.equals("3")){
                        for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                            if(crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("3")){
                                for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                                    if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                        crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                                    }
                                }
                            }
                        }
                        additionalPrice = crustPrice;
                    }
                }
                additionalsLayout.setVisibility(View.VISIBLE);

                if(pricesList.size() == 4){
                    if(quantity == 0){
                        priceAd = Float.parseFloat(pricesList.get(2).getPrice());
                        price = pricesList.get(2).getPrice();
                    finalPrice = priceAd;
                        quantity = 1;
                        itemQuantity.setText("1");
                        itemQty.setText("1");
                        DecimalFormat dec =new DecimalFormat("0.00");
                        itemPrice.setText(dec.format(Float.parseFloat(pricesList.get(2).getPrice())));
                    }else {

                        for(SelectedAddtionals sa: AdditionalsListAdapter.additionalsList){
                            for(int i = 0; i< sa.getAdditionalPriceList().size();i++) {
                                if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                                    additionalPrice = additionalPrice + Float.parseFloat(Constants.convertToArabic(sa.getAdditionalPriceList().get(i).getPrice()));
                                }
                            }
                        }


                        priceAd = (Float.parseFloat(pricesList.get(2).getPrice()) + additionalPrice);
                        price = pricesList.get(2).getPrice();
                        finalPrice = priceAd * quantity;
                        DecimalFormat dec =new DecimalFormat("0.00");
                        itemPrice.setText(""+dec.format(finalPrice));

//                        priceAd = Float.parseFloat(pricesList.get(1).getPrice());
//                        price = pricesList.get(1).getPrice();
//                        finalPrice = priceAd;
//                        itemPrice.setText(pricesList.get(1).getPrice());
                    }
                }else if(pricesList.size() == 2){
                    if(pricesList.get(0).getItemTypeId() == 5){

                    }else if(pricesList.get(0).getItemTypeId() == 3){
                        if(quantity == 0){
                            priceAd = Float.parseFloat(pricesList.get(0).getPrice());
                            price = pricesList.get(0).getPrice();
                        finalPrice = priceAd;
                            quantity = 1;
                            itemQuantity.setText("1");
                            itemQty.setText("1");
                            DecimalFormat dec =new DecimalFormat("0.00");
                            itemPrice.setText(dec.format(Float.parseFloat(pricesList.get(0).getPrice())));
                        }else {
                            for(SelectedAddtionals sa: AdditionalsListAdapter.additionalsList){
                                for(int i = 0; i< sa.getAdditionalPriceList().size();i++) {
                                    if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                                        additionalPrice = additionalPrice + Float.parseFloat(Constants.convertToArabic(sa.getAdditionalPriceList().get(i).getPrice()));
                                    }
                                }
                            }


                            priceAd = (Float.parseFloat(pricesList.get(0).getPrice()) + additionalPrice);
                            price = pricesList.get(0).getPrice();
                            finalPrice = priceAd * quantity;
                            DecimalFormat dec =new DecimalFormat("0.00");
                            itemPrice.setText(""+dec.format(finalPrice));
//                            priceAd = Float.parseFloat(pricesList.get(1).getPrice());
//                            price = pricesList.get(1).getPrice();
//                            finalPrice = priceAd;
//                            itemPrice.setText(pricesList.get(1).getPrice());
                        }
                    }else{
                        if(quantity == 0){
                            priceAd = Float.parseFloat(pricesList.get(1).getPrice());
                            price = pricesList.get(1).getPrice();
                        finalPrice = priceAd;
                            quantity = 1;
                            itemQuantity.setText("1");
                            itemQty.setText("1");
                            DecimalFormat dec =new DecimalFormat("0.00");
                            itemPrice.setText(dec.format(Float.parseFloat(pricesList.get(1).getPrice())));
                        }else {

                            for(SelectedAddtionals sa: AdditionalsListAdapter.additionalsList){
                                for(int i = 0; i< sa.getAdditionalPriceList().size();i++) {
                                    if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                                        additionalPrice = additionalPrice + Float.parseFloat(Constants.convertToArabic(sa.getAdditionalPriceList().get(i).getPrice()));
                                    }
                                }
                            }


                            priceAd = (Float.parseFloat(pricesList.get(1).getPrice()) + additionalPrice);
                            price = pricesList.get(1).getPrice();
                            finalPrice = priceAd * quantity;
                            DecimalFormat dec =new DecimalFormat("0.00");
                            itemPrice.setText(""+dec.format(finalPrice));
//                            priceAd = Float.parseFloat(pricesList.get(0).getPrice());
//                            price = pricesList.get(0).getPrice();
//                            finalPrice = priceAd;
//                            itemPrice.setText(pricesList.get(0).getPrice());
                        }
                    }
                }
//                AdditionalsListAdapter.isAdded = false;
//                AdditionalsListAdapter.additionalsPrice = 0;
//                AdditionalsListAdapter.modifierIdList.clear();
//                AdditionalsListAdapter.dummyModifierIdList.clear();
//                AdditionalsListAdapter.additionalsList.clear();
//                AdditionalsListAdapter.additionalsIdList.clear();
                mAdapter.notifyDataSetChanged();
                break;
            case R.id.medium_layout:
                additionalPrice = 0;
                itemType = "2";
                size = "2";
                if(language.equalsIgnoreCase("En")) {
                    exlarge.setImageResource(R.drawable.ex_largebig_normal);
                    large.setImageResource(R.drawable.large_normal);
                    medium.setImageResource(R.drawable.medium_select);
                    small.setImageResource(R.drawable.small_normal);
                }else if(language.equalsIgnoreCase("Ar")){
                    exlarge.setImageResource(R.drawable.ex_largebig_normal_arabic);
                    large.setImageResource(R.drawable.large_normal_arabic);
                    medium.setImageResource(R.drawable.medium_select_arabic);
                    small.setImageResource(R.drawable.small_normal_arabic);
                }

                mAdapter.notifyDataSetChanged();
                if(categoryId.equalsIgnoreCase("7")|| categoryId.equalsIgnoreCase("8") || categoryId.equalsIgnoreCase("4")){
                    crustLayout.setVisibility(View.VISIBLE);
                    glutenLayout.setVisibility(View.VISIBLE);

                    regularLayout.setLayoutParams(crustParams1);
                    wheatLayout.setLayoutParams(crustParams1);
                    glutenLayout.setLayoutParams(crustParams);
                    if(crustId.equalsIgnoreCase("1")|| crustId.equalsIgnoreCase("2") ||crustId.equalsIgnoreCase("3")){
                        isCrustSelected = true;
                    }
                    if(crustId.equals("3")){
                        for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                            if(crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("3")){
                                for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                                    if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                        crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                                    }
                                }
                            }
                        }
                        additionalPrice = crustPrice;
                    }
                }
                additionalsLayout.setVisibility(View.VISIBLE);

                if(pricesList.size() == 4) {
                    if(quantity == 0){
                    priceAd = Float.parseFloat(pricesList.get(1).getPrice());
                    price = pricesList.get(1).getPrice();
                    finalPrice = priceAd;
                    quantity = 1;
                    itemQuantity.setText("1");
                    itemQty.setText("1");
                    DecimalFormat dec =new DecimalFormat("0.00");
                    itemPrice.setText(dec.format(Float.parseFloat(pricesList.get(1).getPrice())));
                    }else{


                        for(SelectedAddtionals sa: AdditionalsListAdapter.additionalsList){
                            for(int i = 0; i< sa.getAdditionalPriceList().size();i++) {
                                if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                                    additionalPrice = additionalPrice + Float.parseFloat(Constants.convertToArabic(sa.getAdditionalPriceList().get(i).getPrice()));
                                }
                            }
                        }


                        priceAd = (Float.parseFloat(pricesList.get(1).getPrice()) + additionalPrice);
                        price = pricesList.get(1).getPrice();
                        finalPrice = priceAd * quantity;
                        DecimalFormat dec =new DecimalFormat("0.00");
                        itemPrice.setText(""+dec.format(finalPrice));
//                        price = Float.parseFloat(pricesList.get(2).getPrice()) + AdditionalsListAdapter.additionalsPrice;
//                        finalPrice = price * quantity;
//                        itemPrice.setText(""+finalPrice);
                    }
                }
//                AdditionalsListAdapter.additionalsPrice = 0;
//                AdditionalsListAdapter.modifierIdList.clear();
//                AdditionalsListAdapter.dummyModifierIdList.clear();
//                AdditionalsListAdapter.additionalsList.clear();
//                AdditionalsListAdapter.additionalsIdList.clear();
                mAdapter.notifyDataSetChanged();
                break;
            case R.id.small_layout:
                additionalPrice = 0;
                itemType = "1";
                size = "1";
                if(language.equalsIgnoreCase("En")) {
                    exlarge.setImageResource(R.drawable.ex_largebig_normal);
                    large.setImageResource(R.drawable.large_normal);
                    medium.setImageResource(R.drawable.medium_normal);
                    small.setImageResource(R.drawable.small_select);
                }else if(language.equalsIgnoreCase("Ar")){
                    exlarge.setImageResource(R.drawable.ex_largebig_normal_arabic);
                    large.setImageResource(R.drawable.large_normal_arabic);
                    medium.setImageResource(R.drawable.medium_normal_arabic);
                    small.setImageResource(R.drawable.small_select_arabic);
                }

                mAdapter.notifyDataSetChanged();
                if(categoryId.equalsIgnoreCase("7")|| categoryId.equalsIgnoreCase("8") || categoryId.equalsIgnoreCase("4")) {
                    crustLayout.setVisibility(View.VISIBLE);
                    glutenLayout.setVisibility(View.VISIBLE);

                    regularLayout.setLayoutParams(crustParams1);
                    wheatLayout.setLayoutParams(crustParams1);
                    glutenLayout.setLayoutParams(crustParams);
                    if(crustId.equalsIgnoreCase("1")|| crustId.equalsIgnoreCase("2") ||crustId.equalsIgnoreCase("3")){
                        isCrustSelected = true;
                    }
                    if(crustId.equals("3")){
                        for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                            if(crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("3")){
                                for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                                    if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                        crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                                    }
                                }
                            }
                        }
                        additionalPrice = crustPrice;
                    }
                }
                additionalsLayout.setVisibility(View.VISIBLE);


//                if(pricesList.size() == 4){
                    if(quantity == 0){
                    priceAd = Float.parseFloat(pricesList.get(0).getPrice());
                    price = pricesList.get(0).getPrice();
                    finalPrice = priceAd;
                        quantity = 1;
                        itemQuantity.setText("1");
                        itemQty.setText("1");
                        DecimalFormat dec =new DecimalFormat("0.00");
                        itemPrice.setText(dec.format(Float.parseFloat(pricesList.get(0).getPrice())));
                    }else{


                        for(SelectedAddtionals sa: AdditionalsListAdapter.additionalsList){
                            for(int i = 0; i< sa.getAdditionalPriceList().size();i++) {
                                if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                                    additionalPrice = additionalPrice + Float.parseFloat(Constants.convertToArabic(sa.getAdditionalPriceList().get(i).getPrice()));
                                }
                            }
                        }


                        priceAd = (Float.parseFloat(pricesList.get(0).getPrice()) + additionalPrice);
                        price = pricesList.get(0).getPrice();
                        finalPrice = priceAd * quantity;
                        DecimalFormat dec =new DecimalFormat("0.00");
                        itemPrice.setText(""+dec.format(finalPrice));
//                        price = Float.parseFloat(pricesList.get(3).getPrice()) + AdditionalsListAdapter.additionalsPrice;
//                        finalPrice = price * quantity;
//                        itemPrice.setText(""+finalPrice);
                    }
//                }else if(pricesList.size() == 2){
//                    if(pricesList.get(0).getItemTypeId() == 5){
//                    }else if(pricesList.get(0).getItemTypeId() == 3){
//                    }else{
//                        if(quantity == 0){
//                        priceAd = Float.parseFloat(pricesList.get(1).getPrice());
//                        price = pricesList.get(1).getPrice();
//                        finalPrice = priceAd;
//                            quantity = 1;
//                            itemQuantity.setText("1");
//                            itemQty.setText("1");
//                            itemPrice.setText(pricesList.get(1).getPrice());
//                        }else{
//                            int additionalPrice = 0;
//
//                            for(SelectedAddtionals sa: AdditionalsListAdapter.additionalsList){
//                                for(int i = 0; i< sa.getAdditionalPriceList().size();i++) {
//                                    if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
//                                        additionalPrice = additionalPrice + Float.parseFloat(sa.getAdditionalPriceList().get(i).getPrice());
//                                    }
//                                }
//                            }
//
//
//                            priceAd = (Float.parseFloat(pricesList.get(1).getPrice()) + additionalPrice);
//                            price = pricesList.get(1).getPrice();
//                            finalPrice = priceAd * quantity;
//                            itemPrice.setText(""+finalPrice);
////                            price = Float.parseFloat(pricesList.get(1).getPrice()) + AdditionalsListAdapter.additionalsPrice;
////                            finalPrice = price * quantity;
////                            itemPrice.setText(""+finalPrice);
//                        }
//                    }
//                }
//                AdditionalsListAdapter.additionalsPrice = 0;
//                AdditionalsListAdapter.modifierIdList.clear();
//                AdditionalsListAdapter.dummyModifierIdList.clear();
//                AdditionalsListAdapter.additionalsList.clear();
//                AdditionalsListAdapter.additionalsIdList.clear();
                mAdapter.notifyDataSetChanged();

                break;
            case R.id.pcs8_layout:
                itemType = "6";
                size = "6";
                if(language.equalsIgnoreCase("En")) {
                    pcs8.setImageResource(R.drawable.pcs8_select);
                    pcs4.setImageResource(R.drawable.pcs4_normal);
                }else if(language.equalsIgnoreCase("Ar")){
                    pcs8.setImageResource(R.drawable.pcs8_select_arabic);
                    pcs4.setImageResource(R.drawable.pcs4_normal_arabic);
                }

                mAdapter.notifyDataSetChanged();
                additionalsLayout.setVisibility(View.VISIBLE);

                if(pricesList.size() == 2) {
                    if (pricesList.get(1).getItemTypeId() == 6) {
                        if(quantity == 0){
                        priceAd = Float.parseFloat(pricesList.get(1).getPrice());
                        price = pricesList.get(1).getPrice();
                        finalPrice = priceAd;
                            quantity = 1;
                            itemQuantity.setText("1");
                            itemQty.setText("1");
                            DecimalFormat dec =new DecimalFormat("0.00");
                            itemPrice.setText(dec.format(Float.parseFloat(pricesList.get(1).getPrice())));
                        }else {
                            additionalPrice = 0;

                            for(SelectedAddtionals sa: AdditionalsListAdapter.additionalsList){
                                for(int i = 0; i< sa.getAdditionalPriceList().size();i++) {
                                    if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                                        additionalPrice = additionalPrice + Float.parseFloat(Constants.convertToArabic(sa.getAdditionalPriceList().get(i).getPrice()));
                                    }
                                }
                            }


                            priceAd = (Float.parseFloat(pricesList.get(1).getPrice()) + additionalPrice);
                            price = pricesList.get(1).getPrice();
                            finalPrice = priceAd * quantity;
                            DecimalFormat dec =new DecimalFormat("0.00");
                            itemPrice.setText(""+dec.format(finalPrice));
//                            price = Float.parseFloat(pricesList.get(0).getPrice()) + AdditionalsListAdapter.additionalsPrice;
//                            finalPrice = price * quantity;
//                            itemPrice.setText(""+finalPrice);
                        }
                    }
                }
//                AdditionalsListAdapter.additionalsPrice = 0;
//                AdditionalsListAdapter.modifierIdList.clear();
//                AdditionalsListAdapter.dummyModifierIdList.clear();
//                AdditionalsListAdapter.additionalsList.clear();
//                AdditionalsListAdapter.additionalsIdList.clear();
                mAdapter.notifyDataSetChanged();
                break;
            case R.id.pcs4_layout:
                itemType = "5";
                size = "5";
                if(language.equalsIgnoreCase("En")) {
                    pcs8.setImageResource(R.drawable.pcs8_normal);
                    pcs4.setImageResource(R.drawable.pcs4_select);
                }else if(language.equalsIgnoreCase("Ar")){
                    pcs8.setImageResource(R.drawable.pcs8_normal_arabic);
                    pcs4.setImageResource(R.drawable.pcs4_select_arabic);
                }

//                AdditionalsListAdapter.additionalsPrice = 0;
//                AdditionalsListAdapter.modifierIdList.clear();
//                AdditionalsListAdapter.dummyModifierIdList.clear();
//                AdditionalsListAdapter.additionalsList.clear();
//                AdditionalsListAdapter.additionalsIdList.clear();
                mAdapter.notifyDataSetChanged();
                additionalsLayout.setVisibility(View.VISIBLE);
                if(pricesList.size() == 2) {
                    if (pricesList.get(0).getItemTypeId() == 5) {
                        if(quantity == 0){
                        priceAd = Float.parseFloat(pricesList.get(0).getPrice());
                        price = pricesList.get(0).getPrice();
                        finalPrice = priceAd;
                            quantity = 1;
                            itemQuantity.setText("1");
                            itemQty.setText("1");
                            DecimalFormat dec =new DecimalFormat("0.00");
                            itemPrice.setText(dec.format(Float.parseFloat(pricesList.get(0).getPrice())));
                        }else{
                            additionalPrice = 0;

                            for(SelectedAddtionals sa: AdditionalsListAdapter.additionalsList){
                                for(int i = 0; i< sa.getAdditionalPriceList().size();i++) {
                                    if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                                        additionalPrice = additionalPrice + Float.parseFloat(Constants.convertToArabic(sa.getAdditionalPriceList().get(i).getPrice()));
                                    }
                                }
                            }


                            priceAd = (Float.parseFloat(pricesList.get(0).getPrice()) + additionalPrice);
                            price = pricesList.get(0).getPrice();
                            finalPrice = priceAd * quantity;
                            DecimalFormat dec =new DecimalFormat("0.00");
                            itemPrice.setText(""+dec.format(finalPrice));
//                            price = Float.parseFloat(pricesList.get(1).getPrice()) + AdditionalsListAdapter.additionalsPrice;
//                            finalPrice = price * quantity;
//                            itemPrice.setText(""+finalPrice);
                        }
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
            case R.id.one_item:
                itemType = Integer.toString(pricesList.get(0).getItemTypeId());
                size = "0";
                mAdapter.notifyDataSetChanged();
                additionalsLayout.setVisibility(View.VISIBLE);

//                if(quantity == 0){
//                priceAd = Float.parseFloat(pricesList.get(0).getPrice());
//                price = pricesList.get(0).getPrice();
//                finalPrice = priceAd;
//                    quantity = 1;
//                    itemQuantity.setText("1");
//                    itemQty.setText("1");
//                    itemPrice.setText(pricesList.get(0).getPrice());
//                }else{
//                    int additionalPrice = 0;
//
//                    for(SelectedAddtionals sa: AdditionalsListAdapter.additionalsList){
//                        for(int i = 0; i< sa.getAdditionalPriceList().size();i++) {
//                            if (sa.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
//                                additionalPrice = additionalPrice + Float.parseFloat(sa.getAdditionalPriceList().get(i).getPrice());
//                            }
//                        }
//                    }
//
//
//                    priceAd = (Float.parseFloat(pricesList.get(0).getPrice()) + additionalPrice);
//                    price = pricesList.get(0).getPrice();
//                    finalPrice = priceAd * quantity;
//                    itemPrice.setText(""+finalPrice);
////                    price = Float.parseFloat(pricesList.get(0).getPrice()) + AdditionalsListAdapter.additionalsPrice;
////                    finalPrice = price * quantity;
////                    itemPrice.setText(""+finalPrice);
//                }
//                AdditionalsListAdapter.additionalsPrice = 0;
//                AdditionalsListAdapter.modifierIdList.clear();
//                AdditionalsListAdapter.dummyModifierIdList.clear();
//                AdditionalsListAdapter.additionalsList.clear();
//                AdditionalsListAdapter.additionalsIdList.clear();
                mAdapter.notifyDataSetChanged();
                break;


            case R.id.plus_btn:
                if(quantity>0) {
                    quantity = quantity + 1;
                    finalPrice = Float.parseFloat(Constants.convertToArabic((itemPrice.getText().toString().replace(" SR", ""))));
                    finalPrice = finalPrice + (priceAd + crustPrice);
                    DecimalFormat dec =new DecimalFormat("0.00");
                    itemPrice.setText("" + dec.format(finalPrice) );
                    itemQuantity.setText("" + quantity);
                    itemQty.setText("" + quantity);
                }else if(quantity == 0){
                    showDialog();
                }

                break;

            case R.id.minus_btn:
                if(quantity>1) {
                    quantity = quantity - 1;
                    finalPrice = Float.parseFloat(Constants.convertToArabic(itemPrice.getText().toString().replace(" SR", "")));
                    finalPrice = finalPrice - (priceAd + crustPrice);
                    DecimalFormat dec =new DecimalFormat("0.00");
                    itemPrice.setText("" + dec.format(finalPrice) );
                    itemQuantity.setText(""+quantity);
                    itemQty.setText("" + quantity);
                }else if(quantity == 0){
                    showDialog();
                }
                break;

            case R.id.regular_layout:
                regularLayout.setClickable(false);
                wheatLayout.setClickable(true);
                glutenLayout.setClickable(true);
                if(crustId.equals("3")){
                    for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                        if(crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("3")){
                            for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                                if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                    crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                                }
                            }
                        }
                    }
                    finalPrice = finalPrice - (crustPrice*quantity);
                    DecimalFormat dec =new DecimalFormat("0.00");
                    itemPrice.setText("" + dec.format(finalPrice) );
                }
                crustId = "1";
                crustName = "Regular";
                crustNameAr = "عادية";
                isCrustSelected= true;
                crustPrice = 0;
                regularLayout.setBackgroundColor(Color.parseColor("#FF4ED1B9"));
                wheatLayout.setBackgroundColor(Color.parseColor("#CCFFFFFF"));
                glutenLayout.setBackgroundColor(Color.parseColor("#CCFFFFFF"));
                break;

            case R.id.wheat_layout:
                regularLayout.setClickable(true);
                wheatLayout.setClickable(false);
                glutenLayout.setClickable(true);
                if(crustId.equals("3")){
                    for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                        if(crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("3")){
                            for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                                if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                    crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                                }
                            }
                        }
                    }
                    finalPrice = finalPrice - (crustPrice*quantity);
                    DecimalFormat dec =new DecimalFormat("0.00");
                    itemPrice.setText("" + dec.format(finalPrice) );
                }
                crustId = "2";
                crustName = "Wheat";
                crustNameAr = " بر";
                isCrustSelected= true;
                crustPrice = 0;
                regularLayout.setBackgroundColor(Color.parseColor("#CCFFFFFF"));
                wheatLayout.setBackgroundColor(Color.parseColor("#FF4ED1B9"));
                glutenLayout.setBackgroundColor(Color.parseColor("#CCFFFFFF"));
                break;

            case R.id.gluten_layout:
                regularLayout.setClickable(true);
                wheatLayout.setClickable(true);
                glutenLayout.setClickable(false);
                crustId = "3";
                crustName = "99% Gluten Free";
                crustNameAr = "99% خالي من الجلوتين";
                for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                    if(crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("3")){
                        for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                            if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                            }
                        }
                    }
                }
                finalPrice = finalPrice + (crustPrice*quantity);
                itemPrice.setText("" + dec.format(finalPrice) );
                isCrustSelected= true;
                regularLayout.setBackgroundColor(Color.parseColor("#CCFFFFFF"));
                wheatLayout.setBackgroundColor(Color.parseColor("#CCFFFFFF"));
                glutenLayout.setBackgroundColor(Color.parseColor("#FF4ED1B9"));
                break;

            case R.id.penny_regular_layout:
                regularPennyLayout.setClickable(false);
                glutenPennyLayout.setClickable(true);


                if(crustId.equals("96")){
                    for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                        if(crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("96")){
                            for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                                if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                    crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                                }
                            }
                        }
                    }
                    finalPrice = finalPrice - (crustPrice*quantity);
                    DecimalFormat dec1 =new DecimalFormat("0.00");
                    itemPrice.setText("" + dec1.format(finalPrice) );
                }
                for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                    if (crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("95")) {
                        for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                            if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                            }
                        }
                    }
                }

                crustId = "95";
                crustName = "Regular penne";
                crustNameAr = "بيني عادي";
                finalPrice = finalPrice + (crustPrice * quantity);
                DecimalFormat dec1 =new DecimalFormat("0.00");
                itemPrice.setText("" + dec1.format(finalPrice) );
                isCrustSelected = true;
                regularPennyLayout.setBackgroundColor(Color.parseColor("#FF4ED1B9"));
                glutenPennyLayout.setBackgroundColor(Color.parseColor("#CCFFFFFF"));

                break;

            case R.id.penny_gluten_layout:
                regularPennyLayout.setClickable(true);
                glutenPennyLayout.setClickable(false);


                if(crustId.equals("95")){
                    for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                        if(crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("95")){
                            for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                                if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                    crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                                }
                            }
                        }
                    }
                    finalPrice = finalPrice - (crustPrice*quantity);
                    itemPrice.setText("" + finalPrice );
                }

                for (int i = 0; i < crustList.get(0).getChildItems().size(); i++) {
                    if(crustList.get(0).getChildItems().get(i).getAdditionalsId().equalsIgnoreCase("96")){
                        for (int j = 0; j < crustList.get(0).getChildItems().get(0).getAdditionalPriceList().size(); j++) {
                            if (crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getItemType().equals(itemType)) {
                                crustPrice = Float.parseFloat(Constants.convertToArabic(crustList.get(0).getChildItems().get(i).getAdditionalPriceList().get(j).getPrice()));
                            }
                        }
                    }
                }

                crustId = "96";
                crustName = "99% Gluten Free penne";
                crustNameAr = "99% بيني خالي من الجلوتين";
                finalPrice = finalPrice + (crustPrice*quantity);
                DecimalFormat dec2 =new DecimalFormat("0.00");
                itemPrice.setText("" + dec2.format(finalPrice) );
                isCrustSelected= true;
                regularPennyLayout.setBackgroundColor(Color.parseColor("#CCFFFFFF"));
                glutenPennyLayout.setBackgroundColor(Color.parseColor("#FF4ED1B9"));
                break;

            case R.id.write_comment:
                Intent intent = new Intent(AdditionalsActivity.this, CommentsActivity.class);
                if(language.equalsIgnoreCase("En")){
                    intent.putExtra("title", itemName);
                }else if(language.equalsIgnoreCase("Ar")){
                    intent.putExtra("title", itemNameAr);
                }
                intent.putExtra("itemImage", itemImage);
                intent.putExtra("itemId", itemId);
                intent.putExtra("screen", "item");
                startActivity(intent);
                break;

            case R.id.add_layout:
                if(quantity>0){
                    if(categoryId.equalsIgnoreCase("7") || categoryId.equalsIgnoreCase("8") || categoryId.equalsIgnoreCase("4") || (isPennyRequired && categoryId.equalsIgnoreCase("6"))){
                        if(isCrustSelected){
                            HashMap<String, String> values = new HashMap<>();
                            String ids = "0", additionalsStr = "",additionalsStrAr = "",
                                    additionalsPrice = "0";
//                    if(language.equalsIgnoreCase("En")) {
                            if (AdditionalsListAdapter.additionalsList.size() > 0) {


                                for (SelectedAddtionals sa : AdditionalsListAdapter.additionalsList) {
                                    if (ids.equals("0")) {
                                        additionalsPrice = crustPrice + "," + sa.getPrice();
                                        ids = crustId+","+sa.getAdditionalId();
                                        additionalsStr = crustName+","+sa.getAdditionalName();
                                        additionalsStrAr = crustNameAr+","+sa.getAdditionalNameAr();

                                    } else {
                                        ids = ids + "," + sa.getAdditionalId();
                                        additionalsStr = additionalsStr + "," + sa.getAdditionalName();
                                        additionalsStrAr = additionalsStrAr+","+sa.getAdditionalNameAr();
                                        additionalsPrice = additionalsPrice + "," + sa.getPrice();
                                    }

                                }
                            }else {
                                additionalsPrice = String.valueOf(crustPrice);
                                ids = crustId;
                                additionalsStr = crustName;
                                additionalsStrAr = crustNameAr;
                            }

                            values.put("mainCategoryId", categoryId);
                            values.put("subCategoryId", categoryId);
                            values.put("itemId", itemId);
                            values.put("itemName", itemName);
                            values.put("itemImage", itemImage);
                            values.put("description", itemDesc);
                            values.put("itemTypeId", itemType);
                            values.put("itemPrice", price);
                            values.put("itemPriceAd", Float.toString ((priceAd) + (crustPrice)));
                            Log.e("TAG","pricead "+(priceAd + crustPrice));
                            values.put("additionalId", ids);
                            values.put("additionalName", additionalsStr);
                            values.put("additionalPrice", additionalsPrice);
                            values.put("qty", Integer.toString(quantity));
                            values.put("totalAmount", Float.toString(finalPrice));
                            values.put("size", size);
                            values.put("comment", Constants.COMMENTS);
                            values.put("itemNameAr", itemNameAr);
                            values.put("descriptionAr", itemDescAr);
                            values.put("additionalNameAr", additionalsStrAr);
                            values.put("nonDelivery",isDelivery);
                            myDbHelper.insertOrder(values);
                            Log.d("TAG", "onClick1: "+values.toString());
                            try {
                                OrderFragment.orderPrice.setText("" +dec.format( myDbHelper.getTotalOrderPrice()) );
                                OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                                CategoryItems.orderPrice.setText("" + dec.format(myDbHelper.getTotalOrderPrice()) );
                                CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            onBackPressed();
                        }else{
                            showDialog1();
                        }
                    }else {
                        HashMap<String, String> values = new HashMap<>();
                        String ids = "0", additionalsStr = "", additionalsStrAr = "", additionalsPrice = "0";
//                    if(language.equalsIgnoreCase("En")) {
                        if (AdditionalsListAdapter.additionalsList.size() > 0) {


                            for (SelectedAddtionals sa : AdditionalsListAdapter.additionalsList) {
                                if (ids.equals("0")) {
                                    additionalsPrice = sa.getPrice();
                                    ids = sa.getAdditionalId();
                                    additionalsStr = sa.getAdditionalName();
                                    additionalsStrAr = sa.getAdditionalNameAr();

                                } else {
                                    ids = ids + "," + sa.getAdditionalId();
                                    additionalsStr = additionalsStr + "," + sa.getAdditionalName();
                                    additionalsStrAr = additionalsStrAr + "," + sa.getAdditionalNameAr();
                                    additionalsPrice = additionalsPrice + "," + sa.getPrice();
                                }
                            }
                        }

                        values.put("mainCategoryId", categoryId);
                        values.put("subCategoryId", categoryId);
                        values.put("itemId", itemId);
                        values.put("itemName", itemName);
                        values.put("itemImage", itemImage);
                        values.put("description", itemDesc);
                        values.put("itemTypeId", itemType);
                        values.put("itemPrice", price);
                        values.put("itemPriceAd", Float.toString((((priceAd)+ (crustPrice)))));
                        values.put("additionalId", ids);
                        values.put("additionalName", additionalsStr);
                        values.put("additionalPrice", additionalsPrice);
                        values.put("qty", Integer.toString(quantity));
                        values.put("totalAmount", Float.toString( finalPrice));
                        values.put("size", size);
                        values.put("comment", Constants.COMMENTS);
                        values.put("itemNameAr", itemNameAr);
                        values.put("descriptionAr", itemDescAr);
                        values.put("additionalNameAr", additionalsStrAr);
                        values.put("nonDelivery",isDelivery);
                        myDbHelper.insertOrder(values);

                        Log.d("TAG", "onClick: "+values.toString());

                        try {
                            OrderFragment.orderPrice.setText("" + dec.format(myDbHelper.getTotalOrderPrice()) );
                            OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            CategoryItems.orderPrice.setText("" + dec.format(myDbHelper.getTotalOrderPrice()));
                            CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        onBackPressed();
                    }
                }else{
                    showDialog();
                }
                break;
        }
    }


    public void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(AdditionalsActivity.this, android.R.style.Theme_Material_Light_Dialog));
        if(language.equalsIgnoreCase("En")) {
        // set title
        alertDialogBuilder.setTitle("Oregano");

        // set dialog message
        alertDialogBuilder
                .setMessage("Please select an item to use this function")
                .setCancelable(false)
                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.dismiss();
                    }
                });
        }else if(language.equalsIgnoreCase("Ar")){
            // set title
            alertDialogBuilder.setTitle("اوريجانو");

            // set dialog message
            alertDialogBuilder
                    .setMessage("من فضلك إختر منتج على الأقل لإستخدام تلك العملية")
                    .setCancelable(false)
                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
        }
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void showDialog1(){
        String msg;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(AdditionalsActivity.this, android.R.style.Theme_Material_Light_Dialog));
        if(language.equalsIgnoreCase("En")) {
            if(isPennyRequired){
                msg = "Please select any penne";
            }else {
                msg = "Please select any crust";
            }
        // set title
        alertDialogBuilder.setTitle("Oregano");

        // set dialog message
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.dismiss();
                    }
                });

        }else if(language.equalsIgnoreCase("Ar")){
            if(isPennyRequired){
                msg = "من فضلك اختر البيني المفضل";
            }else {
                msg = "من فضلك اختر العجينة المفضلة";
            }
            // set title
            alertDialogBuilder.setTitle("اوريجانو");

            // set dialog message
            alertDialogBuilder
                    .setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
        }
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(AdditionalsActivity.this);
            dialog = ProgressDialog.show(AdditionalsActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            modifiersList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(AdditionalsActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(AdditionalsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONArray ja = new JSONArray(result);
                            Outer:
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo = ja.getJSONObject(i);
                                Modifiers mod = new Modifiers();
                                ArrayList<Additionals> additionalsList = new ArrayList<>();
                                JSONArray key = jo.getJSONArray("Key");

                                for(int j=0; j < key.length(); j++){


                                    JSONObject jo1 = key.getJSONObject(j);
                                    if(j == 0 ){
                                        String modifierName = jo1.getString("ModifierName");
                                        String modifierName_Ar = jo1.getString("ModifierName_Ar");
                                        String modifierId = jo1.getString("ModifierId");
//                                        if(modifierId.equalsIgnoreCase("1")){
//                                            continue Outer;
//                                        }
                                        mod.setModifierName(modifierName);
                                        mod.setModifierNameAr(modifierName_Ar);
                                        mod.setModifierId(modifierId);
                                    }else{

//                                        JSONObject issueObj = jo.getJSONObject("Value");
                                        Iterator iterator = jo1.keys();
                                        while(iterator.hasNext()){
                                            Additionals adis = new Additionals();
                                            String additonalId = (String)iterator.next();
                                            ArrayList<AdditionalPrices> additionalPrices = new ArrayList<>();
                                            JSONArray ja1 = jo1.getJSONArray(additonalId);

                                            for(int k = 0; k<ja1.length(); k++){
                                                JSONArray additionals = ja1.getJSONArray(k);
                                                if(k==0){
                                                    JSONObject additionalDetails = additionals.getJSONObject(0);
                                                    String additionalName = additionalDetails.getString("AdditionalName");
                                                    String additionalName_Ar = additionalDetails.getString("AdditionalName_Ar");
                                                    String images = additionalDetails.getString("Images");
                                                    String additionalId = additionalDetails.getString("AdditionalId");
                                                    adis.setAdditionalName(additionalName);
                                                    adis.setAdditionalNameAr(additionalName_Ar);
                                                    adis.setAdditionalsId(additionalId);
                                                    adis.setImages(images);
                                                    adis.setModifierId(mod.getModifierId());
                                                }else {
                                                    for(int l = 0; l<additionals.length();l++){
                                                        AdditionalPrices ap = new AdditionalPrices();
                                                        JSONObject jo2 = additionals.getJSONObject(l);
                                                        ap.setItemType(jo2.getString("Type"));
                                                        ap.setPrice(jo2.getString("Price"));
                                                        additionalPrices.add(ap);
                                                    }
                                                    adis.setAdditionalPriceList(additionalPrices);
                                                }



                                            }
                                            additionalsList.add(adis);
                                        }
                                    }


                                }

                                mod.setChildItems(additionalsList);
                                if(mod.getModifierId().equals("1") && categoryId.equalsIgnoreCase("4")){
                                    crustLayout.setVisibility(View.VISIBLE);
                                    crustList.add(mod);
                                }else if(mod.getModifierId().equals("1")){
                                    crustList.add(mod);
                                }
                                else if(mod.getModifierId().equals("9") && categoryId.equalsIgnoreCase("6")){
                                    pennyLayout.setVisibility(View.VISIBLE);
                                    crustList.add(mod);
                                    isPennyRequired = true;
                                }
                                else {
                                    modifiersList.add(mod);
                                }


//                                JSONObject key = jo.getJSONObject("Key");

//                                String categoryId = key.getString("RamadanCategoryId");
//                                String itemId = key.getString("ItemId");
//                                String itemName = key.getString("ItemName");
//                                String description = key.getString("Description");
//                                String itemName_Ar = key.getString("ItemName_Ar");
//                                String description_Ar = key.getString("Description_Ar");
//                                String additionalsId = key.getString("AdditionalsId");
//                                String modifierId = key.getString("ModifierId");
//                                String images = key.getString("Images");

//                                    String itemTypeId = jo.getString("ItemTypeId");
//                                    String price = jo.getString("Price");

//                                JSONObject issueObj = jo.getJSONObject("Value");
//                                Iterator iterator = issueObj.keys();
//                                while(iterator.hasNext()){
//                                    ItemPrices ip = new ItemPrices();
//                                    String itemTypeId = (String)iterator.next();
//
//                                    if(!itemTypeId.contains("$")) {
//                                        String price = issueObj.getString(itemTypeId);
//                                        ip.setItemTypeId(Float.parseFloat(itemTypeId));
//                                        ip.setPrice(price);
//                                        priceList.add(ip);
//                                        Log.i("TAG", ""+itemTypeId+ "  "+price);
//                                    }
//                                }


//                                items.setCategoryId(categoryId);
//                                items.setItemId(itemId);
//                                items.setItemName(itemName);
//                                items.setItemDesc(description);
//                                items.setItemName_ar(itemName_Ar);
//                                items.setItemDesc_ar(description_Ar);
//                                items.setAdditionalsId(additionalsId);
//                                items.setModifierId(modifierId);
//                                items.setImages(images);
//                                items.setItemPrices(priceList);
//
//                                Log.i("TAG", "true"+priceList.size());


//                                itemsList.add(items);


                            }







                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(AdditionalsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            mAdapter.notifyDataSetChanged();

            super.onPostExecute(result);

        }

    }





}
