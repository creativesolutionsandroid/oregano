package com.cs.oregano.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.adapters.CategoryItemsAdapter;
import com.cs.oregano.model.ItemPrices;
import com.cs.oregano.model.Items;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Created by CS on 09-06-2016.
 */
public class CategoryItems extends AppCompatActivity {

    String catId, catName;
    int catType;
    TextView title;
    Toolbar toolbar;
    ListView itemsListView;
    LinearLayout catTypeLayout;
    ImageView type1Img, type2Img;
    TextView type1Txt, type2Txt, type1Desc, type2Desc;
    public static TextView orderPrice, orderQuantity, cartCount;
    RelativeLayout checkOut, addMoreLayout;
    DecimalFormat dec =new DecimalFormat("0.00");
    ArrayList<Items> itemsList = new ArrayList<>();
    ArrayList<Items> itemsList1 = new ArrayList<>();

    CategoryItemsAdapter mAdapter;

    String response;
    private DataBaseHelper myDbHelper;

    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.category_items);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.category_items_arabic);
        }

        myDbHelper = new DataBaseHelper(CategoryItems.this);
        catId = getIntent().getExtras().getString("catId");
        catName = getIntent().getExtras().getString("catName");
        catType = getIntent().getExtras().getInt("cat_type");

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        title = (TextView) findViewById(R.id.header_title);
        itemsListView = (ListView) findViewById(R.id.items_listview);
        orderPrice = (TextView) findViewById(R.id.item_price);
        orderQuantity = (TextView) findViewById(R.id.item_qty);
        checkOut = (RelativeLayout) findViewById(R.id.checkout_layout);
        addMoreLayout = (RelativeLayout) findViewById(R.id.addmore_layout);
        catTypeLayout = (LinearLayout) findViewById(R.id.cat_type);
        type1Img = (ImageView) findViewById(R.id.type1);
        type2Img = (ImageView) findViewById(R.id.type2);
        type1Txt = (TextView) findViewById(R.id.type1Text);
        type2Txt = (TextView) findViewById(R.id.type2Text);
        type1Desc = (TextView) findViewById(R.id.type1Desc);
        type2Desc = (TextView) findViewById(R.id.type2Desc);
        cartCount = (TextView) findViewById(R.id.cart_count);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText(catName);

        if(language.equalsIgnoreCase("En")) {
            if (catType == 2) {
                catTypeLayout.setVisibility(View.VISIBLE);
                if (catId.equalsIgnoreCase("8")) {
                    type1Txt.setText("Pizza Classic");
                    type2Txt.setText("Oregano Creation");
                    RelativeLayout.LayoutParams emailSwichParam = new RelativeLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    emailSwichParam.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    emailSwichParam.setMargins(8, 2, 0, 0);
                    type1Txt.setLayoutParams(emailSwichParam);
                    type2Txt.setLayoutParams(emailSwichParam);
                    type1Txt.setCompoundDrawablePadding(8);
                    type2Txt.setCompoundDrawablePadding(8);
                    type1Txt.setGravity(Gravity.CENTER_VERTICAL);
                    type2Txt.setGravity(Gravity.CENTER_VERTICAL);
                    type1Txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.classic_pizza,0,0,0);
                    type2Txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.creation_pizza,0,0,0);
                    type1Desc.setVisibility(View.VISIBLE);
                    type2Desc.setVisibility(View.VISIBLE);
                } else if (catId.equalsIgnoreCase("11")) {
                    type1Txt.setText("Hot Drinks");
                    type2Txt.setText("Cold Drinks");
                }
            } else {
                catTypeLayout.setVisibility(View.GONE);
            }
        }else if(language.equalsIgnoreCase("Ar")){
            if (catType == 2) {
                catTypeLayout.setVisibility(View.VISIBLE);
                if (catId.equalsIgnoreCase("8")) {
                    type1Txt.setText("بيتزا كلاسيك");
                    type2Txt.setText("ابداعات أوريجانوس");
                    RelativeLayout.LayoutParams emailSwichParam = new RelativeLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    emailSwichParam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    emailSwichParam.setMargins(0, 2, 8, 0);
                    type1Txt.setLayoutParams(emailSwichParam);
                    type2Txt.setLayoutParams(emailSwichParam);
                    type1Txt.setCompoundDrawablePadding(8);
                    type2Txt.setCompoundDrawablePadding(8);
                    type1Txt.setGravity(Gravity.CENTER_VERTICAL);
                    type2Txt.setGravity(Gravity.CENTER_VERTICAL);
                    type1Txt.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.classic_pizza,0);
                    type2Txt.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.creation_pizza,0);
                    type1Desc.setVisibility(View.VISIBLE);
                    type2Desc.setVisibility(View.VISIBLE);
                } else if (catId.equalsIgnoreCase("11")) {
                    type1Txt.setText("مشروبات ساخنة");
                    type2Txt.setText("مشروبات باردة");
                }
            } else {
                catTypeLayout.setVisibility(View.GONE);
            }
        }
        mAdapter = new CategoryItemsAdapter(CategoryItems.this, itemsList, language);
        itemsListView.setAdapter(mAdapter);

        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myDbHelper.getTotalOrderQty() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CategoryItems.this, android.R.style.Theme_Material_Light_Dialog));

                    if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("Oregano");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("There is no items in your order? To proceed checkout please add the items")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("اوريجانو");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {
                    Intent checkoutIntent = new Intent(CategoryItems.this, CheckoutActivity.class);
                    startActivity(checkoutIntent);
                }
            }
        });

        type1Img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type1Img.setClickable(false);
                type2Img.setClickable(true);
                type1Img.setImageResource(R.drawable.bg_pizaa_select);
                type2Img.setImageResource(R.drawable.bg_pizaa_normal);
                if(catId.equalsIgnoreCase("8")){
                    new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL+catId);
                }else if(catId.equalsIgnoreCase("11")){
                    new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL+catId);
                }
            }
        });

        type2Img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type1Img.setClickable(true);
                type2Img.setClickable(false);
                type1Img.setImageResource(R.drawable.bg_pizaa_normal);
                type2Img.setImageResource(R.drawable.bg_pizaa_select);
                if(catId.equalsIgnoreCase("8")){
                    new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL+"7");
                }else if(catId.equalsIgnoreCase("11")){
                    new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL+"10");
                }
            }
        });

        addMoreLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL+catId);

        orderPrice.setText("" + dec.format( myDbHelper.getTotalOrderPrice()) );
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        cartCount.setText("" + myDbHelper.getTotalOrderQty());

        cartCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkOut.performClick();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            itemsList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(CategoryItems.this);
                dialog = ProgressDialog.show(CategoryItems.this, "",
                        "Loading items...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            itemsList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(CategoryItems.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(CategoryItems.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONArray ja = new JSONArray(result);
                                for (int i = 0; i < ja.length(); i++) {
                                    Items items = new Items();
                                    JSONObject jo = ja.getJSONObject(i);

                                    JSONObject key = jo.getJSONObject("Key");

                                    String categoryId = key.getString("CategoryId");
                                    String itemId = key.getString("ItemId");
                                    String itemName = key.getString("ItemName");
                                    String description = key.getString("Description");
                                    String itemName_Ar = key.getString("ItemName_Ar");
                                    String description_Ar = key.getString("Description_Ar");
                                    String additionalsId = key.getString("AdditionalsId");
                                    String modifierId = key.getString("ModifierId");
                                    String images = key.getString("Images");
                                    String isDelivery = key.getString("IsDelivery");

//                                    String itemTypeId = jo.getString("ItemTypeId");
//                                    String price = jo.getString("Price");
                                    ArrayList<ItemPrices> priceList = new ArrayList<>();
                                    JSONObject issueObj = jo.getJSONObject("Value");
                                    Iterator iterator = issueObj.keys();
                                    while(iterator.hasNext()){
                                        ItemPrices ip = new ItemPrices();
                                        String itemTypeId = (String)iterator.next();

                                        if(!itemTypeId.contains("$")) {
                                            String price = issueObj.getString(itemTypeId);
                                            ip.setItemTypeId(Integer.parseInt(itemTypeId));
                                            ip.setPrice(price);
                                            priceList.add(ip);
                                        }
                                    }

                                    Collections.sort(priceList, ItemPrices.priceSort);

                                    items.setCategoryId(categoryId);
                                    items.setItemId(itemId);
                                    items.setItemName(itemName);
                                    items.setItemDesc(description);
                                    items.setItemName_ar(itemName_Ar);
                                    items.setItemDesc_ar(description_Ar);
                                    items.setAdditionalsId(additionalsId);
                                    items.setModifierId(modifierId);
                                    items.setImages(images);
                                    items.setItemPrices(priceList);
                                    items.setIsDelivery(isDelivery);

                                    itemsList.add(items);

                                }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(CategoryItems.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            mAdapter.notifyDataSetChanged();

            super.onPostExecute(result);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
        orderPrice.setText("" + dec.format(myDbHelper.getTotalOrderPrice()) );
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        cartCount.setText("" + myDbHelper.getTotalOrderQty());
    }
}
