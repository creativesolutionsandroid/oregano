package com.cs.oregano.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.MainActivity;
import com.cs.oregano.R;
import com.cs.oregano.model.Order;

import java.util.ArrayList;

/**
 * Created by CS on 09-06-2016.
 */
public class MenuActivity extends AppCompatActivity {
    RelativeLayout carryOut, dineIn, delivery;
    Toolbar toolbar;
    SharedPreferences userPrefs;
    String mLoginStatus;
    SharedPreferences languagePrefs;
    String language;
    private DataBaseHelper myDbHelper;
    ArrayList<Order> NonDeliveryArray = new ArrayList<>();
    AlertDialog customDialog;
    float vat = 5;
    float totalprice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.order_fragment);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.order_fragment_arabic);
        }
        myDbHelper = new DataBaseHelper(MenuActivity.this);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        carryOut = (RelativeLayout) findViewById(R.id.carryout_layout);
        dineIn = (RelativeLayout) findViewById(R.id.dinein_layout);
        delivery = (RelativeLayout) findViewById(R.id.delivery_layout);

        carryOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginStatus = userPrefs.getString("login_status", "");
                Constants.ORDER_TYPE = "Carryout";
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(MenuActivity.this, SelectStoresActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
                    startActivityForResult(intent, 2);
                }

            }
        });

        dineIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginStatus = userPrefs.getString("login_status", "");
                Constants.ORDER_TYPE = "Dine-In";
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(MenuActivity.this, SelectStoresActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
                    startActivityForResult(intent, 2);
                }
            }
        });

        delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NonDeliveryArray = myDbHelper.getNonDeliveryItems();

                float tax = myDbHelper.getTotalOrderPrice()*(vat/100);

                totalprice = myDbHelper.getTotalOrderPrice() + tax ;
                if(totalprice >= 50) {

                    if(NonDeliveryArray!=null && NonDeliveryArray.size()>0){

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MenuActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        TextView title = (TextView) dialogView.findViewById(R.id.alert_title);

                        if(language.equalsIgnoreCase("Ar")){
                            yes.setText("تغيير نوع الطلب");
                            no.setText("إزالة المواد التي لا يتم توصيلها");
                            title.setText("مواد لا يتم توصيلها");
                        }
                        String NDitems = null;
                        for(int i = 0; i<NonDeliveryArray.size(); i++){
                            if(i==0){
                                if(language.equalsIgnoreCase("Ar")){
                                    NDitems = NonDeliveryArray.get(i).getItemNameAr();
                                }
                                else {
                                    NDitems = NonDeliveryArray.get(i).getItemName();
                                }
                            }
                            else{
                                if(language.equalsIgnoreCase("Ar")){
                                    NDitems = NDitems+"\n"+NonDeliveryArray.get(i).getItemNameAr();
                                }
                                else {
                                    NDitems = NDitems+"\n"+NonDeliveryArray.get(i).getItemName();
                                }
                            }
                        }

                        desc.setText(NDitems);

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                for(int i = 0; i<NonDeliveryArray.size(); i++) {
                                    myDbHelper.deleteItemFromOrderID(NonDeliveryArray.get(i).getItemId(), NonDeliveryArray.get(i).getItemTypeId());
                                }
                                customDialog.dismiss();
                                finish();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else {
                        mLoginStatus = userPrefs.getString("login_status", "");
                        Constants.ORDER_TYPE = "Delivery";
                        if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                            Intent intent = new Intent(MenuActivity.this, AddressActivity.class);
                            intent.putExtra("confirm_order", true);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
                            startActivityForResult(intent, 2);
                        }
                    }
                }else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MenuActivity.this, android.R.style.Theme_Material_Light_Dialog));


                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Oregano");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Minimum delivery order amount 50 SR.\n\nAdd more items to continue for delivery order")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                        Intent intent = new Intent(MenuActivity.this, MainActivity.class);
                                        intent.putExtra("startWith",2);
                                        startActivity(intent);

                                    }
                                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("اوريجانو");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("الحد الأدنى للتوصيل بقيمة 50 ريال\n" +
                                        "\n" +
                                        "أضف مزيدا من الطلبات للاستمرار في توصيل طلبك")
                                .setCancelable(false)
                                .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                        Intent intent = new Intent(MenuActivity.this, MainActivity.class);
                                        intent.putExtra("startWith",2);
                                        startActivity(intent);
                                    }
                                }).setNegativeButton("لا", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            Intent intent = new Intent(MenuActivity.this, SelectStoresActivity.class);
            startActivity(intent);
        }else if (resultCode == RESULT_CANCELED){
            Toast.makeText(MenuActivity.this, "Login unseccessful", Toast.LENGTH_SHORT).show();
        }
    }
}
