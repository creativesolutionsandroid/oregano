package com.cs.oregano.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.adapters.ViewOrderAdapter;
import com.cs.oregano.model.ViewOrder;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 01-05-2017.
 */

public class ViewDetailsActivity extends AppCompatActivity {

    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    SharedPreferences userPrefs;
    String userId, orderId;
    ArrayList<ViewOrder> OrderList = new ArrayList<>();
    ViewOrderAdapter mAdapter;
    ListView listView;
    public static TextView totalQty;
    TextView totalAmount, vatAmount, mamount, promoAmount, netAmount;
    int tQty = 0;
    ImageView fav;
    AlertDialog alertDialog;
    String response;
    boolean isFav = false;

    String subTotal, vatCharges, vatPercentage, totalPrice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.view_details);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.view_details_arabic);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        orderId = getIntent().getStringExtra("orderid");

        fav = (ImageView) findViewById(R.id.fav);
        listView = (ListView)findViewById(R.id.order_info_list);
        totalQty = (TextView) findViewById(R.id.totalQty);
        totalAmount = (TextView) findViewById(R.id.totalAmount);
        promoAmount = (TextView) findViewById(R.id.promoAmount);
        mamount = (TextView) findViewById(R.id.subAmount);
        netAmount = (TextView) findViewById(R.id.netAmount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);


        mAdapter = new ViewOrderAdapter(ViewDetailsActivity.this, OrderList, language);
        listView.setAdapter(mAdapter);


        new GetCheckoutOrderDetails().execute(Constants.ORDERD_DETAILS_URL+orderId+"&Userid="+userId);

        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFav) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ViewDetailsActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.insert_fav_order_dialog;
                    if (language.equalsIgnoreCase("En")) {
                        layout = R.layout.insert_fav_order_dialog;
                    } else if (language.equalsIgnoreCase("Ar")) {
                        layout = R.layout.insert_fav_order_dialog_arabic;
                    }
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);

                    final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
                    TextView cancelBtn = (TextView) dialogView.findViewById(R.id.no_cancel);
                    final TextView saveOrder = (TextView) dialogView.findViewById(R.id.save_order);
                    favName.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (favName.getText().toString().length() > 0) {
                                saveOrder.setEnabled(true);
                            } else {
                                saveOrder.setEnabled(false);
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {


                        }
                    });


                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });


                    saveOrder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new InsertFavOrder().execute(Constants.INSERT_FAVORITE_ORDER_URL + orderId + "&FOrderName=" + favName.getText().toString().replace(" ", "%20"));
                            alertDialog.dismiss();
                        }
                    });


                    alertDialog = dialogBuilder.create();
                    alertDialog.show();

                    //Grab the window of the dialog, and change the width
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = alertDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else{
                    new DeleteFavOrder().execute(Constants.DELETE_FAVORITE_ORDERS_URL+orderId);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public class InsertFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ViewDetailsActivity.this);
            dialog = ProgressDialog.show(ViewDetailsActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("PUT");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    response = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ViewDetailsActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(ViewDetailsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            String s = jo.getString("Success");
                            fav.setImageResource(R.drawable.favourite_select);
                            isFav = true;

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ViewDetailsActivity.this, "Can not reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            }else {
                Toast.makeText(ViewDetailsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ViewDetailsActivity.this);
            dialog = ProgressDialog.show(ViewDetailsActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("PUT");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    response = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ViewDetailsActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(ViewDetailsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
//                            JSONArray jo= new JSONArray(result);
                            fav.setImageResource(R.drawable.favorite);
                            isFav = false;
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ViewDetailsActivity.this, "Can not reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            }else {
                Toast.makeText(ViewDetailsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class GetCheckoutOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ViewDetailsActivity.this);
            dialog = ProgressDialog.show(ViewDetailsActivity.this, "",
                    "Loading items...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ViewDetailsActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(ViewDetailsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            HashMap<String, String> values = new HashMap<>();

                            JSONObject jo = new JSONObject(result);

                            JSONArray jaa = jo.getJSONArray("MainItem");
                            JSONObject obj = jaa.getJSONObject(0);
                            totalPrice = obj.getString("Total_Price");
                            subTotal = obj.getString("SubTotal");
                            vatCharges = obj.getString("VatCharges");
                            vatPercentage = obj.getString("VatPercentage");
                            float promoAmt = obj.getInt("PromoAmt");
                            float total = Float.parseFloat(Constants.convertToArabic(subTotal)) - Float.parseFloat(String.valueOf(promoAmt));
                            promoAmount.setText(""+Constants.decimalFormat.format(Float.parseFloat(String.valueOf(promoAmt))));
                            mamount.setText(""+Constants.decimalFormat.format(Float.parseFloat(String.valueOf(total))));
                            vatAmount.setText(""+Constants.decimalFormat.format(Float.parseFloat(vatCharges)));
                            netAmount.setText(""+Constants.decimalFormat.format(Float.parseFloat(String.valueOf(totalPrice))));
                            totalAmount.setText(""+Constants.decimalFormat.format(Float.parseFloat(subTotal)));
                            isFav = obj.getBoolean("IsFavorite");
                            if(isFav){
                                fav.setImageResource(R.drawable.favourite_select);
                            }
                            else{
                                fav.setImageResource(R.drawable.favorite);
                            }


                            JSONArray ja = jo.getJSONArray("SubItem");
                            for(int i = 0; i< ja.length(); i++){
                                String ids = "0", additionalsStr = "",additionalsStrAr = "", additionalsPrice = "";
                                String categoryId = "", itemId = "", itemName = "",itemNameAr = "", itemImage = "", itemDesc = "",itemDescAr = "", itemType = "";
                                String price = "";
                                float additionPrice = 0;
                                float priceAd = 0, finalPrice = 0;
                                int quantity = 0;
                                JSONArray ja1 = ja.getJSONArray(i);
                                ViewOrder viewOrder = new ViewOrder();
                                for(int j=0;j<ja1.length();j++){
                                    if(j==0){
                                        JSONObject jo1 = ja1.getJSONObject(0);
                                        viewOrder.setItemId(jo1.getString("ItemId"));
                                        viewOrder.setItemName(jo1.getString("ItemName"));
                                        viewOrder.setItemNameAr(jo1.getString("ItemName_Ar"));
                                        viewOrder.setItemImage(jo1.getString("Images"));
                                        viewOrder.setItemDesc(jo1.getString("Description"));
                                        viewOrder.setItemDescAr(jo1.getString("Description_Ar"));
                                        viewOrder.setItemType(jo1.getString("Size"));
                                        viewOrder.setQuantity(jo1.getString("Qty"));
                                        tQty = tQty+jo1.getInt("Qty");
                                        viewOrder.setPrice(jo1.getString("ItemPrice"));
                                        viewOrder.setCategoryId(jo1.getString("RamadanCategoryId"));
                                    }else{
                                        JSONArray ja2 = ja1.getJSONArray(j);
                                        for(int k =0; k< ja2.length(); k++){
                                            JSONObject jo2 = ja2.getJSONObject(k);
                                            if(!jo2.getString("AdditionalID").equals("0")) {
                                                if (ids.equalsIgnoreCase("0")) {
                                                    ids = jo2.getString("AdditionalID");
                                                    additionalsStr = jo2.getString("AdditionalName");
                                                    additionalsStrAr = jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = jo2.getString("AdditionalPrice");
                                                }else{
                                                    ids = ids +","+ jo2.getString("AdditionalID");
                                                    additionalsStr = additionalsStr +","+jo2.getString("AdditionalName");
                                                    additionalsStrAr = additionalsStrAr +","+jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = additionalsPrice + ","+jo2.getString("AdditionalPrice");
                                                }
                                                additionPrice = additionPrice + Float.parseFloat(jo2.getString("AdditionalPrice"));
                                            }

                                        }
                                    }
                                }
                                viewOrder.setIds(ids);
                                viewOrder.setAdditionalsStr(additionalsStr);
                                viewOrder.setAdditionalsStrAr(additionalsStrAr);
                                viewOrder.setAdditionalsPrice(additionalsPrice);
                                OrderList.add(viewOrder);
                            }
                            if(tQty<10) {
                                totalQty.setText("0" + tQty);
                            }
                            else{
                                totalQty.setText("" + tQty);
                            }
                            mAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(ViewDetailsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);

        }

    }
}
