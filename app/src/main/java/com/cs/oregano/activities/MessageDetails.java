package com.cs.oregano.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.oregano.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by CS on 10-08-2016.
 */
public class MessageDetails extends AppCompatActivity {
    Toolbar toolbar;
    ImageView icon;
    TextView status, date, message, clear;
    String image, statusStr, dateStr, messageStr;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.message_details);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.message_details_arabic);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        statusStr = getIntent().getExtras().getString("status");
        dateStr = getIntent().getExtras().getString("date");
        messageStr = getIntent().getExtras().getString("message");

        icon = (ImageView) findViewById(R.id.image);
        status = (TextView) findViewById(R.id.status);
        date = (TextView) findViewById(R.id.date);
        message = (TextView) findViewById(R.id.message);

        message.setText(""+messageStr);
        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date dateObj = null;
        String[] parts = dateStr.split(" ");
        try {
            dateObj = curFormater.parse(dateStr.replace("  ", " 0"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a");
        String datetext = postFormater.format(dateObj);

        date.setText(datetext);

        if(language.equalsIgnoreCase("En")) {
            if (statusStr.equals("1")) {
                status.setText("Confirmed");
                icon.setImageResource(R.drawable.notification1);
            } else if (statusStr.equals("2")) {
                status.setText("Ready");
                icon.setImageResource(R.drawable.notification2);
            } else if (statusStr.equals("3")) {
                status.setText("Served");
                icon.setImageResource(R.drawable.notification3);
            } else if (statusStr.equals("4")) {
                status.setText("Cancel");
                icon.setImageResource(R.drawable.notification4);
            } else if (statusStr.equals("5")) {
                status.setText("On The Way");
                icon.setImageResource(R.drawable.notification5);
            } else if (statusStr.equals("6")) {
                status.setText("Delivered");
                icon.setImageResource(R.drawable.notification6);
            } else if (statusStr.equals("7")) {
                status.setText("Offer");
                icon.setImageResource(R.drawable.notification7);
            } else if (statusStr.equals("8")) {
                status.setText("Welcome");
                icon.setImageResource(R.drawable.notification8);
            }
        }else if(language.equalsIgnoreCase("Ar")) {
            if (statusStr.equals("1")) {
                status.setText("تأكيد");
                icon.setImageResource(R.drawable.notification1_arabic);
            } else if (statusStr.equals("2")) {
                status.setText("جاهز");
                icon.setImageResource(R.drawable.notification2_arabic);
            } else if (statusStr.equals("3")) {
                status.setText("تم استلامه");
                icon.setImageResource(R.drawable.notification3_arabic);
            } else if (statusStr.equals("4")) {
                status.setText("الغاء");
                icon.setImageResource(R.drawable.notification4_arabic);
            } else if (statusStr.equals("5")) {
                status.setText("في الطريق");
                icon.setImageResource(R.drawable.notification5_arabic);
            } else if (statusStr.equals("6")) {
                status.setText("تم التوصيل");
                icon.setImageResource(R.drawable.notification6_arabic);
            } else if (statusStr.equals("7")) {
                status.setText(" عرض خاص");
                icon.setImageResource(R.drawable.notification7_arabic);
            } else if (statusStr.equals("8")) {
                status.setText("مرحباً ");
                icon.setImageResource(R.drawable.notification8_arabic);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
