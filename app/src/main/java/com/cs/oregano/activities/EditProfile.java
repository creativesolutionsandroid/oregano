package com.cs.oregano.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.R;
import com.cs.oregano.widgets.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by CS on 09-08-2016.
 */
public class EditProfile extends AppCompatActivity {
    EditText mFirstName, mFamilyName, mNickName, mPhoneNumber, mEmail;
    TextView mGenderMale, mGenderFemale;
    private String firstName, familyName,nickName, phoneNumber, email, gender = "Male";
    RelativeLayout updateBtn;
    Toolbar toolbar;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    SharedPreferences languagePrefs;
    String language;
    String response;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.profile_update);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.profile_update_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        String profile = userPrefs.getString("user_profile",null);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFirstName = (EditText) findViewById(R.id.name);
        mFamilyName = (EditText) findViewById(R.id.family_name);
        mNickName = (EditText) findViewById(R.id.nick_name);
        mPhoneNumber = (EditText) findViewById(R.id.mobile_number);
        mEmail = (EditText) findViewById(R.id.email);
        mGenderMale = (TextView) findViewById(R.id.gender_male);
        mGenderFemale = (TextView) findViewById(R.id.gender_female);
        updateBtn = (RelativeLayout) findViewById(R.id.update_btn);

        setFilters();

        if(profile != null) {
            try {


                JSONObject  property = new JSONObject(profile);
                JSONObject userObjuect = property.getJSONObject("profile");

//                String parts[] = userObjuect.getString("phone").split("-");

//                userId = userObjuect.getString("userId");
                mFirstName.setText(userObjuect.getString("fullName"));
                mFamilyName.setText(userObjuect.getString("family_name"));
                mNickName.setText(userObjuect.getString("nick_name"));
                mPhoneNumber.setText("+"+userObjuect.getString("mobile"));
                mEmail.setText(userObjuect.getString("email"));
                gender = userObjuect.getString("gender");


            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }

        }

        if(gender.equalsIgnoreCase("Male")){
            mGenderMale.setTextColor(Color.parseColor("#FFFFFF"));
            mGenderFemale.setTextColor(Color.parseColor("#000000"));
            mGenderMale.setBackgroundColor(Color.parseColor("#00000000"));
            mGenderFemale.setBackgroundColor(Color.parseColor("#F2F2F2"));
        }else if(gender.equalsIgnoreCase("Female")){
            mGenderMale.setTextColor(Color.parseColor("#000000"));
            mGenderFemale.setTextColor(Color.parseColor("#FFFFFF"));
            mGenderMale.setBackgroundColor(Color.parseColor("#F2F2F2"));
            mGenderFemale.setBackgroundColor(Color.parseColor("#00000000"));
        }

        mGenderMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender = "Male";
                mGenderMale.setTextColor(Color.parseColor("#FFFFFF"));
                mGenderFemale.setTextColor(Color.parseColor("#000000"));
                mGenderMale.setBackgroundColor(Color.parseColor("#00000000"));
                mGenderFemale.setBackgroundColor(Color.parseColor("#F2F2F2"));
            }
        });

        mGenderFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender = "Female";
                mGenderMale.setTextColor(Color.parseColor("#000000"));
                mGenderFemale.setTextColor(Color.parseColor("#FFFFFF"));
                mGenderMale.setBackgroundColor(Color.parseColor("#F2F2F2"));
                mGenderFemale.setBackgroundColor(Color.parseColor("#00000000"));
            }
        });

        mFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().startsWith(" ")){
                    mFirstName.setText("");
                }
            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject parent = new JSONObject();
                firstName = mFirstName.getText().toString();
                familyName = mFamilyName.getText().toString();
                nickName = mNickName.getText().toString();
                phoneNumber = mPhoneNumber.getText().toString();
                email = mEmail.getText().toString().replaceAll(" ","");
                if(firstName.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mFirstName.setError("Please enter First Name");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mFirstName.setError("من فضلك ارسل الاسم بالكامل");
                    }
                }else if(phoneNumber.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mPhoneNumber.setError("Please enter Mobile Number");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mPhoneNumber.setError("من فضلك أدخل رقم الجوال");
                    }
                }else if(email.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mEmail.setError("Please enter Email");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mEmail.setError("من فضلك ادخل البريد الالكتروني");
                    }
                }else if(!isValidEmail(email)){
                    if(language.equalsIgnoreCase("En")) {
                        mEmail.setError("Please use Email format (example - abc@abc.com");
                    }else if(language.equalsIgnoreCase("Ar")){
                        mEmail.setError("من فضلك استخدم صيغة البريد الالكتروني (example - abc@abc.com)");
                    }

                }else{
                    try {
                        JSONArray mainItem = new JSONArray();



                        JSONObject mainObj = new JSONObject();
                        mainObj.put("FullName",firstName);
                        mainObj.put("FamilyName", familyName);
                        mainObj.put("NickName", nickName);
                        mainObj.put("Gender", gender);
                        mainObj.put("Email", email);
                        mainObj.put("DeviceToken", SplashActivity.regid);
                        mainObj.put("Language", language);
                        mainObj.put("UserId", userId);
                        mainItem.put(mainObj);



                        parent.put("UserProfile", mainItem);
                        Log.i("TAG", parent.toString());
                    }catch (JSONException je){

                    }
                    new InsertRegistration().execute(parent.toString());
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public class InsertRegistration extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(EditProfile.this);
            dialog = ProgressDialog.show(EditProfile.this, "",
                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.UPDATE_PROFILE_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(EditProfile.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(EditProfile.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
//                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    Toast.makeText(EditProfile.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                                    finish();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je){
                                je.printStackTrace();
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(EditProfile.this, android.R.style.Theme_Material_Light_Dialog));

                                if(language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("Oregano");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(result)
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                }else if(language.equalsIgnoreCase("Ar")){
                                    // set title
                                    alertDialogBuilder.setTitle("اوريجانو");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(result)
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                }


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(EditProfile.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void setFilters() {
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                return Character.isLetterOrDigit(c) || Character.isSpaceChar(c);
            }
        };

        mFirstName.setFilters(new InputFilter[] { filter });
        mFamilyName.setFilters(new InputFilter[] { filter });
        mNickName.setFilters(new InputFilter[] { filter });

    }
}
