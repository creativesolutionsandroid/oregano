package com.cs.oregano.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.GPSTracker;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.adapters.SelectStoresAdapter;
import com.cs.oregano.model.StoreInfo;
import com.cs.oregano.widgets.NetworkUtil;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 15-06-2016.
 */
public class SelectStoresActivity extends AppCompatActivity {

    Toolbar toolbar;
    ListView listView;
    TextView title;

    private String timeResponse = null;

    String response;
    private ArrayList<StoreInfo> storesList = new ArrayList<>();
    SelectStoresAdapter mAdapter;

    Double lat, longi;
    String serverTime;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;

    SharedPreferences languagePrefs;
    String language;

    private SwipeRefreshLayout swipeLayout;
    boolean loading = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        setContentView(R.layout.select_stores_layout);
        title = (TextView) findViewById(R.id.header_title);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.stores_list);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        }else {
            getGPSCoordinates();
        }


        if(language.equalsIgnoreCase("En")){
            title.setText("Select Store");
        }else if(language.equalsIgnoreCase("Ar")){
            title.setText("إختر الفرع");
        }

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                if(!loading) {
                    loading = true;
                    getGPSCoordinates();
                }else{
                    swipeLayout.setRefreshing(false);
                }

            }
        });
        swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StoreInfo si = storesList.get(position);
                if(si.getOpenFlag() != 1){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(SelectStoresActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Oregano");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Sorry! Store is closed! You can't make the order from this store.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("اوريجانو");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("نأسف الفرع مغلق ، لا يمكنك إتمام الطلب من ذلك الفرع")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {
                    Intent intent = new Intent(SelectStoresActivity.this, OrderConfirmation.class);
                    if(language.equalsIgnoreCase("En")){
                        intent.putExtra("storeName", si.getStoreName());
                        intent.putExtra("storeAddress", si.getStoreAddress());
                    }else if(language.equalsIgnoreCase("Ar")){
                        intent.putExtra("storeName", si.getStoreName_ar());
                        intent.putExtra("storeAddress", si.getStoreAddress_ar());
                    }
                    intent.putExtra("storeImage", si.getImageURL());
                    intent.putExtra("storeId", si.getStoreId());
                    intent.putExtra("latitude", si.getLatitude());
                    intent.putExtra("longitude", si.getLongitude());
                    intent.putExtra("lat", lat);
                    intent.putExtra("longi", longi);
                    intent.putExtra("start_time", si.getStartTime());
                    intent.putExtra("end_time", si.getEndTime());
                    intent.putExtra("full_hours", si.getIs24x7());
                    intent.putExtra("order_type", Constants.ORDER_TYPE);
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void getGPSCoordinates(){
            gps = new GPSTracker(SelectStoresActivity.this);
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
                new GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(SelectStoresActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(SelectStoresActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String dayOfWeek;
        @Override
        protected void onPreExecute() {
            storesList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(SelectStoresActivity.this);
            dialog = ProgressDialog.show(SelectStoresActivity.this, "",
                    "Fetching stores...");
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]+dayOfWeek);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            storesList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(SelectStoresActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(SelectStoresActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONArray ja = new JSONArray(result);
                            for (int i = 0; i < ja.length(); i++) {
                                StoreInfo si = new StoreInfo();
                                JSONObject jo = ja.getJSONObject(i);
//                                lat = 24.70321657;
//                                longi = 46.68097073;
                                si.setStoreId(jo.getString("storeId"));
                                si.setOnlineOrderStatus(jo.getString("OnlineOrderStatus"));
                                si.setStartTime(jo.getString("ST"));
                                si.setEndTime(jo.getString("ET"));
                                si.setStoreName(jo.getString("StoreName"));
                                si.setStoreAddress(jo.getString("StoreAddress"));
                                si.setLatitude(jo.getDouble("Latitude"));
                                si.setLongitude(jo.getDouble("Longitude"));
                                si.setCountryName(jo.getString("CountryName"));
                                si.setCityName(jo.getString("CityName"));
                                si.setImageURL(jo.getString("imageURL"));
                                si.setCarryoutdistance(jo.getInt("DineInDistance"));
                                si.setFamilySection(jo.getString("FamilySection"));
                                si.setWifi(jo.getString("Wifi"));
                                si.setPatioSitting(jo.getString("PatioSitting"));
                                si.setDriveThru(jo.getString("DriveThru"));
                                si.setMeetingSpace(jo.getString("MeetingSpace"));
                                si.setHospital(jo.getString("Hospital"));
                                si.setUniversity(jo.getString("University"));
                                si.setOffice(jo.getString("Office"));
                                si.setShoppingMall(jo.getString("ShoppingMall"));
                                try{
                                    si.setAirPort(jo.getString("Airport"));
                                }catch (Exception e){
                                    si.setAirPort("false");
                                }
                                try{
                                    si.setDineIn(jo.getString("DineIn"));
                                }catch (Exception e){
                                    si.setDineIn("false");
                                }
                                try{
                                    si.setLadies(jo.getString("Ladies"));
                                }catch (Exception e){
                                    si.setLadies("false");
                                }

                                si.setNeighborhood(jo.getString("Neighborhood"));
                                si.setStoreNumber(jo.getString("phone"));
                                si.setIs24x7(jo.getString("is24x7"));
                                si.setStatus(jo.getString("status"));
                                si.setOgCountry(jo.getString("OGCountry"));
                                si.setOgCity(jo.getString("OGCity"));
                                si.setStoreName_ar(jo.getString("StoreName_ar"));
                                si.setStoreAddress_ar(jo.getString("StoreAddress_ar"));
                                si.setStoreNumber(jo.getString("phone"));
                                try{
                                    si.setMessage(jo.getString("Message"));
                                }catch (Exception e){
                                    si.setMessage("");
                                }

                                try{
                                    si.setMessage_ar(jo.getString("Message_ar"));
                                }catch (Exception e){
                                    si.setMessage_ar("");
                                }

                                Location me   = new Location("");
                                Location dest = new Location("");

                                me.setLatitude(lat);
                                me.setLongitude(longi);

                                dest.setLatitude(jo.getDouble("Latitude"));
                                dest.setLongitude(jo.getDouble("Longitude"));

                                float dist = (me.distanceTo(dest))/1000;
                                si.setDistance(dist);

                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
                                SimpleDateFormat dateFormat3 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                                Calendar c = Calendar.getInstance();
                                System.out.println("Current time => " + c.getTime());
                                serverTime = timeResponse;
                                String startTime = si.getStartTime();
                                String endTime = si.getEndTime();


//                                if(dist <= 50 && (jo.getBoolean("OnlineOrderStatus"))) {
//                                if(dist <= si.getCarryoutdistance() && (jo.getBoolean("OnlineOrderStatus"))) {
                                if(dist <= 5000 && (jo.getBoolean("OnlineOrderStatus"))) {
                                    if (startTime.equals("null") && endTime.equals("null")) {
                                        si.setOpenFlag(-1);
                                        storesList.add(si);
                                    } else {

                                        if (endTime.equals("00:00AM")) {
                                            si.setOpenFlag(1);
                                            storesList.add(si);

                                            continue;
                                        } else if (endTime.equals("12:00AM")) {
                                            endTime = "11:59PM";
                                        }

                                        Calendar now = Calendar.getInstance();

                                        int hour = now.get(Calendar.HOUR_OF_DAY);
                                        int minute = now.get(Calendar.MINUTE);


                                        Date serverDate = null;
                                        Date end24Date = null;
                                        Date start24Date = null;
                                        Date current24Date = null;
                                        Date dateToday = null;
                                        Calendar dateStoreClose = Calendar.getInstance();
                                        try {
                                            serverDate = dateFormat.parse(serverTime);
                                            dateToday = dateFormat.parse(serverTime);
                                            end24Date = dateFormat3.parse(endTime);
                                            start24Date = dateFormat3.parse(startTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        Date startDate = null;
                                        Date endDate = null;

//                                        try {
//                                            dateStoreClose.setTime(dateToday);
//                                            dateStoreClose.add(Calendar.DATE, 1);
//                                            String current24 = timeFormat1.format(serverDate);
//                                            String end24 = timeFormat1.format(end24Date);
//                                            String start24 = timeFormat1.format(start24Date);
//                                            String startDateString = dateFormat1.format(dateToday);
//                                            String endDateString = dateFormat1.format(dateToday);
//                                            String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
//                                            dateStoreClose.add(Calendar.DATE, -2);
//                                            String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());
//
//
//                                            try {
//                                                end24Date = timeFormat1.parse(end24);
//                                                start24Date = timeFormat1.parse(start24);
//                                                current24Date = timeFormat1.parse(current24);
//                                            } catch (ParseException e) {
//                                                e.printStackTrace();
//                                            }
//
//                                            String[] parts2 = start24.split(":");
//                                            int startHour = Integer.parseInt(parts2[0]);
//                                            int startMinute = Integer.parseInt(parts2[1]);
//
//                                            String[] parts = end24.split(":");
//                                            int endHour = Integer.parseInt(parts[0]);
//                                            int endMinute = Integer.parseInt(parts[1]);
//
//                                            String[] parts1 = current24.split(":");
//                                            int currentHour = Integer.parseInt(parts1[0]);
//                                            int currentMinute = Integer.parseInt(parts1[1]);
//
//
////                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");
//
//
//                                            if (startTime.contains("AM") && endTime.contains("AM")) {
//                                                if (startHour < endHour) {
//                                                    startDateString = startDateString + " " + startTime;
//                                                    endDateString = endDateString + "  " + endTime;
//                                                    try {
//                                                        startDate = dateFormat2.parse(startDateString);
//                                                        endDate = dateFormat2.parse(endDateString);
//                                                    } catch (ParseException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                } else if (startHour > endHour) {
//                                                    if (serverTime.contains("AM")) {
//                                                        if (currentHour > endHour) {
//                                                            startDateString = startDateString + " " + startTime;
//                                                            endDateString = endDateTomorrow + "  " + endTime;
//                                                            try {
//                                                                startDate = dateFormat2.parse(startDateString);
//                                                                endDate = dateFormat2.parse(endDateString);
//                                                            } catch (ParseException e) {
//                                                                e.printStackTrace();
//                                                            }
//                                                        } else {
//                                                            startDateString = endDateYesterday + " " + startTime;
//                                                            endDateString = endDateString + "  " + endTime;
//                                                            try {
//                                                                startDate = dateFormat2.parse(startDateString);
//                                                                endDate = dateFormat2.parse(endDateString);
//                                                            } catch (ParseException e) {
//                                                                e.printStackTrace();
//                                                            }
//                                                        }
//                                                    } else {
//                                                        startDateString = startDateString + " " + startTime;
//                                                        endDateString = endDateTomorrow + "  " + endTime;
//                                                        try {
//                                                            startDate = dateFormat2.parse(startDateString);
//                                                            endDate = dateFormat2.parse(endDateString);
//                                                        } catch (ParseException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    }
//                                                }
//                                            } else if (startTime.contains("AM") && endTime.contains("PM")) {
//                                                startDateString = startDateString + " " + startTime;
//                                                endDateString = endDateString + "  " + endTime;
//                                                try {
//                                                    startDate = dateFormat2.parse(startDateString);
//                                                    endDate = dateFormat2.parse(endDateString);
//                                                } catch (ParseException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            } else if (startTime.contains("PM") && endTime.contains("AM")) {
//                                                if (serverTime.contains("AM")) {
//                                                    if (currentHour <= endHour) {
//                                                        startDateString = endDateYesterday + " " + startTime;
//                                                        endDateString = endDateString + "  " + endTime;
//                                                        try {
//                                                            startDate = dateFormat2.parse(startDateString);
//                                                            endDate = dateFormat2.parse(endDateString);
//                                                        } catch (ParseException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    } else {
//                                                        startDateString = startDateString + " " + startTime;
//                                                        endDateString = endDateTomorrow + "  " + endTime;
//                                                        try {
//                                                            startDate = dateFormat2.parse(startDateString);
//                                                            endDate = dateFormat2.parse(endDateString);
//                                                        } catch (ParseException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    }
//                                                } else {
//                                                    startDateString = startDateString + " " + startTime;
//                                                    endDateString = endDateTomorrow + "  " + endTime;
//                                                    try {
//                                                        startDate = dateFormat2.parse(startDateString);
//                                                        endDate = dateFormat2.parse(endDateString);
//                                                    } catch (ParseException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                }
//
//                                            } else if (startTime.contains("PM") && endTime.contains("PM")) {
//                                                startDateString = startDateString + " " + startTime;
//                                                endDateString = endDateString + "  " + endTime;
//                                            }
//                                        } catch (NumberFormatException e) {
//                                            e.printStackTrace();
//                                        }

                                        try {
                                            startDate = dateFormat3.parse(si.getStartTime());
                                            endDate = dateFormat3.parse(si.getEndTime());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        String serverDateString = null;

                                        try {
                                            serverDateString = dateFormat.format(serverDate);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        try {
                                            serverDate = dateFormat.parse(serverDateString);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("TAG DATE", "" + startDate);
                                        Log.i("TAG DATE1", "" + endDate);
                                        Log.i("TAG DATE2", "" + serverDate);

                                        if (serverDate.after(startDate) && serverDate.before(endDate)) {
                                            Log.i("TAG Visible", "true");
                                            si.setOpenFlag(1);
                                            storesList.add(si);
                                        } else {
                                            si.setOpenFlag(0);
                                            storesList.add(si);
                                        }
                                    }
                                }
//                                storesList.add(si);
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Collections.sort(storesList, StoreInfo.storeDistance);
                        Collections.sort(storesList, StoreInfo.storeOpenSort);
                    }
                }

            }else {
                Toast.makeText(SelectStoresActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            mAdapter.notifyDataSetChanged();
            loading = false;
            swipeLayout.setRefreshing(false);

            if(storesList.size() == 0){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(SelectStoresActivity.this, android.R.style.Theme_Material_Light_Dialog));

                if (language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("Oregano");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("We're sorry, but no stores were found in your area.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                } else if (language.equalsIgnoreCase("Ar")) {
                    // set title
                    alertDialogBuilder.setTitle("اوريجانو");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("نعتذر لك ، لا يوجد فروع متوفرة في منطتك")
                            .setCancelable(false)
                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                }

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

            super.onPostExecute(result);

        }

    }


    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(SelectStoresActivity.this);
                dialog = ProgressDialog.show(SelectStoresActivity.this, "",
                        "Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            }else{
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(serverTime == null){
                dialog.dismiss();
            } else if(serverTime.equals("no internet")){
                dialog.dismiss();
                Toast.makeText(SelectStoresActivity.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
//                    timeResponse = "24/4/2018 05:00 PM";
                }catch (JSONException je){
                    je.printStackTrace();
                }
                new GetStoresInfo().execute(Constants.STORES_URL);
//                if(language.equalsIgnoreCase("En")) {

                mAdapter = new SelectStoresAdapter(SelectStoresActivity.this, storesList, lat, longi, timeResponse);
                    listView.setAdapter(mAdapter);
//                }else if(language.equalsIgnoreCase("Ar")){
//                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
//                    mStoresListView.setAdapter(mStoreListAdapterArabic);
//                }

            }


            super.onPostExecute(result1);
        }
    }
}
