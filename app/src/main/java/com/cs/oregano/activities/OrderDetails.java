package com.cs.oregano.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.GPSTracker;
import com.cs.oregano.MainActivity;
import com.cs.oregano.R;
import com.cs.oregano.widgets.NetworkUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 16-06-2016.
 */
public class OrderDetails extends AppCompatActivity {

    private String insertResponse = null;
    private GoogleMap map;
    MarkerOptions markerOptions;
    TextView storeNameTextView, storeAddressTextView, totalItems, totalAmount, estTime, orderNumber, paymentMode, orderType, netTotal, vatAmount, amount;
    TextView storeAddressTxt;
    TextView vatPercent,receipt_close;
    ImageView minvoice;
    Button trackOrder, createOrder;
    CardView getDirection;
    ImageView favOrder;
    private String storeId, storeName, storeAddress, total_amt, total_items, expected_time, payment_mode, order_type, order_number, orderId, mamount, mvatamount, mnetamount;
    private Double latitude, longitude;
    SharedPreferences userPrefs;
    String response, userResponse, userId;
    SharedPreferences orderPrefs;
    AlertDialog alertDialog;
    SharedPreferences.Editor orderPrefsEditor;
    SharedPreferences languagePrefs;
    String language;
    GPSTracker gps;
    RelativeLayout recieptLayout;
    private double lat, longi;
    float vat=5;

    DecimalFormat decim =new DecimalFormat("0.00");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.order_details);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.order_details_arabic);
        }
        storeId = getIntent().getExtras().getString("storeId");
        storeName = getIntent().getExtras().getString("storeName");
        storeAddress = getIntent().getExtras().getString("storeAddress");
        latitude = getIntent().getExtras().getDouble("latitude");
        longitude = getIntent().getExtras().getDouble("longitude");
        total_amt = getIntent().getExtras().getString("total_amt");
        total_items = getIntent().getExtras().getString("total_items");
        expected_time = getIntent().getExtras().getString("expected_time");
        mamount = getIntent().getExtras().getString("amount");
        mvatamount = getIntent().getExtras().getString("vatamount");
        mnetamount = getIntent().getExtras().getString("netamount");
        payment_mode = getIntent().getExtras().getString("payment_mode");
        order_type = getIntent().getExtras().getString("order_type");
        order_number = getIntent().getExtras().getString("order_number");
        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor  = orderPrefs.edit();
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userResponse = userPrefs.getString("user_profile", null);
        userId = userPrefs.getString("userId", null);
        storeNameTextView = (TextView) findViewById(R.id.store_detail_name);
        storeAddressTextView = (TextView) findViewById(R.id.store_detail_address);
        totalItems = (TextView) findViewById(R.id.total_items);
        totalAmount = (TextView) findViewById(R.id.total_amount);
        estTime = (TextView) findViewById(R.id.expected_time);
        orderNumber = (TextView) findViewById(R.id.order_number);
        paymentMode = (TextView) findViewById(R.id.payment_mode);
        orderType = (TextView) findViewById(R.id.order_type);
        getDirection = (CardView) findViewById(R.id.get_direction);
        trackOrder = (Button) findViewById(R.id.track_order_btn);
        createOrder = (Button) findViewById(R.id.create_order_btn);
        favOrder = (ImageView) findViewById(R.id.fav_order);
        storeAddressTxt = (TextView) findViewById(R.id.store_address_txt);

        recieptLayout = (RelativeLayout) findViewById(R.id.receipt_layout);
        receipt_close = (TextView) findViewById(R.id.receipt_close);
        amount = (TextView) findViewById(R.id.amount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        vatPercent = (TextView) findViewById(R.id.vatPercent);
        netTotal = (TextView) findViewById(R.id.net_total);
        minvoice= (ImageView) findViewById(R.id.invoice);

//        FragmentManager fm = getChildFragmentManager();
//        Fragment fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
//        if (fragment == null) {
//            fragment = SupportMapFragment.newInstance();
//            fm.beginTransaction().replace(R.id.map, fragment).commit();
//        }
//        map = ((SupportMapFragment) fragment).getMap();
//        markerOptions = new MarkerOptions();
//        markerOptions.position(new LatLng(latitude, longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
//        map.addMarker(markerOptions);
//        map.animateCamera(CameraUpdateFactory.newLatLngZoom
//                (new LatLng(latitude, longitude), 7.0f));

        String[] orderNo = order_number.split("-");
        String[] parts = order_number.split(",");
        orderId = parts[0];

        orderPrefsEditor.putString("order_id", orderId);
        orderPrefsEditor.putString("order_status", "open");
        orderPrefsEditor.commit();

        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieptLayout.setVisibility(View.GONE);
            }
        });


        minvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recieptLayout.setVisibility(View.VISIBLE);
            }
        });
//        DecimalFormat decim = new DecimalFormat("0.00");
        amount.setText(mamount);
//        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
        vatAmount.setText(mvatamount);
        netTotal.setText(mnetamount);

        storeNameTextView.setText(storeName);
        storeAddressTextView.setText(storeAddress);
        totalItems.setText(total_items);
        totalAmount.setText(decim.format(Float.parseFloat(Constants.convertToArabic(total_amt))));
        SimpleDateFormat datetime = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy",Locale.US);
        SimpleDateFormat time = new SimpleDateFormat("hh:mm a",Locale.US);
        Date date2 = null, time2 = null;
        try {
            time2 = datetime.parse(expected_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String date1 = date.format(time2);
        String time1 = time.format(time2);
        estTime.setText(date1+"\n"+time1);
        if (order_type.equalsIgnoreCase("Delivery")) {
            if(language.equalsIgnoreCase("En")){
                storeAddressTxt.setText("Your Address");
            }else if(language.equalsIgnoreCase("Ar")){
                storeAddressTxt.setText("عنوانك");
            }
            storeNameTextView.setTextSize(12);
            storeAddressTextView.setTextSize(12);
        }

        if(language.equalsIgnoreCase("En")) {
            if (payment_mode.equals("2")) {
                paymentMode.setText("Cash Payment");
            } else if (payment_mode.equals("3")) {
                paymentMode.setText("Online Payment");
            }else if(payment_mode.equals("4")){
                paymentMode.setText("Free Drink");
            }
            orderType.setText(order_type);
        }else if(language.equalsIgnoreCase("Ar")) {
            if (payment_mode.equals("2")) {
                paymentMode.setText("نقدي عند استلام الطلب");
            } else if (payment_mode.equals("3")) {
                paymentMode.setText("الدفع أونلاين");
            } else if(payment_mode.equals("4")){
                paymentMode.setText("مجاني");
            }

            if(order_type.equalsIgnoreCase("Dine-In")){
                orderType.setText("داخل الفرع");
            }else if(order_type.equalsIgnoreCase("Carryout")){
                orderType.setText("خارج الفرع");
            }else if(order_type.equalsIgnoreCase("Delivery")){
                orderType.setText("توصيل");
                getDirection.setVisibility(View.GONE);
            }
        }
//        paymentMode.setText(payment_mode);

        if(order_type.equalsIgnoreCase("Delivery")){
            getDirection.setVisibility(View.GONE);
        }
        orderNumber.setText(orderNo[1]);


        gps = new GPSTracker(OrderDetails.this);
        if(gps.canGetLocation()){

            lat = gps.getLatitude();
            longi = gps.getLongitude();
        }

        getDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+lat+","+longi+"&daddr=" + latitude + "," + longitude));
                startActivity(intent);
            }
        });

        trackOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(OrderDetails.this, TrackOrderSteps.class);
                intent.putExtra("orderId", orderId);
                startActivity(intent);

//                Fragment merchandiseFragment = new TrackOrderFragment();
//                Bundle mBundle7 = new Bundle();
//                mBundle7.putString("orderId", orderId);
//                mBundle7.putInt("flag", 0);
//                merchandiseFragment.setArguments(mBundle7);

            }
        });

        createOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderDetails.this, MainActivity.class);
                intent.putExtra("startWith",2);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });




        favOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.insert_fav_order_dialog;
                if(language.equalsIgnoreCase("En")){
                    layout = R.layout.insert_fav_order_dialog;
                }else if(language.equalsIgnoreCase("Ar")){
                    layout = R.layout.insert_fav_order_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);

                final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
                TextView cancelBtn = (TextView) dialogView.findViewById(R.id.no_cancel);
                final TextView saveOrder = (TextView) dialogView.findViewById(R.id.save_order);
                favName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(favName.getText().toString().length()>0){
                            saveOrder.setEnabled(true);
                        }else{
                            saveOrder.setEnabled(false);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {


                    }
                });


                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });



                saveOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new InsertFavOrder().execute(Constants.INSERT_FAVORITE_ORDER_URL+orderId+"&FOrderName="+favName.getText().toString().replace(" ", "%20"));
                        alertDialog.dismiss();
                    }
                });


                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });
    }


    public class InsertFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetails.this);
            dialog = ProgressDialog.show(OrderDetails.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("PUT");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    response = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderDetails.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            String s = jo.getString("Success");
                            favOrder.setImageResource(R.drawable.favourite_select);
                            favOrder.setEnabled(false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(OrderDetails.this, "Can not reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            }else {
                Toast.makeText(OrderDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
