package com.cs.oregano.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.oregano.Constants;
import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.MainActivity;
import com.cs.oregano.R;
import com.cs.oregano.adapters.CheckoutAdapter;
import com.cs.oregano.fragments.OrderFragment;
import com.cs.oregano.model.Order;
import com.cs.oregano.widgets.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by CS on 15-06-2016.
 */
public class CheckoutActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    public static TextView orderPrice, orderQuantity, clearOrder, netTotal, vatAmount, amount;
    RelativeLayout ordernowLayout, addMoreLayout, recieptLayout;
    LinearLayout  vatLayout;
    TextView vatPercent,receipt_close;
    ImageView minvoice;
    ListView listView;
    ArrayList<Order> orderList = new ArrayList<>();
    AlertDialog alertDialog;
    String favNameTxt;
    public static int CHECKOUT_REQUEST = 1;
    String userId;
    SharedPreferences userPrefs;
    
    private DataBaseHelper myDbHelper;

    CheckoutAdapter mAdapter;
    SharedPreferences languagePrefs;
    String language;
    float vat = 5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.checkout_layout);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.checkout_layout_arabic);
        }

        myDbHelper = new DataBaseHelper(CheckoutActivity.this);

        orderList = myDbHelper.getOrderInfo();

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "-1");

        recieptLayout = (RelativeLayout) findViewById(R.id.receipt_layout);
        receipt_close = (TextView) findViewById(R.id.receipt_close);
        amount = (TextView) findViewById(R.id.amount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        vatPercent = (TextView) findViewById(R.id.vatPercent);
        netTotal = (TextView) findViewById(R.id.net_total);
        minvoice= (ImageView) findViewById(R.id.invoice);

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        orderPrice = (TextView) findViewById(R.id.item_price);
        orderQuantity = (TextView) findViewById(R.id.item_qty);

        addMoreLayout = (RelativeLayout) findViewById(R.id.addmore_layout);
        ordernowLayout = (RelativeLayout) findViewById(R.id.ordernow_layout);
        listView = (ListView) findViewById(R.id.order_info_list);
        clearOrder = (TextView) findViewById(R.id.clear_order);

        mAdapter = new CheckoutAdapter(CheckoutActivity.this, orderList, language);
        listView.setAdapter(mAdapter);

        addMoreLayout.setOnClickListener(this);
        ordernowLayout.setOnClickListener(this);
        clearOrder.setOnClickListener(this);

        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieptLayout.setVisibility(View.GONE);
            }
        });

        minvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieptLayout.setVisibility(View.VISIBLE);
            }
        });
        double number;
        number=myDbHelper.getTotalOrderPrice();
        DecimalFormat decim = new DecimalFormat("0.00");
        amount.setText(""+decim.format(myDbHelper.getTotalOrderPrice()));
        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
        vatAmount.setText(""+decim.format(tax));
        netTotal.setText(""+decim.format(myDbHelper.getTotalOrderPrice()+tax));
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        clearOrder.setText("" + myDbHelper.getTotalOrderQty());
        orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addmore_layout:
                Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
                intent.putExtra("startWith",2);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;

            case R.id.clear_order:

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CheckoutActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = null;
                if(language.equalsIgnoreCase("En")){
                    dialogView = inflater.inflate(R.layout.dialog_save_order, null);
                }else if(language.equalsIgnoreCase("Ar")){
                    dialogView = inflater.inflate(R.layout.dialog_save_order_ar, null);
                }
//                View dialogView = inflater.inflate(R.layout.cancel_order_dialog, null);
                dialogBuilder.setView(dialogView);

                final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
                TextView cancelBtn = (TextView) dialogView.findViewById(R.id.cancel);
                final TextView clearOrder = (TextView) dialogView.findViewById(R.id.order_clear);
                final TextView saveOrder = (TextView) dialogView.findViewById(R.id.save_order);
                TextView cancelAlertText = (TextView) dialogView.findViewById(R.id.cancel_alert_text);
                if(language.equalsIgnoreCase("En")){
                    cancelAlertText.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                }else if(language.equalsIgnoreCase("Ar")){
                    cancelAlertText.setText(" منتج في الحقيبة ، من خلال هذا الإجراء ستصبح حقيبتك خالية من المنتجا ت" +myDbHelper.getTotalOrderQty()+"  لديك  ");
                }

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                clearOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDbHelper.deleteOrderTable();
                        alertDialog.dismiss();
                        Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
                        intent.putExtra("startWith",2);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

                saveOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        favNameTxt = favName.getText().toString();
                        if(favNameTxt.length()>0){
                            alertDialog.dismiss();
                            if(userId.equals("-1")) {
                                Intent intent = new Intent(CheckoutActivity.this, LoginActivity.class);
                                startActivityForResult(intent, CHECKOUT_REQUEST);
                            }
                            else {
                                new InsertOrder().execute();
                            }
                        }
                    }
                });

                favName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        favNameTxt = favName.getText().toString();
                        if(favNameTxt.length()>0){
                            saveOrder.setTextColor(Color.parseColor("#2A8AEE"));
                        }
                        else{
                            saveOrder.setTextColor(Color.parseColor("#555555"));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                break;

//                AlertDialog.Builder alertDialogBuilder1 = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//
//                if(language.equalsIgnoreCase("En")) {
//                    // set title
//                    alertDialogBuilder1.setTitle("Oregano");
//
//                    // set dialog message
//                    alertDialogBuilder1
//                            .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear")
//                            .setCancelable(false)
//                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                }
//                            })
//                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    myDbHelper.deleteOrderTable();
//                                    try {
//
//                                        double number;
//                                        number=myDbHelper.getTotalOrderPrice();
//                                        DecimalFormat decim = new DecimalFormat("0.00");
//                                        amount.setText(""+decim.format(myDbHelper.getTotalOrderPrice()));
//                                        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
//                                        vatAmount.setText(""+decim.format(tax));
//                                        netTotal.setText(""+decim.format(myDbHelper.getTotalOrderPrice()+tax));
//                                        CheckoutActivity.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice())+tax);
//                                        CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        clearOrder.setText("" + myDbHelper.getTotalOrderQty());
//                                        OrderFragment.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice())  );
//                                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        CategoryItems.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()) );
//                                        CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        CategoryItems.cartCount.setText("" + myDbHelper.getTotalOrderQty());
//                                    } catch (NullPointerException e) {
//                                        e.printStackTrace();
//                                    }
//                                    dialog.dismiss();
//                                    Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
//                                    intent.putExtra("startWith",2);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            });
//                }else if(language.equalsIgnoreCase("Ar")){
//                    // set title
//                    alertDialogBuilder1.setTitle("اوريجانو");
//
//                    // set dialog message
//                    alertDialogBuilder1
//                            .setMessage(" منتج في الحقيبة ، من خلال هذا الإجراء ستصبح حقيبتك خالية من المنتجا ت" +myDbHelper.getTotalOrderQty()+"  لديك  ")
//                            .setCancelable(false)
//                            .setNegativeButton("لا", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                }
//                            })
//                            .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    myDbHelper.deleteOrderTable();
//                                    try {
//                                        double number;
//                                        number=myDbHelper.getTotalOrderPrice();
//                                        DecimalFormat decim = new DecimalFormat("0.00");
//                                        amount.setText(""+decim.format(myDbHelper.getTotalOrderPrice()));
//                                        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
//                                        vatAmount.setText(""+decim.format(tax));
//                                        netTotal.setText(""+decim.format(myDbHelper.getTotalOrderPrice()+tax));
//                                        CheckoutActivity.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice())+tax);
//                                        CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        clearOrder.setText("" + myDbHelper.getTotalOrderQty());
//                                        OrderFragment.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()) );
//                                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        CategoryItems.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()) );
//                                        CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        CategoryItems.cartCount.setText("" + myDbHelper.getTotalOrderQty());
//                                    } catch (NullPointerException e) {
//                                        e.printStackTrace();
//                                    }
//                                    dialog.dismiss();
//                                    Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
//                                    intent.putExtra("startWith",2);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            });
//                }
//
//
//                // create alert dialog
//                AlertDialog alertDialog1 = alertDialogBuilder1.create();
//
//                // show it
//                alertDialog1.show();

            case R.id.ordernow_layout:
                if(myDbHelper.getTotalOrderQty() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Oregano");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if(language.equalsIgnoreCase("Ar")){
                        // set title
                        alertDialogBuilder.setTitle("اوريجانو");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    Intent menuIntent = new Intent(CheckoutActivity.this, MenuActivity.class);
                    startActivity(menuIntent);
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CHECKOUT_REQUEST && resultCode == RESULT_OK){
            new InsertOrder().execute();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public class InsertOrder extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        String currentTime;
        String kitchenStart;
        String mamount, mvatamount;
        String estimatedTime, total_amt;
        JSONObject parent = new JSONObject();
        InputStream inputStream = null;
        String result = "";
        ProgressDialog dialog;

        String itemId = "", qty = "", comments = "", total_price = null, item_price = "", sizes = "", additionals = "", additionalsPices = "";
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat6 = new SimpleDateFormat("dd-MM-yyyy HH:MM", Locale.US);
        SimpleDateFormat timeFormat5 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(CheckoutActivity.this);
            dialog = ProgressDialog.show(CheckoutActivity.this, "",
                    "Please wait...");

            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());

            SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
            dateFormat1.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
            df3.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));

            currentTime = df3.format(c.getTime()).replace("a.m", "AM")
                    .replace("p.m", "PM").replace("am", "AM")
                    .replace("pm", "PM").replace("AM.", "AM")
                    .replace("PM.", "PM");
            estimatedTime = dateFormat1.format(c.getTime()).replace("a.m", "AM")
                    .replace("p.m", "PM").replace("am", "AM")
                    .replace("pm", "PM").replace("AM.", "AM")
                    .replace("PM.", "PM");

            total_amt = orderPrice.getText().toString().replace(" SR", "");
            DecimalFormat dec = new DecimalFormat("0.00");
            mamount = dec.format(Float.parseFloat(String.valueOf(myDbHelper.getTotalOrderPrice())));

            float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
            mvatamount = dec.format(tax);

            String version = "";
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                version = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            try {
                JSONArray mainItem = new JSONArray();
                JSONArray subItem = new JSONArray();
                JSONArray promoItem = new JSONArray();

                JSONObject mainObj = new JSONObject();
                mainObj.put("UserId", userId);
                mainObj.put("StoreId", "3");
                mainObj.put("OrderType", "Favorite");
                mainObj.put("ExpectedTime", estimatedTime);
                mainObj.put("OrderDate", currentTime);
                mainObj.put("IsFavorite", true);
                mainObj.put("FavoriteName", favNameTxt);
                mainObj.put("comments", "Android v" + version + "," + getResources().getConfiguration().locale);
                mainObj.put("PaymentMode", "0");
                mainObj.put("Total_Price", Constants.convertToArabic(total_amt));
                mainObj.put("OrderStatus", "Close");
                mainObj.put("Device_token", SplashActivity.regid);
                mainObj.put("VatPercentage", "5");
                mainObj.put("SubTotal", Constants.convertToArabic(mamount));
                mainObj.put("VatCharges", Constants.convertToArabic(mvatamount));
                mainItem.put(mainObj);

                JSONObject promoObj = new JSONObject();
                promoObj.put("DeviceToken", SplashActivity.regid);
                promoObj.put("promotionCode", "");
                promoObj.put("BonusAmt", "");

                promoItem.put(promoObj);

                for (Order order : orderList) {
                    JSONObject subObj = new JSONObject();

                    JSONArray subItem1 = new JSONArray();
                    JSONArray subItem2 = new JSONArray();

                    subObj.put("ItemPrice", Constants.convertToArabic(dec.format(Float.parseFloat(order.getItemPrice()))));
                    subObj.put("Qty", order.getQty());
                    subObj.put("Comments", order.getComment());
                    subObj.put("ItemId", order.getItemId());
                    subObj.put("Size", order.getItemTypeId());
                    if (!order.getAdditionalId().equals("")) {
                        String[] addIdParts = order.getAdditionalId().split(",");
                        String[] addPriceParts = order.getAdditionalPrice().split(",");
                        for (int i = 0; i < addIdParts.length; i++) {
                            JSONObject subObj1 = new JSONObject();
                            if (addPriceParts[i] != null) {
                                subObj1.put("AdditionalID", addIdParts[i]);
                                subObj1.put("AdditionalPrice", Constants.convertToArabic(addPriceParts[i]));
                                subItem2.put(subObj1);
                            }
                        }
                    }
                    subItem1.put(subObj);
                    if (subItem2.length() > 0) {
                        subItem1.put(subItem2);
                    }
                    subItem.put(subItem1);
                }

                parent.put("MainItem", mainItem);
                parent.put("SubItem", subItem);
                Log.i("TAG", parent.toString());
            } catch (JSONException je) {
                je.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.INSERT_ORDER_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(parent.toString());

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            result = convertInputStreamToString(inputStream);
                            return result;
                        }
                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + result);
            } else {
                return "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                if (result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Oregano");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    myDbHelper.deleteOrderTable();
                    try {
                        DecimalFormat decim = new DecimalFormat("0.00");
                        OrderFragment.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice())  );
                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        CategoryItems.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()) );
                        CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                        CategoryItems.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    alertDialog.dismiss();
                    Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
                    intent.putExtra("startWith",2);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }
            dialog.dismiss();
            super.onPostExecute(result1);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
