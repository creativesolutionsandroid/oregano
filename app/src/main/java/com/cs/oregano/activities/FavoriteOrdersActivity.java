package com.cs.oregano.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.oregano.Constants;
import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.adapters.FavouriteOrderAdapter;
import com.cs.oregano.model.FavouriteOrder;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 21-06-2016.
 */
public class FavoriteOrdersActivity extends AppCompatActivity {

    private DataBaseHelper myDbHelper;
    String response;
    private ArrayList<FavouriteOrder> favoritesList = new ArrayList<>();
    private SwipeMenuListView orderHistoryListView;
    TextView emptyView;
    private FavouriteOrderAdapter mAdapter;
    SharedPreferences userPrefs;
    String userId;
    TextView title;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        setContentView(R.layout.order_history);
        myDbHelper = new DataBaseHelper(FavoriteOrdersActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        title = (TextView) findViewById(R.id.header_title);




        orderHistoryListView = (SwipeMenuListView) findViewById(R.id.fav_order_listview);
        emptyView = (TextView) findViewById(R.id.empty_view);
        mAdapter = new FavouriteOrderAdapter(FavoriteOrdersActivity.this, favoritesList, language);
        orderHistoryListView.setAdapter(mAdapter);
        new GetFavoriteOrderDetails().execute(Constants.GET_FAVORITE_ORDERS_URL+userId);
        if(language.equalsIgnoreCase("En")){
            title.setText("Favorite Orders");
        }else if(language.equalsIgnoreCase("Ar")){
            title.setText("المفضلة");
            emptyView.setText("لا يوجد منتجات في السلة");
        }
        orderHistoryListView.setEmptyView(emptyView);

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                if(language.equalsIgnoreCase("En")){
                    deleteItem.setTitle("Delete");
                }
                else if(language.equalsIgnoreCase("Ar")){
                    deleteItem.setTitle("حذف");
                }
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        orderHistoryListView.setMenuCreator(creator);

        // step 2. listener item click event
        orderHistoryListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        new DeleteFavOrder().execute(Constants.DELETE_FAVORITE_ORDERS_URL + favoritesList.get(position).getOrderId());
                        break;
                }
                return false;
            }
        });


        orderHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                myDbHelper.deleteOrderTable();
                String orderId = favoritesList.get(position).getOrderId();
                new GetOrderDetails().execute(Constants.ORDERD_DETAILS_URL+orderId+"&Userid="+userId);
            }
        });
    }


    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteOrdersActivity.this);
            dialog = ProgressDialog.show(FavoriteOrdersActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("PUT");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    response = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(FavoriteOrdersActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

//                        try {
//                            JSONObject jo= new JSONObject(result);
//                            String s = jo.getString("Success");
                            new GetFavoriteOrderDetails().execute(Constants.GET_FAVORITE_ORDERS_URL+userId);
//                            Toast.makeText(FavoriteOrdersActivity.this, "Order deleted successfully", Toast.LENGTH_SHORT).show();

//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Toast.makeText(FavoriteOrdersActivity.this, "Can not reach server", Toast.LENGTH_SHORT).show();
//                        }

                    }
                }

            }else {
                Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteOrdersActivity.this);
            dialog = ProgressDialog.show(FavoriteOrdersActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(FavoriteOrdersActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            HashMap<String, String> values = new HashMap<>();

                            JSONObject jo = new JSONObject(result);

                            JSONArray ja = jo.getJSONArray("SubItem");
                            myDbHelper.deleteOrderTable();
                            for(int i = 0; i< ja.length(); i++){
                                String ids = "0", additionalsStr = "",additionalsStrAr = "";
                                float additionalsPrice = 0;
                                String categoryId = "", itemId = "", itemName = "",itemNameAr = "", itemImage = "", itemDesc = "",itemDescAr = "", itemType = "";
                                String nonDelivery= "";
                                String price = "";
                                String additionPrice = "";
                                float prices = 0;
                                float priceAd = 0, finalPrice = 0;
                                int quantity = 0;
                                JSONArray ja1 = ja.getJSONArray(i);
                                for(int j=0;j<ja1.length();j++){
                                    if(j==0){
                                        JSONObject jo1 = ja1.getJSONObject(0);
                                        itemId = jo1.getString("ItemId");
                                        itemName = jo1.getString("ItemName");
                                        itemNameAr = jo1.getString("ItemName_Ar");
                                        itemImage = jo1.getString("Images");
                                        itemDesc = jo1.getString("Description");
                                        itemDescAr = jo1.getString("Description_Ar");
                                        itemType = jo1.getString("Size");
                                        quantity = jo1.getInt("Qty");
                                        price = jo1.getString("ItemPrice");
                                        categoryId = jo1.getString("CategoryId");
                                        nonDelivery = jo1.getString("IsDelivery");
                                    }else{
                                        JSONArray ja2 = ja1.getJSONArray(j);
                                        for(int k =0; k< ja2.length(); k++){
                                            JSONObject jo2 = ja2.getJSONObject(k);
                                            if(!jo2.getString("AdditionalID").equals("0")) {
                                                if (ids.equalsIgnoreCase("0")) {
                                                    ids = jo2.getString("AdditionalID");
                                                    additionalsStr = jo2.getString("AdditionalName");
                                                    additionalsStrAr = jo2.getString("AdditionalName_Ar");
                                                    additionPrice = jo2.getString("AdditionalPrice");
                                                }else{
                                                    ids = ids +","+ jo2.getString("AdditionalID");
                                                    additionalsStr = additionalsStr +","+jo2.getString("AdditionalName");
                                                    additionalsStrAr = additionalsStrAr +","+jo2.getString("AdditionalName_Ar");
                                                    additionPrice = additionPrice + ","+jo2.getString("AdditionalPrice");
                                                }
                                                additionalsPrice = additionalsPrice + Float.parseFloat(jo2.getString("AdditionalPrice"));
                                            }

                                        }

                                        priceAd = Float.parseFloat(price) + additionalsPrice;
                                        prices = Float.parseFloat(price);
                                    }
                                }




                                finalPrice = priceAd * quantity;


                            values.put("mainCategoryId", categoryId);
                            values.put("subCategoryId", categoryId);
                            values.put("itemId", itemId);
                            values.put("itemName", itemName);
                            values.put("itemImage", itemImage);
                            values.put("description", itemDesc);
                            values.put("itemTypeId", itemType);
                            values.put("itemPrice", price);
                            values.put("itemPriceAd", String.valueOf((priceAd)));
                            values.put("additionalId", ids);
                            values.put("additionalName", additionalsStr);
                            values.put("additionalPrice", additionPrice);
                            values.put("qty", Integer.toString(quantity));
                            values.put("totalAmount", String.valueOf(finalPrice));
                            values.put("size", itemType);
                            values.put("comment", "");
                            values.put("itemNameAr", itemNameAr);
                            values.put("descriptionAr", itemDescAr);
                            values.put("additionalNameAr", additionalsStrAr);
                            values.put("nonDelivery", nonDelivery);
                            myDbHelper.insertOrder(values);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            Intent intent = new Intent(FavoriteOrdersActivity.this, CheckoutActivity.class);
            startActivity(intent);

            super.onPostExecute(result);

        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }



    public class GetFavoriteOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteOrdersActivity.this);
            dialog = ProgressDialog.show(FavoriteOrdersActivity.this, "",
                    "Loading...");
            favoritesList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    JSONParser jParser = new JSONParser();

                    response = jParser
                            .getJSONFromUrl(params[0]);
                    Log.i("TAG", "user response:" + response);
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            favoritesList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(FavoriteOrdersActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i<ja.length(); i++) {

                                    FavouriteOrder fo = new FavouriteOrder();
                                    JSONObject jo1 = ja.getJSONObject(i);


                                    String orderId = jo1.getString("odrId");
                                    String favoriteName = jo1.getString("Fname");
                                    String orderDate = jo1.getString("Odate");
                                    String storeName = jo1.getString("Sname");
                                    String storeName_ar = jo1.getString("Sname_ar");
                                    String total_Price = jo1.getString("TotPrice");
                                    String itemDetails = "", itemDetailsAr = "";

                                    JSONArray ja1 = jo1.getJSONArray("items");
                                    for(int j=0;j<ja1.length();j++){
                                        JSONObject jo2 = ja1.getJSONObject(j);
                                        if(itemDetails.equalsIgnoreCase("") && itemDetailsAr.equalsIgnoreCase("")){
                                            itemDetails = jo2.getString("ItmName");
                                            itemDetailsAr = jo2.getString("ItmName_ar");
                                        }else{
                                            itemDetails = itemDetails + ", "+jo2.getString("ItmName");
                                            itemDetailsAr = itemDetailsAr+ ", "+jo2.getString("ItmName_ar");
                                        }
                                    }



                                    fo.setOrderId(orderId);
                                    fo.setFavoriteName(favoriteName);
                                    fo.setOrderDate(orderDate);
                                    fo.setStoreName(storeName);
                                    fo.setStoreName_ar(storeName_ar);
                                    fo.setTotalPrice(total_Price);
                                    fo.setItemDetails(itemDetails);
                                    fo.setItemDetails_ar(itemDetailsAr);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                    favoritesList.add(fo);

                                }
                            }catch (JSONException je){
                                je.printStackTrace();
//                                Toast.makeText(FavoriteOrdersActivity.this, "Sorry You Don't Have any Favorite Orders", Toast.LENGTH_SHORT).show();
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Oregano");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(FavoriteOrdersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }
}
