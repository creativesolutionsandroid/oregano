package com.cs.oregano.model;

/**
 * Created by CS on 18-08-2017.
 */

public class PreparationTime {

    String catId, catName, qty1, qty2, qty3, qty4, qty5, qty5Plus;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getQty1() {
        return qty1;
    }

    public void setQty1(String qty1) {
        this.qty1 = qty1;
    }

    public String getQty2() {
        return qty2;
    }

    public void setQty2(String qty2) {
        this.qty2 = qty2;
    }

    public String getQty3() {
        return qty3;
    }

    public void setQty3(String qty3) {
        this.qty3 = qty3;
    }

    public String getQty4() {
        return qty4;
    }

    public void setQty4(String qty4) {
        this.qty4 = qty4;
    }

    public String getQty5() {
        return qty5;
    }

    public void setQty5(String qty5) {
        this.qty5 = qty5;
    }

    public String getQty5Plus() {
        return qty5Plus;
    }

    public void setQty5Plus(String qty5Plus) {
        this.qty5Plus = qty5Plus;
    }
}
