package com.cs.oregano.model;

import java.io.Serializable;

/**
 * Created by cs android on 14-02-2017.
 */

public class Track_types implements Serializable {


    String addl_name, addl_name_ar, addl_price;

    public String getAddl_name() {
        return addl_name;
    }

    public void setAddl_name(String addl_name) {
        this.addl_name = addl_name;
    }

    public String getAddl_price() {
        return addl_price;
    }

    public void setAddl_price(String addl_price) {
        this.addl_price = addl_price;
    }

    public String getAddl_name_ar() {
        return addl_name_ar;
    }

    public void setAddl_name_ar(String addl_name_ar) {
        this.addl_name_ar = addl_name_ar;
    }
}
