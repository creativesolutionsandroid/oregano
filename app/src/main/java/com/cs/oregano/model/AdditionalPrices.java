package com.cs.oregano.model;

/**
 * Created by CS on 12-06-2016.
 */
public class AdditionalPrices {

    String itemType, price;

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
