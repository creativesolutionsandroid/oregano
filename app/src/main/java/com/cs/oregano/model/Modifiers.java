package com.cs.oregano.model;

import java.util.ArrayList;

/**
 * Created by SKT on 31-01-2016.
 */
public class Modifiers {
    String modifierId, modifierName, modifierNameAr, images;
    ArrayList<Additionals> childItems;

    public ArrayList<Additionals> getChildItems() {
        return childItems;
    }

    public void setChildItems(ArrayList<Additionals> childItems) {
        this.childItems = childItems;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public String getModifierName() {
        return modifierName;
    }

    public void setModifierName(String modifierName) {
        this.modifierName = modifierName;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getModifierNameAr() {
        return modifierNameAr;
    }

    public void setModifierNameAr(String modifierNameAr) {
        this.modifierNameAr = modifierNameAr;
    }
}
