package com.cs.oregano.model;

/**
 * Created by brahmam on 19/2/16.
 */
public class FavouriteOrder {

    String orderId, favoriteName, storeId, orderDate, storeName, storeName_ar, totalPrice, orderDate1, itemDetails, itemDetails_ar;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getFavoriteName() {
        return favoriteName;
    }

    public void setFavoriteName(String favoriteName) {
        this.favoriteName = favoriteName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreName_ar() {
        return storeName_ar;
    }

    public void setStoreName_ar(String storeName_ar) {
        this.storeName_ar = storeName_ar;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOrderDate1() {
        return orderDate1;
    }

    public void setOrderDate1(String orderDate1) {
        this.orderDate1 = orderDate1;
    }

    public String getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(String itemDetails) {
        this.itemDetails = itemDetails;
    }

    public String getItemDetails_ar() {
        return itemDetails_ar;
    }

    public void setItemDetails_ar(String itemDetails_ar) {
        this.itemDetails_ar = itemDetails_ar;
    }
}
