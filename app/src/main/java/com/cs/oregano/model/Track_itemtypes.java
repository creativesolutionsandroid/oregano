package com.cs.oregano.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by cs android on 14-02-2017.
 */

public class Track_itemtypes implements Serializable {

    String item_name;
    String price;
    String item_name_ar;
    String quantity;
    ArrayList<Track_types> itemtypes;

    public ArrayList<Track_types> getItemtypes() {
        return itemtypes;
    }

    public void setItemtypes(ArrayList<Track_types> itemtypes) {
        this.itemtypes = itemtypes;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_name_ar() {
        return item_name_ar;
    }

    public void setItem_name_ar(String item_name_ar) {
        this.item_name_ar = item_name_ar;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
