package com.cs.oregano.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by cs android on 14-02-2017.
 */

public class TrackItems implements Serializable {

    ArrayList<Track_itemtypes> items;

    public ArrayList<Track_itemtypes> getItems() {
        return items;
    }

    public void setItems(ArrayList<Track_itemtypes> items) {
        this.items = items;
    }


}