package com.cs.oregano.model;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by CS on 09-06-2016.
 */
public class ItemPrices implements Serializable {

    String price;
    int itemTypeId;

    public int getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(int itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public static Comparator<ItemPrices> priceSort = new Comparator<ItemPrices>() {

        public int compare(ItemPrices s1, ItemPrices s2) {

            int rollno1 = s1.getItemTypeId();
            int rollno2 = s2.getItemTypeId();

	   /*For ascending order*/
            return rollno1-rollno2;

	   /*For descending order*/
            //rollno2-rollno1;
        }};
}
