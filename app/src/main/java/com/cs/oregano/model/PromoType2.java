package com.cs.oregano.model;

/**
 * Created by CS on 30-06-2016.
 */
public class PromoType2 {

    String ItemId, ItemPrice, Qty, Size, Comments, AdditionalID, AdditionalPrice;

    public String getItemId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }

    public String getItemPrice() {
        return ItemPrice;
    }

    public void setItemPrice(String itemPrice) {
        ItemPrice = itemPrice;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public String getAdditionalID() {
        return AdditionalID;
    }

    public void setAdditionalID(String additionalID) {
        AdditionalID = additionalID;
    }

    public String getAdditionalPrice() {
        return AdditionalPrice;
    }

    public void setAdditionalPrice(String additionalPrice) {
        AdditionalPrice = additionalPrice;
    }
}
