package com.cs.oregano.model;

import java.util.ArrayList;

/**
 * Created by SKT on 21-02-2016.
 */
public class OrderDetailsTrack {

    String itemName, itemNameAr, itemTypeId, additionalName, additionalNameAr, price, additionalPrice, tottalAmount, qty;

    ArrayList<Additionals> additionalList;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNameAr() {
        return itemNameAr;
    }

    public void setItemNameAr(String itemNameAr) {
        this.itemNameAr = itemNameAr;
    }

    public String getAdditionalName() {
        return additionalName;
    }

    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }


    public String getAdditionalNameAr() {
        return additionalNameAr;
    }

    public void setAdditionalNameAr(String additionalNameAr) {
        this.additionalNameAr = additionalNameAr;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdditionalPrice() {
        return additionalPrice;
    }

    public void setAdditionalPrice(String additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public String getTottalAmount() {
        return tottalAmount;
    }

    public void setTottalAmount(String tottalAmount) {
        this.tottalAmount = tottalAmount;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }


    public String getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(String itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public ArrayList<Additionals> getAdditionalList() {
        return additionalList;
    }

    public void setAdditionalList(ArrayList<Additionals> additionalList) {
        this.additionalList = additionalList;
    }
}
