package com.cs.oregano.model;

import java.util.ArrayList;

/**
 * Created by CS on 09-06-2016.
 */
public class Items {
    String itemId, itemName, itemDesc, itemName_ar, itemDesc_ar, additionalsId, modifierId, images, categoryId, isDelivery;
    ArrayList<ItemPrices> itemPrices;


    public ArrayList<ItemPrices> getItemPrices() {
        return itemPrices;
    }

    public void setItemPrices(ArrayList<ItemPrices> itemPrices) {
        this.itemPrices = itemPrices;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getItemName_ar() {
        return itemName_ar;
    }

    public void setItemName_ar(String itemName_ar) {
        this.itemName_ar = itemName_ar;
    }

    public String getItemDesc_ar() {
        return itemDesc_ar;
    }

    public void setItemDesc_ar(String itemDesc_ar) {
        this.itemDesc_ar = itemDesc_ar;
    }

    public String getAdditionalsId() {
        return additionalsId;
    }

    public void setAdditionalsId(String additionalsId) {
        this.additionalsId = additionalsId;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getIsDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(String isDelivery) {
        this.isDelivery = isDelivery;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

}
