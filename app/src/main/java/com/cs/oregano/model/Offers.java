package com.cs.oregano.model;

import java.util.ArrayList;

/**
 * Created by CS on 08-08-2016.
 */
public class Offers {
    String offerDesc, offerDescAr, offerTitle, offerTitleAr, offerImage;
    Boolean isItemAvailble;
    ArrayList<Items> itemsList = new ArrayList<>();

    public Boolean getItemAvailble() {
        return isItemAvailble;
    }

    public void setItemAvailble(Boolean itemAvailble) {
        isItemAvailble = itemAvailble;
    }

    public ArrayList<Items> getItemsList() {
        return itemsList;
    }

    public void setItemsList(ArrayList<Items> itemsList) {
        this.itemsList = itemsList;
    }

    public String getOfferDesc() {
        return offerDesc;
    }

    public void setOfferDesc(String offerDesc) {
        this.offerDesc = offerDesc;
    }

    public String getOfferDescAr() {
        return offerDescAr;
    }

    public void setOfferDescAr(String offerDescAr) {
        this.offerDescAr = offerDescAr;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getOfferTitleAr() {
        return offerTitleAr;
    }

    public void setOfferTitleAr(String offerTitleAr) {
        this.offerTitleAr = offerTitleAr;
    }

    public String getOfferImage() {
        return offerImage;
    }

    public void setOfferImage(String offerImage) {
        this.offerImage = offerImage;
    }
}
