package com.cs.oregano.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.oregano.R;
import com.cs.oregano.model.Address;

import java.util.ArrayList;

/**
 * Created by CS on 20-06-2016.
 */
public class AddressAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Address> addressList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;

    public AddressAdapter(Context context, ArrayList<Address> addressList, String language) {
        this.context = context;
        this.addressList = addressList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return addressList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView flatNo, landmark, address, addressName;
        ImageView addressType;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.address_list_item, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.address_list_item_arabic, null);
            }


            holder.flatNo = (TextView) convertView
                    .findViewById(R.id.flat_no);
//            holder.storeName = (TextView) convertView
//                    .findViewById(R.id.store_name);
            holder.landmark = (TextView) convertView
                    .findViewById(R.id.landmark);
            holder.address = (TextView) convertView
                    .findViewById(R.id.address);
            holder.addressName = (TextView) convertView.findViewById(R.id.address_name);
            holder.addressType = (ImageView) convertView.findViewById(R.id.address_type);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(language.equalsIgnoreCase("En")) {
            if(!addressList.get(position).getHouseNo().equals("null")) {
                holder.flatNo.setText("Flat No : " + addressList.get(position).getHouseNo());
            }
            else{
                holder.flatNo.setText("Flat No : --");
            }

            if(!addressList.get(position).getLandmark().equals("null")) {
                holder.landmark.setText("LandMark : " + addressList.get(position).getLandmark());
            }
            else{
                holder.landmark.setText("LandMark : --");
            }
            if(!addressList.get(position).getAddress().equals("null")) {
                holder.address.setText("Address : " + addressList.get(position).getAddress());
            }
            else{
                holder.address.setText("Address : --");
            }
            if(!addressList.get(position).getAddressName().equals("null")) {
                holder.addressName.setText("" + addressList.get(position).getAddressName());
            }
            else{
                holder.addressName.setText("--");
            }
        }else{

            if(!addressList.get(position).getHouseNo().equals("null")) {
                holder.flatNo.setText(""+addressList.get(position).getHouseNo());
            }
            else{
                holder.flatNo.setText("--");
            }

            if(!addressList.get(position).getLandmark().equals("null")) {
                holder.landmark.setText(""+addressList.get(position).getLandmark());
            }
            else{
                holder.landmark.setText("--");
            }
            if(!addressList.get(position).getAddress().equals("null")) {
                holder.address.setText(""+addressList.get(position).getAddress());
            }
            else{
                holder.address.setText("--");
            }
            if(!addressList.get(position).getAddressName().equals("null")) {
                holder.addressName.setText("" + addressList.get(position).getAddressName());
            }
            else{
                holder.addressName.setText("--");
            }

        }

        if(addressList.get(position).getAddressType().equalsIgnoreCase("1")){
            holder.addressType.setImageResource(R.drawable.address_home);
        }else if(addressList.get(position).getAddressType().equalsIgnoreCase("2")){
            holder.addressType.setImageResource(R.drawable.address_work);
        }else{
            holder.addressType.setImageResource(R.drawable.address_other);
        }

        return convertView;
    }
}