package com.cs.oregano.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.oregano.R;
import com.cs.oregano.model.Promos;

import java.util.ArrayList;

/**
 * Created by CS on 30-06-2016.
 */
public class PromoAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Promos> promosList = new ArrayList<>();
    String language;
    public PromoAdapter(Context context, ArrayList<Promos> promosList) {
        this.context = context;
        this.promosList = promosList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        SharedPreferences languagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
    }

    public int getCount() {
        return promosList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView offerName, offerDesc;
        ImageView promo_banner;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.promo_list_item, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.promo_list_item_arabic, null);
            }

            holder.offerName = (TextView) convertView
                    .findViewById(R.id.offer_title);
            holder.offerDesc = (TextView) convertView
                    .findViewById(R.id.offer_desc);
            holder.promo_banner = (ImageView) convertView.findViewById(R.id.promo_banner);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        Glide.with(context).load("http://www.ircfood.com/images/"+ promosList.get(position).getImage()).into(holder.promo_banner);
        if(language.equalsIgnoreCase("En")) {
//            if(promosList.get(position).getPromoType().equals("1")){
                holder.offerDesc.setText(promosList.get(position).getDescription());
//            }else if(promosList.get(position).getPromoType().equals("2")){
//                holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//            }else if(promosList.get(position).getPromoType().equals("3")){
//                holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//            }else if(promosList.get(position).getPromoType().equals("4")){
//                holder.offerDesc.setText(promosList.get(position).getDescription());
//            }else if(promosList.get(position).getPromoType().equals("5")){
//                holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//            }else if(promosList.get(position).getPromoType().equals("6")){
//                if(!promosList.get(position).getRemainingBonus().equals("0")) {
//                    holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//                }else{
//                    holder.offerDesc.setText("Free Pizza");
//                }
//            }
            holder.offerName.setText(promosList.get(position).getPromoTitle());
        }else if(language.equalsIgnoreCase("Ar")){
//            if(promosList.get(position).getPromoType().equals("1")){
                holder.offerDesc.setText(promosList.get(position).getDescriptionAr());
//            }else if(promosList.get(position).getPromoType().equals("2")){
//                holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//            }else if(promosList.get(position).getPromoType().equals("3")){
//                holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//            }else if(promosList.get(position).getPromoType().equals("4")){
//                holder.offerDesc.setText(promosList.get(position).getDescriptionAr());
//            }else if(promosList.get(position).getPromoType().equals("5")){
//                holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//            }else if(promosList.get(position).getPromoType().equals("6")){
//                if(!promosList.get(position).getRemainingBonus().equals("0")) {
//                    holder.offerDesc.setText(promosList.get(position).getRemainingBonus()+" SR");
//                }else{
//                    holder.offerDesc.setText("بيتزا مجانية");
//                }
//            }
            holder.offerName.setText(promosList.get(position).getPromoTitleAr());
        }



        return convertView;
    }

}
