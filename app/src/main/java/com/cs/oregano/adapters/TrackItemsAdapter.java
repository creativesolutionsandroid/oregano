package com.cs.oregano.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.oregano.R;
import com.cs.oregano.model.Track_itemtypes;
import com.cs.oregano.model.Track_types;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by cs android on 14-02-2017.
 */

public class TrackItemsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
//    ArrayList<TrackItems> orderList = new ArrayList<>();
    ArrayList<Track_itemtypes> items = new ArrayList<>();
    ArrayList<Track_types> Addl = new ArrayList<>();
    int qty, price;
    String language, size;
    int pos=0;

    public TrackItemsAdapter(Context context, ArrayList<Track_itemtypes> Track_itemtypes, String language) {
        this.context = context;
        this.items = Track_itemtypes;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }


    public int getCount() {
        Log.i("TEST","track items adapter size "+items.size());
        return items.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView itemName, itemPrice, itemNo;
        LinearLayout additionalsLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

//            items=(orderList.get(position).getItems());
            Addl=(items.get(position).getItemtypes());

            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.track_items_list, null);
            }
            else if(language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.track_items_list_arabic, null);
            }

            holder.itemName = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemPrice = (TextView) convertView.findViewById(R.id.total_amount);
            holder.itemNo = (TextView) convertView.findViewById(R.id.qty);
            holder.additionalsLayout = (LinearLayout) convertView.findViewById(R.id.additionals_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
//            for(int i=0; i<items.size();i++)
        if(language.equalsIgnoreCase("En")) {
            holder.itemName.setText(items.get(position).getItem_name());
        }
        else if(language.equalsIgnoreCase("Ar")) {
            holder.itemName.setText(items.get(position).getItem_name_ar());
        }
        DecimalFormat dec =new DecimalFormat("0.00");
        holder.itemPrice.setText(dec.format(Float.parseFloat(items.get(position).getPrice())));
        holder.itemNo.setText(items.get(position).getQuantity());

        holder.additionalsLayout.removeAllViews();
        holder.additionalsLayout.setVisibility(View.VISIBLE);

        ArrayList<Track_types> itemTypes = new ArrayList<>();
        itemTypes = items.get(position).getItemtypes();


            for (int i = 0; i < itemTypes.size(); i++) {
                if(!itemTypes.get(i).getAddl_name().equals("null")) {
                    View v = null;
                    if(language.equalsIgnoreCase("En")) {
                        v = inflater.inflate(R.layout.confirmation_addl_list, null);
                    }
                    else if(language.equalsIgnoreCase("Ar")){
                        v = inflater.inflate(R.layout.confirmation_addl_list_arabic, null);
                    }
                TextView addtionalsName = (TextView) v.findViewById(R.id.addl_item_name);
                TextView addtionalsPrice = (TextView) v.findViewById(R.id.addl_amount);
                    if(language.equalsIgnoreCase("En")) {
                        addtionalsName.setText(itemTypes.get(i).getAddl_name());
                    }
                    else if(language.equalsIgnoreCase("Ar")) {
                        addtionalsName.setText(itemTypes.get(i).getAddl_name_ar());
                    }
                addtionalsPrice.setText(dec.format(Float.parseFloat(itemTypes.get(i).getAddl_price())));

                holder.additionalsLayout.addView(v);
            }
        }

//        if(position == 0){
//            pos=0;
//        }
//            pos = pos+1;
//            if (position < 9) {
//                holder.itemNo.setText("0" + pos);
//                TrackOrderSteps.totalQty.setText("0" + pos);
//            } else {
//                holder.itemNo.setText("" + pos);
//                TrackOrderSteps.totalQty.setText("" + pos);
//        }
        return convertView;
    }

}
