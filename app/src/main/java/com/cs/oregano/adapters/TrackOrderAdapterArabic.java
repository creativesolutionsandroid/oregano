package com.cs.oregano.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.oregano.R;
import com.cs.oregano.model.OrderDetailsTrack;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by SKT on 21-02-2016.
 */
public class TrackOrderAdapterArabic extends BaseAdapter {


    public Context context;
    public LayoutInflater inflater;
    ArrayList<OrderDetailsTrack> newsList = new ArrayList<>();

    DecimalFormat dec =new DecimalFormat("0.00");

    public TrackOrderAdapterArabic(Context context, ArrayList<OrderDetailsTrack> newsList) {
        this.context = context;
        this.newsList = newsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return newsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView itemName, price, qty, totalAmt, additionalName, additionalPrice;
        LinearLayout additionalsLayout;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.track_order_listitem_arabic, null);


            holder.itemName = (TextView) convertView
                    .findViewById(R.id.item_name);
            holder.price = (TextView) convertView.
                    findViewById(R.id.price);
            holder.qty = (TextView) convertView.
                    findViewById(R.id.qty);
            holder.totalAmt = (TextView) convertView.
                    findViewById(R.id.total_amount);
            holder.additionalsLayout = (LinearLayout) convertView.findViewById(R.id.additionals_layout);
//            holder.additionalPrice = (TextView) convertView.findViewById(R.id.additional_price);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }



        int sno = position+1;

//        holder.additionalName.setText(newsList.get(position).getAdditionalNameAr());
//        holder.additionalPrice.setText(newsList.get(position).getAdditionalPrice());

        String[] parts = newsList.get(position).getAdditionalNameAr().split(",");
        String[] parts1 = newsList.get(position).getAdditionalPrice().split(",");
        holder.additionalsLayout.removeAllViews();
        for(int i=0; i< parts.length;i++){
            View v = inflater.inflate(R.layout.track_order_additionals_arabic, null);
            TextView addtionalsName = (TextView) v.findViewById(R.id.additional_name);
            TextView addtionalsPrice = (TextView) v.findViewById(R.id.additional_price);
            addtionalsName.setText(parts[i]);
            addtionalsPrice.setText(dec.format(Float.parseFloat(parts1[i])));
            holder.additionalsLayout.addView(v);
        }

        holder.price.setText(dec.format(Float.parseFloat(newsList.get(position).getPrice())));
        holder.qty.setText(newsList.get(position).getQty());
        holder.totalAmt.setText(dec.format(Float.parseFloat(newsList.get(position).getTottalAmount())));
        if(newsList.get(position).getItemTypeId().equals("1")){
            holder.itemName.setText(newsList.get(position).getItemNameAr()+" (صغير)");
        }else if(newsList.get(position).getItemTypeId().equals("3")){
            holder.itemName.setText(newsList.get(position).getItemNameAr()+" (عادي)");
        }else if(newsList.get(position).getItemTypeId().equals("2")){
            holder.itemName.setText(newsList.get(position).getItemNameAr()+" (كبير)");
        }else if(newsList.get(position).getItemTypeId().equals("6")){
            holder.itemName.setText(newsList.get(position).getItemNameAr()+"(جامبو) ");
        } else if (newsList.get(position).getItemTypeId().equals("7")) {
            holder.itemName.setText(newsList.get(position).getItemNameAr()+"(مفرد) ");
        } else if (newsList.get(position).getItemTypeId().equals("8")) {
            holder.itemName.setText(newsList.get(position).getItemNameAr()+" (مضاعف) ");
        } else{
            holder.itemName.setText(newsList.get(position).getItemNameAr());
        }

        return convertView;
    }
}
