package com.cs.oregano.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.oregano.Constants;
import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.MainActivity;
import com.cs.oregano.R;
import com.cs.oregano.activities.CategoryItems;
import com.cs.oregano.activities.CheckoutActivity;
import com.cs.oregano.activities.CommentsActivity;
import com.cs.oregano.fragments.OrderFragment;
import com.cs.oregano.model.Order;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class CheckoutAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    DecimalFormat decim = new DecimalFormat("0.00");
    int qty;
    float price;
    String language;
    float vat=5;

    public CheckoutAdapter(Context context, ArrayList<Order> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title, price, qty, qty1, itemDesc;
        ImageView itemType, plusBtn, minusBtn, itemComment, itemIcon;
        ImageView non_delivery;
        LinearLayout additionalsLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.checkout_row, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
            }

            holder.title = (TextView) convertView.findViewById(R.id.item_name);
            holder.price = (TextView) convertView.findViewById(R.id.item_price);
            holder.qty = (TextView) convertView.findViewById(R.id.item_qty);
            holder.qty1 = (TextView) convertView.findViewById(R.id.item_qty1);
            holder.plusBtn = (ImageView) convertView.findViewById(R.id.plus_btn);
            holder.minusBtn = (ImageView) convertView.findViewById(R.id.minus_btn);

            holder.itemComment = (ImageView) convertView.findViewById(R.id.edit_comment);
            holder.itemType = (ImageView) convertView.findViewById(R.id.item_type);
            holder.itemDesc = (TextView) convertView.findViewById(R.id.item_desc);
            holder.additionalsLayout = (LinearLayout) convertView.findViewById(R.id.additionals_layout);
            holder.itemIcon = (ImageView) convertView.findViewById(R.id.item_image);
            holder.non_delivery = (ImageView) convertView.findViewById(R.id.non_delivery);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        if(orderList.get(position).getAdditionalName().equals("")){
//            holder.additionalsName.setVisibility(View.GONE);
//        }else {
//            holder.additionalsName.setVisibility(View.VISIBLE);
//            holder.additionalsName.setText(orderList.get(position).getAdditionalName());
//        }

        if(orderList.get(position).getNonDelivery().equals("true")){
            holder.non_delivery.setVisibility(View.INVISIBLE);
        }
        else{
            holder.non_delivery.setVisibility(View.VISIBLE);
        }

        if(language.equalsIgnoreCase("En")){
            String[] parts = orderList.get(position).getAdditionalName().split(",");
            String[] parts1 = orderList.get(position).getAdditionalPrice().split(",");

            Log.d("TAG", "parts: "+orderList.get(position).getAdditionalName());
            Log.d("TAG", "parts1: "+orderList.get(position).getAdditionalPrice());
            holder.additionalsLayout.removeAllViews();
            for(int i=0; i< parts.length;i++){

                if(!parts[i].equalsIgnoreCase("")) {
                    View v = inflater.inflate(R.layout.checkout_additionals, null);
                    TextView addtionalsName = (TextView) v.findViewById(R.id.additionals_name);
                    TextView addtionalsPrice = (TextView) v.findViewById(R.id.additional_price);
                    addtionalsName.setText(parts[i]);
                    float priceTxt = Float.parseFloat(Constants.convertToArabic(parts1[i])) * Integer.parseInt(orderList.get(position).getQty());
                    addtionalsPrice.setText("" + decim.format(priceTxt));
                    holder.additionalsLayout.addView(v);
                }
            }
        }else if(language.equalsIgnoreCase("Ar")){
            String[] parts = orderList.get(position).getAdditionalNameAr().split(",");
            String[] parts1 =orderList.get(position).getAdditionalPrice().split(",");

            holder.additionalsLayout.removeAllViews();
            for(int i=0; i< parts.length;i++){
                if(!parts[i].equalsIgnoreCase("")) {
                    View v = inflater.inflate(R.layout.checkout_additionals_arabic, null);
                    TextView addtionalsName = (TextView) v.findViewById(R.id.additionals_name);
                    TextView addtionalsPrice = (TextView) v.findViewById(R.id.additional_price);
                    addtionalsName.setText(parts[i]);
                    float priceTxt = Float.parseFloat(Constants.convertToArabic(parts1[i])) * Integer.parseInt(orderList.get(position).getQty());
                    addtionalsPrice.setText("" + decim.format(priceTxt));
                    holder.additionalsLayout.addView(v);
                }
            }
        }



        if(orderList.get(position).getSize().equalsIgnoreCase("1")){
            holder.itemType.setImageResource(R.drawable.small_item);
        }else if(orderList.get(position).getSize().equalsIgnoreCase("2")){
            holder.itemType.setImageResource(R.drawable.medium_item);
        }else if(orderList.get(position).getSize().equalsIgnoreCase("3")){
            holder.itemType.setImageResource(R.drawable.large_item);
        }else if(orderList.get(position).getSize().equalsIgnoreCase("4")){
            holder.itemType.setImageResource(R.drawable.extra_large_item);
        }else if(orderList.get(position).getSize().equalsIgnoreCase("5")){
            holder.itemType.setImageResource(R.drawable.pcs4_item);
        }else if(orderList.get(position).getSize().equalsIgnoreCase("6")){
            holder.itemType.setImageResource(R.drawable.pcs8_item);
        } else{
            holder.itemType.setVisibility(View.INVISIBLE);
        }

        if(language.equalsIgnoreCase("En")){
            holder.itemDesc.setText(orderList.get(position).getDescription());
            holder.title.setText(orderList.get(position).getItemName());
        }else if(language.equalsIgnoreCase("Ar")){
            holder.itemDesc.setText(orderList.get(position).getDescriptionAr());
            holder.title.setText(orderList.get(position).getItemNameAr());
        }


        holder.qty.setText(orderList.get(position).getQty());
        holder.qty1.setText(orderList.get(position).getQty());
        float priceTxt = Float.parseFloat(orderList.get(position).getItemPrice())* Integer.parseInt(orderList.get(position).getQty());
        holder.price.setText(""+decim.format(priceTxt));
        orderList = myDbHelper.getOrderInfo();
        Glide.with(context).load("http://www.ircfood.com/images/"+orderList.get(position).getItemImage()).into(holder.itemIcon);

		/*ArrayList<HashMap<String, String>> List = SchoolAlertMain.DBcontroller.getAllAlerts();
		//System.out.println("adapter"+alertsArrayList.get(position).getTime());
		if(List.contains(alertsArrayList.get(position).getAlertId())){
			holder.alertList.setBackgroundColor(Color.GRAY);
		}*/

        Log.e("TAG","total amount "+(Float.parseFloat(orderList.get(position).getTotalAmount())));
        Log.e("TAG","Item price ad "+(Float.parseFloat(orderList.get(position).getItemPriceAd())));
        holder.plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qty = Integer.parseInt(orderList.get(position).getQty()) + 1;

                float totalamount = (Float.parseFloat(orderList.get(position).getTotalAmount()));
                float itemprice = (Float.parseFloat(orderList.get(position).getItemPriceAd()));

                Log.e("TAG","total amount "+totalamount);
                Log.e("TAG","Item price ad "+itemprice);
                price = (totalamount) + (itemprice);

                Log.e("TAG","price " +price);
                myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getOrderId());
                orderList = myDbHelper.getOrderInfo();
                notifyDataSetChanged();
                try {
                    OrderFragment.orderPrice.setText("" +decim.format( myDbHelper.getTotalOrderPrice()) + " SR");
                    OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    CategoryItems.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()) + " SR");
                    CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    CategoryItems.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                DecimalFormat decim = new DecimalFormat("0.00");
                CheckoutActivity.amount.setText(""+decim.format(myDbHelper.getTotalOrderPrice()));
                float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                CheckoutActivity.vatAmount.setText(""+decim.format(tax));
                CheckoutActivity.netTotal.setText(""+decim.format(myDbHelper.getTotalOrderPrice()+tax));
                CheckoutActivity.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
                CheckoutActivity.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
                CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                CheckoutActivity.clearOrder.setText(""+myDbHelper.getTotalOrderQty());

            }
        });

        holder.minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qty = Integer.parseInt(orderList.get(position).getQty()) - 1;
                if(qty==0){
                    myDbHelper.deleteItemFromOrder(orderList.get(position).getOrderId());
                }else {
                    price = (Float.parseFloat(orderList.get(position).getTotalAmount())) - (Float.parseFloat(orderList.get(position).getItemPriceAd()));
                    myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getOrderId());
                }


                orderList = myDbHelper.getOrderInfo();
                if(myDbHelper.getTotalOrderQty()==0){
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.putExtra("startWith",2);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);

                }

                try {
                    OrderFragment.orderPrice.setText("" +decim.format( myDbHelper.getTotalOrderPrice()) + " SR");
                    OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    CategoryItems.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()) + " SR");
                    CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                    CategoryItems.cartCount.setText("" + myDbHelper.getTotalOrderQty());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                double number;
                number=myDbHelper.getTotalOrderPrice();
                DecimalFormat decim = new DecimalFormat("0.00");
                CheckoutActivity.amount.setText(""+decim.format(myDbHelper.getTotalOrderPrice()));
                float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                CheckoutActivity.vatAmount.setText(""+decim.format(tax));
                CheckoutActivity.netTotal.setText(""+decim.format(myDbHelper.getTotalOrderPrice()+tax));
                CheckoutActivity.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
                CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                CheckoutActivity.clearOrder.setText(""+myDbHelper.getTotalOrderQty());
                notifyDataSetChanged();
            }
        });

        holder.itemComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderList = myDbHelper.getOrderInfo();
                Intent intent = new Intent(context, CommentsActivity.class);
                if(language.equalsIgnoreCase("En")){
                    intent.putExtra("title", orderList.get(position).getItemName());
                }else if(language.equalsIgnoreCase("Ar")){
                    intent.putExtra("title", orderList.get(position).getItemNameAr());
                }

                intent.putExtra("itemImage", orderList.get(position).getItemImage());
                intent.putExtra("itemId", orderList.get(position).getItemId());
                intent.putExtra("orderId", orderList.get(position).getOrderId());
                intent.putExtra("comment", orderList.get(position).getComment());
                intent.putExtra("screen", "checkout");
                context.startActivity(intent);
            }
        });

        return convertView;
    }
}