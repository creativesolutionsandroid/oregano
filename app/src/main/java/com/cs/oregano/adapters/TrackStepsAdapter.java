package com.cs.oregano.adapters;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.R;
import com.cs.oregano.model.TrackSteps;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 20-06-2016.
 */
public class TrackStepsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<TrackSteps> arrayList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    int qty, price;
    String language;
    ViewHolder holder;
    AnimationDrawable animation;

    public TrackStepsAdapter(Context context, ArrayList<TrackSteps> arrayList, String language) {
        this.context = context;
        this.arrayList = arrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return arrayList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView status, orderNumber, expectTime, expectTimeTxt, trackTime, orderNumberTxt;
        ImageView statusImage;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            holder = new ViewHolder();

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.track_steps_item, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.track_steps_item_arabic, null);
            }


            holder.status = (TextView) convertView.findViewById(R.id.order_status);
            holder.orderNumber = (TextView) convertView.findViewById(R.id.order_number);
            holder.expectTime = (TextView) convertView.findViewById(R.id.expected_time);

            holder.trackTime = (TextView) convertView.findViewById(R.id.track_time);
            holder.expectTimeTxt = (TextView) convertView.findViewById(R.id.expected_time_txt);
            holder.orderNumberTxt = (TextView) convertView.findViewById(R.id.order_number_txt);
            holder.statusImage = (ImageView) convertView.findViewById(R.id.track_status_image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        String date;
        if (!arrayList.get(position).getTrackTime().equals("")) {
            Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
            try {
                dateObj = curFormater.parse(arrayList.get(position).getTrackTime());
            } catch (ParseException e) {
                e.printStackTrace();
                try {
                    dateObj = curFormater1.parse(arrayList.get(position).getTrackTime());
                } catch (ParseException pe) {

                }

            }
            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
            date = postFormater.format(dateObj);
        } else {
            date = "";
        }
        if (arrayList.get(position).getStatus().equalsIgnoreCase("Order!")) {
            if (language.equalsIgnoreCase("En")) {
                holder.status.setText(arrayList.get(position).getStatus());
                holder.orderNumber.setText("order number");
                holder.expectTime.setText("expected time");
            } else if (language.equalsIgnoreCase("Ar")) {
                holder.status.setText("الطلب");
                holder.orderNumber.setText("رقم الطلب");
                holder.expectTime.setText("الوقت المتوقع");
            }
            final String[] order_no = arrayList.get(position).getOrderNumber().split("-");
            holder.orderNumberTxt.setText(order_no[1]);
            holder.trackTime.setVisibility(View.VISIBLE);
            holder.trackTime.setText(date);

            holder.expectTime.setVisibility(View.VISIBLE);
            holder.expectTimeTxt.setVisibility(View.VISIBLE);
            holder.orderNumberTxt.setVisibility(View.VISIBLE);
            Log.i("TAG", "esttime " + arrayList.get(position).getExpectedTime());
            SimpleDateFormat datetime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
            SimpleDateFormat datetime1 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
            SimpleDateFormat date2 = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat time2 = new SimpleDateFormat("hh:mm a");
            Date date3 = null, time3 = null;
            try {

                date3 = datetime1.parse(arrayList.get(position).getExpectedTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

//            try {
//
//                time3 = datetime.parse(arrayList.get(position).getExpectedTime());
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }

//            if (time3 == null){
//
//                String time1 = time2.format(date3);
//                String date1 = date2.format(date3);
//                holder.expectTimeTxt.setText(date1 + "\n" + time1);
//
//            }else if (date3 == null){
//
//                String time1 = time2.format(time3);
//                String date1 = date2.format(time3);
//                holder.expectTimeTxt.setText(date1 + "\n" + time1);
//
//            }else {
                holder.expectTimeTxt.setText(arrayList.get(position).getExpectedTime());
//            }

            holder.statusImage.setImageResource(R.drawable.track_received);
        }
        if (arrayList.get(position).getStatus().equalsIgnoreCase("Cancel!")) {
            if (language.equalsIgnoreCase("En")) {
                holder.status.setText(arrayList.get(position).getStatus());
                holder.orderNumber.setText("Your order has been cancelled");
            } else if (language.equalsIgnoreCase("Ar")) {
                holder.status.setText("الغاء");
                holder.orderNumber.setText("طلبك تم الغاءه");
            }

            holder.statusImage.setImageResource(R.drawable.track_cancel);
            holder.trackTime.setVisibility(View.GONE);
            holder.expectTime.setVisibility(View.GONE);
            holder.expectTimeTxt.setVisibility(View.GONE);
            holder.orderNumberTxt.setVisibility(View.GONE);
        } else if (arrayList.get(position).getStatus().equalsIgnoreCase("Confirmed!")) {
            if (language.equalsIgnoreCase("En")) {
                holder.status.setText(arrayList.get(position).getStatus());
                holder.orderNumber.setText("The restaurant has begun preparing your food");
            } else if (language.equalsIgnoreCase("Ar")) {
                holder.status.setText("لقد بدأنا بتحضير طلبك");
                holder.orderNumber.setText("لقد تم بالفعل تحضير طلبك");
            }
            if (date.equals("")) {
                holder.trackTime.setVisibility(View.GONE);
                holder.statusImage.setImageResource(R.drawable.track_confirmed_select);
            } else {
                holder.trackTime.setVisibility(View.VISIBLE);
                holder.trackTime.setText(date);
                holder.statusImage.setImageResource(R.drawable.track_confirmed);
            }

            holder.expectTime.setVisibility(View.GONE);
            holder.expectTimeTxt.setVisibility(View.GONE);
            holder.orderNumberTxt.setVisibility(View.GONE);
        } else if (arrayList.get(position).getStatus().equalsIgnoreCase("Ready!")) {
            if (language.equalsIgnoreCase("En")) {
                holder.status.setText(arrayList.get(position).getStatus());
                if (date.equals("")) {
                    holder.trackTime.setVisibility(View.GONE);
                    holder.statusImage.setImageResource(R.drawable.track_ready_select);
                    holder.orderNumber.setText("Your food is ready");
                } else {
                    holder.trackTime.setVisibility(View.VISIBLE);
                    holder.trackTime.setText(date);
                    holder.statusImage.setImageResource(R.drawable.track_ready);
                    holder.orderNumber.setText("Your food is ready at \n" + arrayList.get(position).getStoreName());
                }
            } else if (language.equalsIgnoreCase("Ar")) {
                holder.status.setText("طلبك الآن جاهز");
                if (date.equals("")) {
                    holder.trackTime.setVisibility(View.GONE);
                    holder.statusImage.setImageResource(R.drawable.track_ready_select);
                    holder.orderNumber.setText("طلبك جاهز في");
                } else {
                    holder.trackTime.setVisibility(View.VISIBLE);
                    holder.trackTime.setText(date);
                    holder.statusImage.setImageResource(R.drawable.track_ready);
                    holder.orderNumber.setText("طلبك جاهز في " + arrayList.get(position).getStoreNameAr());
                }
            }


            holder.expectTime.setVisibility(View.GONE);
            holder.expectTimeTxt.setVisibility(View.GONE);
            holder.orderNumberTxt.setVisibility(View.GONE);

        } else if (arrayList.get(position).getStatus().equalsIgnoreCase("On The Way!")) {
            if (language.equalsIgnoreCase("En")) {
                holder.status.setText(arrayList.get(position).getStatus());
                if (date.equals("")) {
                    holder.trackTime.setVisibility(View.GONE);
                    holder.statusImage.setImageResource(R.drawable.track_ontheway_select);
                    holder.orderNumber.setText("Your food is on the way to");
                } else {
                    startAnimation();
                    holder.trackTime.setVisibility(View.VISIBLE);
                    holder.trackTime.setText(date);
                    holder.statusImage.setImageResource(R.drawable.track_ontheway);
                    holder.orderNumber.setText("Your food is on the way to \n" + arrayList.get(position).getAddress());
//                    holder.statusImage.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            try{
//                                if(!arrayList.get(position+1).getStatus().equalsIgnoreCase("Delivered!")){
//                                    Intent i = new Intent(context, LiveTrackingActivity.class);
//                                    i.putExtra("driver_name", TrackOrderSteps.driverName);
//                                    i.putExtra("driver_number", TrackOrderSteps.driverNumber);
//                                    i.putExtra("driver_id", TrackOrderSteps.driverId);
//                                    i.putExtra("order_id", arrayList.get(position).getOrderId());
//                                    i.putExtra("exp_time", arrayList.get(position).getExpectedTime());
//                                    context.startActivity(i);
//                                }
//                            }catch (Exception e){
//                                Intent i = new Intent(context, LiveTrackingActivity.class);
//                                i.putExtra("driver_name", TrackOrderSteps.driverName);
//                                i.putExtra("driver_number", TrackOrderSteps.driverNumber);
//                                i.putExtra("driver_id", TrackOrderSteps.driverId);
//                                i.putExtra("order_id", arrayList.get(position).getOrderId());
//                                i.putExtra("exp_time", arrayList.get(position).getExpectedTime());
//                                context.startActivity(i);
//                            }
//
//                        }
//                    });
                }
            } else if (language.equalsIgnoreCase("Ar")) {
                holder.status.setText("في الطريق");
                if (date.equals("")) {
                    holder.trackTime.setVisibility(View.GONE);
                    holder.statusImage.setImageResource(R.drawable.track_ontheway_select_arabic);
                    holder.orderNumber.setText("طلبك الآن في الطريق إلى");
                } else {
                    holder.trackTime.setVisibility(View.VISIBLE);
                    holder.trackTime.setText(date);
                    holder.statusImage.setImageResource(R.drawable.track_ontheway_arabic);
                    holder.orderNumber.setText("طلبك الآن في الطريق إلى \n" + arrayList.get(position).getAddress());
//                    holder.statusImage.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            try{
//                                if(!arrayList.get(position+1).getStatus().equalsIgnoreCase("Delivered!")){
//                                    Intent i = new Intent(context, LiveTrackingActivity.class);
//                                    i.putExtra("driver_name", TrackOrderSteps.driverName);
//                                    i.putExtra("driver_number", TrackOrderSteps.driverNumber);
//                                    i.putExtra("driver_id", TrackOrderSteps.driverId);
//                                    i.putExtra("order_id", arrayList.get(position).getOrderId());
//                                    i.putExtra("exp_time", arrayList.get(position).getExpectedTime());
//                                    context.startActivity(i);
//                                }
//                            }catch (Exception e){
//                                Intent i = new Intent(context, LiveTrackingActivity.class);
//                                i.putExtra("driver_name", TrackOrderSteps.driverName);
//                                i.putExtra("driver_number", TrackOrderSteps.driverNumber);
//                                i.putExtra("driver_id", TrackOrderSteps.driverId);
//                                i.putExtra("order_id", arrayList.get(position).getOrderId());
//                                i.putExtra("exp_time", arrayList.get(position).getExpectedTime());
//                                context.startActivity(i);
//                            }
//
//                        }
//                    });
                }
            }


            holder.expectTime.setVisibility(View.GONE);
            holder.expectTimeTxt.setVisibility(View.GONE);
            holder.orderNumberTxt.setVisibility(View.GONE);
        } else if (arrayList.get(position).getStatus().equalsIgnoreCase("Served!")) {
            if (language.equalsIgnoreCase("En")) {
                holder.status.setText(arrayList.get(position).getStatus());
                holder.orderNumber.setText("Your food has been Served");
            } else if (language.equalsIgnoreCase("Ar")) {
                holder.status.setText("تم استلامه");
                holder.orderNumber.setText("تم تقديم طلبك ");
            }

            if (date.equals("")) {
                holder.trackTime.setVisibility(View.GONE);
                holder.statusImage.setImageResource(R.drawable.track_delivered_select);
            } else {
                holder.trackTime.setVisibility(View.VISIBLE);
                holder.trackTime.setText(date);
                holder.statusImage.setImageResource(R.drawable.track_delivered);
            }

            holder.expectTime.setVisibility(View.GONE);
            holder.expectTimeTxt.setVisibility(View.GONE);
            holder.orderNumberTxt.setVisibility(View.GONE);
        } else if (arrayList.get(position).getStatus().equalsIgnoreCase("Delivered!")) {
            if (language.equalsIgnoreCase("En")) {
                holder.status.setText(arrayList.get(position).getStatus());
                holder.orderNumber.setText("Your food has been delivered");
            } else if (language.equalsIgnoreCase("Ar")) {
                holder.status.setText("تم التوصيل");
                holder.orderNumber.setText("تم توصيل طلبك");
            }

            if (date.equals("")) {
                holder.trackTime.setVisibility(View.GONE);
                holder.statusImage.setImageResource(R.drawable.track_delivered_select);
            } else {
                holder.trackTime.setVisibility(View.VISIBLE);
                holder.trackTime.setText(date);
                holder.statusImage.setImageResource(R.drawable.track_delivered);
            }

            holder.expectTime.setVisibility(View.GONE);
            holder.expectTimeTxt.setVisibility(View.GONE);
            holder.orderNumberTxt.setVisibility(View.GONE);
        }

        return convertView;
    }

    class Starter implements Runnable {
        public void run() {
            animation.start();
        }
    }

    private void startAnimation() {
        animation = new AnimationDrawable();

        animation.addFrame(context.getResources().getDrawable(R.drawable.bike1), 200);
        animation.addFrame(context.getResources().getDrawable(R.drawable.bike2), 200);
        animation.addFrame(context.getResources().getDrawable(R.drawable.bike3), 200);
        animation.addFrame(context.getResources().getDrawable(R.drawable.bike4), 200);
        animation.addFrame(context.getResources().getDrawable(R.drawable.bike5), 200);


        animation.setOneShot(false);


        holder.statusImage.setImageDrawable(animation);

        holder.statusImage.post(new Starter());
    }

    private void startAnimationArabic() {
        animation = new AnimationDrawable();

        animation.addFrame(context.getResources().getDrawable(R.drawable.bike1_arabic), 200);
        animation.addFrame(context.getResources().getDrawable(R.drawable.bike2_arabic), 200);
        animation.addFrame(context.getResources().getDrawable(R.drawable.bike3_arabic), 200);
        animation.addFrame(context.getResources().getDrawable(R.drawable.bike4_arabic), 200);
        animation.addFrame(context.getResources().getDrawable(R.drawable.bike5_arabic), 200);


        animation.setOneShot(false);


        holder.statusImage.setImageDrawable(animation);

        holder.statusImage.post(new Starter());
    }

}
