package com.cs.oregano.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.cs.oregano.Constants;
import com.cs.oregano.R;
import com.cs.oregano.model.Banner;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class BannersAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    int[] mResources = {
            R.drawable.main_banner1,
    };

    boolean offerBanner;
    ArrayList<Banner> bannerList;


    public BannersAdapter(Context context, ArrayList<Banner> bannerList) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.offerBanner = false;
        this.bannerList = bannerList;
    }

    @Override
    public int getCount() {
        if(bannerList.size()>0){
            return bannerList.size();
        }else {
            return mResources.length;
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.banner_image, container, false);

        CircleImageView imageView = (CircleImageView) itemView.findViewById(R.id.bannerImage);
        if (bannerList.size()>0) {
            Glide.with(mContext).load(Constants.IMAGE_URL+ bannerList.get(position).getImage())
                    .placeholder(R.drawable.pizza).into(imageView);
        }else {
            imageView.setImageResource(mResources[position]);
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}