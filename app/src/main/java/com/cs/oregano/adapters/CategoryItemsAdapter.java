package com.cs.oregano.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.oregano.DataBaseHelper;
import com.cs.oregano.R;
import com.cs.oregano.activities.AdditionalsActivity;
import com.cs.oregano.model.ItemPrices;
import com.cs.oregano.model.Items;
import com.cs.oregano.model.Order;

import org.apache.commons.lang3.text.WordUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 09-06-2016.
 */
public class CategoryItemsAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Items> itemsList = new ArrayList<>();
    ArrayList<ItemPrices> priceList = new ArrayList<>();
    int pos;
    String id;
    private DataBaseHelper myDbHelper;
    DecimalFormat dec =new DecimalFormat("0.00");

    private int orderPrice, orderQuantity;
    String language;
    //public ImageLoader imageLoader;

    public CategoryItemsAdapter(Context context, ArrayList<Items> itemsList, String language) {
        this.context = context;
        this.itemsList = itemsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myDbHelper = new DataBaseHelper(context);
        this.language = language;
        //DBcontroller = new DataBaseHelper(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return itemsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView itemName, itemDesc, childBadgeQty;
        LinearLayout itemsLayout, childLayout;
        RelativeLayout badgeLayout;
        ImageView itemImage,non_delivery;
//        View v1;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final View v1 = inflater.inflate(R.layout.product_info_dropdown, null);
        final TextView price = (TextView) v1.findViewById(R.id.product_price);
        ImageView plusBtn = (ImageView) v1.findViewById(R.id.plus_btn);
        ImageView minusBtn = (ImageView) v1.findViewById(R.id.minus_btn);
        final TextView count = (TextView) v1.findViewById(R.id.product_count);
        final LinearLayout commentayout = (LinearLayout) v1.findViewById(R.id.comment_layout);
        final ArrayList<Order> orderList = new ArrayList<>();
        pos = position;
        priceList = itemsList.get(position).getItemPrices();
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.category_items_row, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.category_items_row_arabic, null);
            }

            holder.itemName = (TextView) convertView
                    .findViewById(R.id.item_name);
            holder.itemDesc = (TextView) convertView
                    .findViewById(R.id.item_desc);
//            holder.itemPrice = (TextView) convertView.findViewById(R.id.item_price);
            holder.itemsLayout = (LinearLayout) convertView.findViewById(R.id.items_layout);

            holder.childBadgeQty = (TextView) convertView.findViewById(R.id.child_badge_qty);
            final RelativeLayout infoLayout = (RelativeLayout) v1.findViewById(R.id.info_layout);
//            LinearLayout itemLayout = (LinearLayout) convertView.findViewById(R.id.item_layout);
            holder.childLayout = (LinearLayout) convertView.findViewById(R.id.child_layout);
//            final TextView childQty = (TextView) convertView.findViewById(R.id.child_item_qty);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.item_image);
            holder.badgeLayout = (RelativeLayout) convertView.findViewById(R.id.child_badge_layout);
            holder.non_delivery = (ImageView) convertView.findViewById(R.id.non_delivery);
//            final TextView foodPrice = (TextView) convertView.findViewById(R.id.item_price_food);
//            final TextView dummyPrice = (TextView) convertView.findViewById(R.id.dummy_price);

//            holder.childLayout.removeAllViews();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(language.equalsIgnoreCase("En")){
            holder.itemName.setText(WordUtils.capitalizeFully(itemsList.get(position).getItemName()));
            holder.itemDesc.setText(itemsList.get(position).getItemDesc());
        }else if(language.equalsIgnoreCase("Ar")){
            holder.itemName.setText(itemsList.get(position).getItemName_ar());
            holder.itemDesc.setText(itemsList.get(position).getItemDesc_ar());
        }
        holder.childLayout.removeAllViews();

        Glide.with(context).load("http://www.ircfood.com/images/"+itemsList.get(position).getImages())
                .into(holder.itemImage);
//        holder.itemPrice.setText(priceList.get(0).getPrice()+" SR");
        price.setText(dec.format(Float.parseFloat(priceList.get(0).getPrice())));

        if(itemsList.get(position).getIsDelivery().equals("true")){
            holder.non_delivery.setVisibility(View.INVISIBLE);
        }
        else{
            holder.non_delivery.setVisibility(View.VISIBLE);
        }

        holder.itemsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priceList = itemsList.get(position).getItemPrices();
                orderList.clear();
                orderList.addAll(myDbHelper.getItemOrderInfo(itemsList.get(position).getItemId()));
//                if(!itemsList.get(position).getAdditionalsId().equalsIgnoreCase("0")){
                    Intent intent = new Intent(context, AdditionalsActivity.class);
                    intent.putExtra("item_name", itemsList.get(position).getItemName());
                    intent.putExtra("item_desc", itemsList.get(position).getItemDesc());
                    intent.putExtra("item_nameAr", itemsList.get(position).getItemName_ar());
                    intent.putExtra("item_descAr", itemsList.get(position).getItemDesc_ar());
                    intent.putExtra("item_id",itemsList.get(position).getItemId());
                    intent.putExtra("category_id",itemsList.get(position).getCategoryId());
                    intent.putExtra("additional_id", itemsList.get(position).getAdditionalsId());
                    intent.putExtra("item_prices", itemsList.get(position).getItemPrices());
                    intent.putExtra("item_image", itemsList.get(position).getImages());
                    intent.putExtra("delivery", itemsList.get(position).getIsDelivery());
                    context.startActivity(intent);

            }
        });


        int cnt = myDbHelper.getItemOrderCount(itemsList.get(position).getItemId());
        if(cnt !=0){
            holder.badgeLayout.setVisibility(View.VISIBLE);
            holder.childBadgeQty.setText(""+cnt);
//            orderQuantity = cnt;
        }else {
            holder.badgeLayout.setVisibility(View.GONE);
        }

        return convertView;
    }
}
