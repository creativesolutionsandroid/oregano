package com.cs.oregano.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.oregano.Constants;
import com.cs.oregano.R;
import com.cs.oregano.model.OrderHistory;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by SKT on 20-02-2016.
 */
public class OrderHistoryAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<OrderHistory> favOrderList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;

    public OrderHistoryAdapter(Context context, ArrayList<OrderHistory> favOrderList, String language) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }
    @Override
    public int getViewTypeCount() {
        // menu type count
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if(favOrderList.get(position).getOrderStatus().equals("Rejected") ||favOrderList.get(position).getOrderStatus().equals("Cancel") ||favOrderList.get(position).getOrderStatus().equals("Close")){
            type = 0;
        }
        else{
            type = 1;
        }
        // current menu type
        return type;
    }
    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView orderNumber,orderDate,totalPrice, itemDetails, storeName;
        ImageView progress;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.order_history_listitem, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.order_history_listitem_arabic, null);
            }


            holder.orderNumber = (TextView) convertView
                    .findViewById(R.id.order_number);
            holder.orderDate = (TextView) convertView
                    .findViewById(R.id.order_date);
            holder.totalPrice = (TextView) convertView
                    .findViewById(R.id.favorite_price);
            holder.storeName = (TextView) convertView
                    .findViewById(R.id.store_name);

            holder.itemDetails = (TextView) convertView.findViewById(R.id.item_details);
            holder.progress = (ImageView) convertView.findViewById(R.id.order_step);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        DecimalFormat dec =new DecimalFormat("0.00");
//        float historyprice = dec.format(Float.parseFloat(Constants.convertToArabic(favOrderList.get(position).getTotalPrice())));
        holder.totalPrice.setText(dec.format(Float.parseFloat(Constants.convertToArabic(favOrderList.get(position).getTotalPrice()))).replace("??.??","00.00"));
        if(language.equalsIgnoreCase("En")){
            holder.orderNumber.setText("Order No : "+favOrderList.get(position).getInvoiceNo());
            holder.storeName.setText(favOrderList.get(position).getStoreName());
            holder.itemDetails.setText(favOrderList.get(position).getItemDetails());
        }else if(language.equalsIgnoreCase("Ar")){
            holder.orderNumber.setText("رقم الطلب : "+favOrderList.get(position).getInvoiceNo());
            holder.storeName.setText(favOrderList.get(position).getStoreName_ar());
            holder.itemDetails.setText(favOrderList.get(position).getItemDetails_ar());
        }

        Log.i("DATE TAG", "" + favOrderList.get(position).getOrderDate());
        SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getOrderDate());
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                dateObj = curFormater1.parse(favOrderList.get(position).getOrderDate());
            }catch (ParseException pe){

            }

        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.orderDate.setText(date);

        if(language.equalsIgnoreCase("En")) {
            if (!favOrderList.get(position).getOrderType().equalsIgnoreCase("Delivery")) {
                if (favOrderList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.order_history4);
                } else if (favOrderList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.order_history2);
                } else if (favOrderList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.order_history3);
                } else if (favOrderList.get(position).getOrderStatus().equals("Rejected") || favOrderList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.order_history5);
                } else {
                    holder.progress.setImageResource(R.drawable.order_history1);
                }
            } else {
                if (favOrderList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery5);
                } else if (favOrderList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery2);
                } else if (favOrderList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery3);
                } else if (favOrderList.get(position).getOrderStatus().equals("OnTheWay")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery4);
                } else if (favOrderList.get(position).getOrderStatus().equals("Rejected") || favOrderList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery6);
                } else {
                    holder.progress.setImageResource(R.drawable.order_history_delivery1);
                }
            }
        }else if(language.equalsIgnoreCase("Ar")){
            if (!favOrderList.get(position).getOrderType().equalsIgnoreCase("Delivery")) {
                if (favOrderList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.order_history4_arabic);
                } else if (favOrderList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.order_history2_arabic);
                } else if (favOrderList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.order_history3_arabic);
                } else if (favOrderList.get(position).getOrderStatus().equals("Rejected") || favOrderList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.order_history5_arabic);
                } else {
                    holder.progress.setImageResource(R.drawable.order_history1_arabic);
                }
            } else {
                if (favOrderList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery5_arabic);
                } else if (favOrderList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery2_arabic);
                } else if (favOrderList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery3_arabic);
                } else if (favOrderList.get(position).getOrderStatus().equals("OnTheWay")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery4_arabic);
                } else if (favOrderList.get(position).getOrderStatus().equals("Rejected") || favOrderList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery6_arabic);
                } else {
                    holder.progress.setImageResource(R.drawable.order_history_delivery1_arabic);
                }
            }
        }
//
//        holder.totalBalance.setText(String.format("%.2f", Float.parseFloat(storesList.get(position).getTotalBalance())));


        return convertView;
    }
}