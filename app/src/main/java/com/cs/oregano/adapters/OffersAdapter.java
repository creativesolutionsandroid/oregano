package com.cs.oregano.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.oregano.Constants;
import com.cs.oregano.R;
import com.cs.oregano.model.Offers;

import java.util.ArrayList;

/**
 * Created by CS on 08-08-2016.
 */
public class OffersAdapter  extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Offers> offersList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id, language;
    //public ImageLoader imageLoader;

    public OffersAdapter(Context context, ArrayList<Offers> offersList, String language) {
        this.context = context;
        this.offersList = offersList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return offersList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView title, desc;
        ImageView offerImage;
//        View dot;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.offers_listitem, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.offers_listitem_arabic, null);
            }


            holder.title = (TextView) convertView
                    .findViewById(R.id.offer_title);
            holder.desc = (TextView) convertView
                    .findViewById(R.id.offer_desc);
            holder.offerImage = (ImageView) convertView
                    .findViewById(R.id.offer_image);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(language.equalsIgnoreCase("En")) {
            holder.title.setText(offersList.get(position).getOfferTitle());
            holder.desc.setText(offersList.get(position).getOfferDesc());
        }else if(language.equalsIgnoreCase("Ar")){
            holder.title.setText(offersList.get(position).getOfferTitleAr());
            holder.desc.setText(offersList.get(position).getOfferDescAr());
        }
        Glide.with(context).load(Constants.IMAGE_URL +offersList.get(position).getOfferImage()).into(holder.offerImage);

        return convertView;
    }
}