package com.cs.oregano.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.oregano.Constants;
import com.cs.oregano.R;
import com.cs.oregano.activities.AdditionalsActivity;
import com.cs.oregano.model.Additionals;
import com.cs.oregano.model.Modifiers;
import com.cs.oregano.model.SelectedAddtionals;

import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by SKT on 31-01-2016.
 */
public class AdditionalsListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Modifiers> groups;
    private ArrayList<Additionals> nutritionList;
    private ArrayList<Additionals> additionalPriceList;
    private ArrayList<String> additionalsStrList = new ArrayList<>();
    public static ArrayList<String> additionalsIdList = new ArrayList<>();
    public static ArrayList<String> modifierIdList = new ArrayList<>();
    public static ArrayList<String> dummyModifierIdList = new ArrayList<>();
    public static ArrayList<SelectedAddtionals> additionalsList = new ArrayList<>();
    public ArrayList<SelectedAddtionals> additionalsList1 = new ArrayList<>();
//    List<SelectedAddtionals> thingsToBeAdd = new ArrayList<>();
    ArrayList<Additionals> addList = new ArrayList<>();
    TextView price;
    TextView count;
    DecimalFormat dec =new DecimalFormat("0.00");
    public static int additionalsPrice;
    String addStr, modStr, modifiersId, categoryId;
    String language;
    int previousId;
    boolean isAlreadyAdded = false;
    public static boolean isMilkSelected = false, isCoffeeSelected = false, isFlavourSelected = false, isAdded = false;


    public AdditionalsListAdapter(Context context, ArrayList<Modifiers> groups, String categoryId, String language) {
        this.context = context;
        this.groups = groups;
        additionalsIdList.clear();
        modifierIdList.clear();
        additionalsList.clear();
        additionalsPrice = 0;
        this.categoryId = categoryId;
        this.language = language;
        isAdded = false;
        dummyModifierIdList.clear();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<Additionals> chList = groups.get(groupPosition).getChildItems();

        return chList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final Additionals child = (Additionals) getChild(groupPosition, childPosition);
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
//            LayoutInflater infalInflater = (LayoutInflater) context
//                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            if(language.equalsIgnoreCase("En")){
                convertView = infalInflater.inflate(R.layout.additional_child_item, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = infalInflater.inflate(R.layout.additional_child_item_arabic, null);
            }

        }

        TextView price = (TextView) convertView.findViewById(R.id.additional_child_price);
        TextView checkBoxText = (TextView) convertView.findViewById(R.id.additional_checkbox_text);
        final CheckBox childItem = (CheckBox) convertView.findViewById(R.id.checkBox);
        ImageView additionalImmage = (ImageView) convertView.findViewById(R.id.additional_image);
        RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.additional_child_layout);

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isAdded = false;
                if (!childItem.isChecked()) {
                    Log.i("check", "true");
                    isAlreadyAdded = false;
                    additionalsPrice = 0;

                    if (additionalsList.size() > 0) {
                        for (Iterator<SelectedAddtionals> it = additionalsList.iterator(); it.hasNext(); ) {
                            SelectedAddtionals sa1 = it.next();
                            if (!child.getAdditionalsId().equals(sa1.getAdditionalId())) {
                                if(child.getModifierId().equals("2")) {
                                    if(categoryId.equals("4")|| categoryId.equals("7")||categoryId.equals("8")){
                                        if(child.getModifierId().equals(sa1.getModifierId())) {
                                            additionalsList1.remove(sa1);
                                            additionalsIdList.remove(sa1.getAdditionalId());
                                            additionalsStrList.remove(sa1.getAdditionalName());
                                            modifierIdList.remove(sa1.getModifierId());
                                            float delprice = Float.parseFloat(Constants.convertToArabic(sa1.getPrice()));
                                            AdditionalsActivity.priceAd = AdditionalsActivity.priceAd - delprice;
                                            AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice-(delprice*AdditionalsActivity.quantity);
                                            AdditionalsActivity.itemPrice.setText("" + dec.format(AdditionalsActivity.finalPrice) );
                                            if (!isAlreadyAdded) {
                                                isAlreadyAdded = true;
                                                if(child.getAdditionalPrice() == null){
                                                    child.setAdditionalPrice("0");
                                                }
                                                additionalsIdList.add(child.getAdditionalsId());
                                                modifierIdList.add(child.getModifierId());
                                                SelectedAddtionals sa = new SelectedAddtionals();
                                                sa.setAdditionalId(child.getAdditionalsId());
                                                sa.setModifierId(child.getModifierId());
                                                sa.setPrice(child.getAdditionalPrice());
                                                sa.setAdditionalName(child.getAdditionalName());
                                                sa.setAdditionalNameAr(child.getAdditionalNameAr());
                                                sa.setAdditionalPriceList(child.getAdditionalPriceList());
                                                additionalsList1.add(sa);
                                                float price = Float.parseFloat(Constants.convertToArabic(child.getAdditionalPrice()));
                                                AdditionalsActivity.priceAd = AdditionalsActivity.priceAd + price;
                                                AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice + (price * AdditionalsActivity.quantity);
                                                AdditionalsActivity.itemPrice.setText("" + dec.format(AdditionalsActivity.finalPrice) );
                                            }
                                        }else{
                                            if(!isAlreadyAdded) {
                                                isAlreadyAdded = true;
                                                if(child.getAdditionalPrice() == null){
                                                    child.setAdditionalPrice("0");
                                                }
                                                additionalsIdList.add(child.getAdditionalsId());
                                                modifierIdList.add(child.getModifierId());
                                                SelectedAddtionals sa = new SelectedAddtionals();
                                                sa.setAdditionalId(child.getAdditionalsId());
                                                sa.setModifierId(child.getModifierId());
                                                sa.setPrice(child.getAdditionalPrice());
                                                sa.setAdditionalName(child.getAdditionalName());
                                                sa.setAdditionalNameAr(child.getAdditionalNameAr());
                                                sa.setAdditionalPriceList(child.getAdditionalPriceList());
                                                additionalsList1.add(sa);
                                                float price = Float.parseFloat(Constants.convertToArabic(child.getAdditionalPrice()));
                                                AdditionalsActivity.priceAd = AdditionalsActivity.priceAd + price;
                                                AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice + (price * AdditionalsActivity.quantity);
                                                AdditionalsActivity.itemPrice.setText("" +dec.format( AdditionalsActivity.finalPrice) );
                                            }
                                        }
                                    }else{
                                        if(!isAlreadyAdded) {
                                            isAlreadyAdded = true;
                                            if(child.getAdditionalPrice() == null){
                                                child.setAdditionalPrice("0");
                                            }
                                            additionalsIdList.add(child.getAdditionalsId());
                                            modifierIdList.add(child.getModifierId());
                                            SelectedAddtionals sa = new SelectedAddtionals();
                                            sa.setAdditionalId(child.getAdditionalsId());
                                            sa.setModifierId(child.getModifierId());
                                            sa.setPrice(child.getAdditionalPrice());
                                            sa.setAdditionalName(child.getAdditionalName());
                                            sa.setAdditionalNameAr(child.getAdditionalNameAr());
                                            sa.setAdditionalPriceList(child.getAdditionalPriceList());
                                            additionalsList1.add(sa);
                                            float price = Float.parseFloat(Constants.convertToArabic(child.getAdditionalPrice()));
                                            AdditionalsActivity.priceAd = AdditionalsActivity.priceAd + price;
                                            AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice + (price * AdditionalsActivity.quantity);
                                            AdditionalsActivity.itemPrice.setText("" + dec.format(AdditionalsActivity.finalPrice) );
                                        }
                                    }
                                }else if(child.getModifierId().equals("8") || child.getModifierId().equals("13")
                                || child.getModifierId().equals("14")){
                                    if(child.getModifierId().equals(sa1.getModifierId())) {
                                        additionalsList1.remove(sa1);
                                        additionalsIdList.remove(sa1.getAdditionalId());
                                        additionalsStrList.remove(sa1.getAdditionalName());
                                        modifierIdList.remove(sa1.getModifierId());
                                        float delprice = Float.parseFloat(Constants.convertToArabic(sa1.getPrice()));
                                        AdditionalsActivity.priceAd = AdditionalsActivity.priceAd - delprice;
                                        AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice-(delprice*AdditionalsActivity.quantity);
                                        AdditionalsActivity.itemPrice.setText("" + dec.format(AdditionalsActivity.finalPrice) );
                                        if (!isAlreadyAdded) {
                                            isAlreadyAdded = true;
                                            if(child.getAdditionalPrice() == null){
                                                child.setAdditionalPrice("0");
                                            }
                                            additionalsIdList.add(child.getAdditionalsId());
                                            modifierIdList.add(child.getModifierId());
                                            SelectedAddtionals sa = new SelectedAddtionals();
                                            sa.setAdditionalId(child.getAdditionalsId());
                                            sa.setModifierId(child.getModifierId());
                                            sa.setPrice(child.getAdditionalPrice());
                                            sa.setAdditionalName(child.getAdditionalName());
                                            sa.setAdditionalNameAr(child.getAdditionalNameAr());
                                            sa.setAdditionalPriceList(child.getAdditionalPriceList());
                                            additionalsList1.add(sa);
                                            float price = Float.parseFloat(Constants.convertToArabic(child.getAdditionalPrice()));
                                            AdditionalsActivity.priceAd = AdditionalsActivity.priceAd + price;
                                            AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice + (price * AdditionalsActivity.quantity);
                                            AdditionalsActivity.itemPrice.setText("" + dec.format(AdditionalsActivity.finalPrice) );
                                        }
                                    }else{
                                        if(!isAlreadyAdded) {
                                            isAlreadyAdded = true;
                                            if(child.getAdditionalPrice() == null){
                                                child.setAdditionalPrice("0");
                                            }
                                            additionalsIdList.add(child.getAdditionalsId());
                                            modifierIdList.add(child.getModifierId());
                                            SelectedAddtionals sa = new SelectedAddtionals();
                                            sa.setAdditionalId(child.getAdditionalsId());
                                            sa.setModifierId(child.getModifierId());
                                            sa.setPrice(child.getAdditionalPrice());
                                            sa.setAdditionalName(child.getAdditionalName());
                                            sa.setAdditionalNameAr(child.getAdditionalNameAr());
                                            sa.setAdditionalPriceList(child.getAdditionalPriceList());
                                            additionalsList1.add(sa);
                                            float price = Float.parseFloat(Constants.convertToArabic(child.getAdditionalPrice()));
                                            AdditionalsActivity.priceAd = AdditionalsActivity.priceAd + price;
                                            AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice + (price * AdditionalsActivity.quantity);
                                            AdditionalsActivity.itemPrice.setText("" + dec.format(AdditionalsActivity.finalPrice) );
                                        }
                                    }
                                }else{
                                    if(!isAlreadyAdded) {
                                        isAlreadyAdded = true;
                                        if(child.getAdditionalPrice() == null){
                                            child.setAdditionalPrice("0");
                                        }
                                        additionalsIdList.add(child.getAdditionalsId());
                                        modifierIdList.add(child.getModifierId());
                                        SelectedAddtionals sa = new SelectedAddtionals();
                                        sa.setAdditionalId(child.getAdditionalsId());
                                        sa.setModifierId(child.getModifierId());
                                        sa.setPrice(child.getAdditionalPrice());
                                        sa.setAdditionalName(child.getAdditionalName());
                                        sa.setAdditionalNameAr(child.getAdditionalNameAr());
                                        sa.setAdditionalPriceList(child.getAdditionalPriceList());
                                        additionalsList1.add(sa);
                                        float price = Float.parseFloat(Constants.convertToArabic(child.getAdditionalPrice()));
                                        AdditionalsActivity.priceAd = AdditionalsActivity.priceAd + price;
                                        AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice + (price * AdditionalsActivity.quantity);
                                        AdditionalsActivity.itemPrice.setText("" + dec.format(AdditionalsActivity.finalPrice) );
                                    }
                                }

                                }else{
                                if(!isAlreadyAdded) {
                                    isAlreadyAdded = true;
                                    if(child.getAdditionalPrice() == null){
                                        child.setAdditionalPrice("0");
                                    }
                                    additionalsIdList.add(child.getAdditionalsId());
                                    modifierIdList.add(child.getModifierId());
                                    SelectedAddtionals sa = new SelectedAddtionals();
                                    sa.setAdditionalId(child.getAdditionalsId());
                                    sa.setModifierId(child.getModifierId());
                                    sa.setPrice(child.getAdditionalPrice());
                                    sa.setAdditionalName(child.getAdditionalName());
                                    sa.setAdditionalNameAr(child.getAdditionalNameAr());
                                    sa.setAdditionalPriceList(child.getAdditionalPriceList());
                                    additionalsList1.add(sa);
                                    float price = Float.parseFloat(Constants.convertToArabic(child.getAdditionalPrice()));
                                    AdditionalsActivity.priceAd = AdditionalsActivity.priceAd + price;
                                    AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice + (price * AdditionalsActivity.quantity);
                                    AdditionalsActivity.itemPrice.setText("" + dec.format(AdditionalsActivity.finalPrice) );
                                }
                            }
                        }
                    }else{
                        if(!isAlreadyAdded){
                            isAlreadyAdded = true;
                            if(child.getAdditionalPrice() == null){
                                child.setAdditionalPrice("0");
                            }
                            additionalsIdList.add(child.getAdditionalsId());
                            modifierIdList.add(child.getModifierId());
                            SelectedAddtionals sa = new SelectedAddtionals();
                            sa.setAdditionalId(child.getAdditionalsId());
                            sa.setModifierId(child.getModifierId());
                            sa.setPrice(child.getAdditionalPrice());
                            sa.setAdditionalName(child.getAdditionalName());
                            sa.setAdditionalNameAr(child.getAdditionalNameAr());
                            sa.setAdditionalPriceList(child.getAdditionalPriceList());
                            additionalsList1.add(sa);
                            float price = Float.parseFloat(Constants.convertToArabic(child.getAdditionalPrice()));
                            AdditionalsActivity.priceAd = AdditionalsActivity.priceAd + price;
                            AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice+(price*AdditionalsActivity.quantity);

                            AdditionalsActivity.itemPrice.setText("" + dec.format(AdditionalsActivity.finalPrice) );
                        }
                    }

                    notifyDataSetChanged();
                } else {
                    Log.i("uncheck", "true");
                    for(Iterator<SelectedAddtionals> it = additionalsList.iterator(); it.hasNext();) {
                        SelectedAddtionals sa1 = it.next();
                        if (child.getAdditionalsId().equals(sa1.getAdditionalId())) {
                            additionalsList1.remove(sa1);
                            additionalsIdList.remove(child.getAdditionalsId());
                            additionalsStrList.remove(child.getAdditionalName());
                            float price = Float.parseFloat(Constants.convertToArabic(child.getAdditionalPrice()));
                            AdditionalsActivity.priceAd = AdditionalsActivity.priceAd - price;
                            AdditionalsActivity.finalPrice = AdditionalsActivity.finalPrice-(price*AdditionalsActivity.quantity);
                            AdditionalsActivity.itemPrice.setText("" + dec.format(AdditionalsActivity.finalPrice) );
                        }
                    }
                }

                Log.i("size TAG", ""+additionalsList1.size());
                Log.i("size TAG", ""+additionalsList.size());
                additionalsList.clear();
                additionalsList.addAll(additionalsList1);
                Log.i("size TAG1", ""+additionalsList.size());
                notifyDataSetChanged();
            }
        });

        if(child.getModifierId().equals("8") || child.getModifierId().equals("13")
                || child.getModifierId().equals("14")) {
            childItem.setButtonDrawable(context.getResources().getDrawable(R.drawable.payment_selector));
        }else{
            childItem.setButtonDrawable(context.getResources().getDrawable(R.drawable.additional_selector));
        }

        if(categoryId.equals("4")|| categoryId.equals("7")||categoryId.equals("8")) {
            if (child.getModifierId().equals("2")) {
//                    for (SelectedAddtionals sa : additionalsList) {
//                        if (sa.getAdditionalId().equals(child.getAdditionalsId())) {
                childItem.setButtonDrawable(context.getResources().getDrawable(R.drawable.payment_selector));
//                        } else {
//                            childItem.setChecked(false);
//                        }
//                    }
            }else{
                childItem.setButtonDrawable(context.getResources().getDrawable(R.drawable.additional_selector));
            }
        }

        if(additionalsIdList.contains(child.getAdditionalsId())){
            childItem.setChecked(true);
            if(child.getModifierId().equals("8") || child.getModifierId().equals("13")
                    || child.getModifierId().equals("14")){

            }else{
                childItem.setChecked(true);
            }
            if(categoryId.equals("4")|| categoryId.equals("7")||categoryId.equals("8")) {
                if (child.getModifierId().equals("2")) {
//                    for (SelectedAddtionals sa : additionalsList) {
//                        if (sa.getAdditionalId().equals(child.getAdditionalsId())) {
//                            childItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.payment_selector));
//                        } else {
//                            childItem.setChecked(false);
//                        }
//                    }
                }else{
                }
            }else{
                childItem.setChecked(true);
            }
        }else{
            childItem.setChecked(false);
        }

        String modId = "";

        if(language.equalsIgnoreCase("En")){
            checkBoxText.setText(child.getAdditionalName());
        }else if(language.equalsIgnoreCase("Ar")){
            checkBoxText.setText(child.getAdditionalNameAr());
        }

        Glide.with(context).load("http://www.ircfood.com/images/"+child.getImages()).into(additionalImmage);

        if(child.getAdditionalPriceList().size() == 1){
            if(child.getAdditionalPriceList().get(0).getPrice().equals("0")){
                price.setText("0.00");
            }else {
                price.setText(dec.format(Float.parseFloat(child.getAdditionalPriceList().get(0).getPrice()) ));
            }
            child.setAdditionalPrice(child.getAdditionalPriceList().get(0).getPrice());
        }else {
            for (int i = 0; i < child.getAdditionalPriceList().size(); i++) {
                if (child.getAdditionalPriceList().get(i).getItemType().equals(AdditionalsActivity.itemType)) {
                    if(child.getAdditionalPriceList().get(i).getPrice().equals("0")){
                        price.setText("0.00");
                    }else {
                        price.setText(dec.format(Float.parseFloat(child.getAdditionalPriceList().get(i).getPrice())) );
                    }
                    child.setAdditionalPrice(dec.format(Float.parseFloat(child.getAdditionalPriceList().get(i).getPrice())));
                }
            }
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Additionals> chList = groups.get(groupPosition).getChildItems();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Modifiers group = (Modifiers) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            if(language.equalsIgnoreCase("En")){
                convertView = inf.inflate(R.layout.additionals_group_item, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inf.inflate(R.layout.additionals_group_item_arabic, null);
            }
        }

        TextView tv = (TextView) convertView.findViewById(R.id.additional_group_name);
        TextView price = (TextView) convertView.findViewById(R.id.additional_price);
        ImageView iv = (ImageView) convertView.findViewById(R.id.group_indicator);
        ImageView groupIcon = (ImageView) convertView.findViewById(R.id.additional_group_icon);
        if(language.equalsIgnoreCase("En")){
            tv.setText(WordUtils.capitalizeFully(group.getModifierName()));
            if (isExpanded) {
                iv.setImageResource(R.drawable.list_selected);
            } else {
                iv.setImageResource(R.drawable.list_unselected);
            }
        }else if(language.equalsIgnoreCase("Ar")){
            tv.setText(group.getModifierNameAr());
            if (isExpanded) {
                iv.setImageResource(R.drawable.list_selected);
            } else {
                iv.setImageResource(R.drawable.list_unselected_left);
            }
        }

        try {
            // get input stream
            InputStream ims = context.getAssets().open(group.getModifierId()+".png");
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            groupIcon.setImageDrawable(d);
        }
        catch(IOException ex) {
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    Animation.AnimationListener animL = new Animation.AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            //this is just a method call you can create to delete the animated view or hide it until you need it again.
//            clearAnimation();
        }
    };

}